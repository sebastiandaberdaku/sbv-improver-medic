PathwayID	Importance_Optional	Description
PathID_20	0.284571	PWY-7196: superpathway of pyrimidine ribonucleosides salvage|unclassified
PathID_171	0.221857	PWY0-1297: superpathway of purine deoxyribonucleosides degradation
PathID_585	0.176	PWY-5097: L-lysine biosynthesis VI|g__Ruminococcus.s__Ruminococcus_callidus
PathID_799	0.315	UNINTEGRATED|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_870	0.189286	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Eubacterium.s__Eubacterium_ventriosum
PathID_993	0.234429	PWY-6277: superpathway of 5-aminoimidazole ribonucleotide biosynthesis|g__Eubacterium.s__Eubacterium_ventriosum
PathID_1073	0.246143	PANTO-PWY: phosphopantothenate biosynthesis I|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_1096	0.208571	PWY-7220: adenosine deoxyribonucleotides de novo biosynthesis II|unclassified
PathID_1131	0.185714	PWY-6385: peptidoglycan biosynthesis III (mycobacteria)|g__Eubacterium.s__Eubacterium_ventriosum
PathID_1221	0.267571	PWY-7111: pyruvate fermentation to isobutanol (engineered)|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_1229	0.246286	CENTFERM-PWY: pyruvate fermentation to butanoate
PathID_1522	0.194857	PWY0-1296: purine ribonucleosides degradation|g__Clostridium.s__Clostridium_clostridioforme
PathID_1935	0.202143	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Parabacteroides.s__Parabacteroides_merdae
PathID_2136	0.309286	PWY-5304: superpathway of sulfur oxidation (Acidianus ambivalens)
PathID_2268	0.211143	FASYN-ELONG-PWY: fatty acid elongation -- saturated|unclassified
PathID_2359	0.257429	PWY-6737: starch degradation V|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_2503	0.210429	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Ruminococcus.s__Ruminococcus_callidus
PathID_2531	0.185714	PWY-6121: 5-aminoimidazole ribonucleotide biosynthesis I|g__Ruminococcus.s__Ruminococcus_callidus
PathID_2620	0.148	PWY-6737: starch degradation V
PathID_2785	0.183286	PWY-6317: galactose degradation I (Leloir pathway)|g__Ruminococcus.s__Ruminococcus_bromii
PathID_2806	0.157	PWY-6163: chorismate biosynthesis from 3-dehydroquinate|g__Clostridium.s__Clostridium_clostridioforme
PathID_2823	0.235	PWY-6122: 5-aminoimidazole ribonucleotide biosynthesis II|g__Eubacterium.s__Eubacterium_ventriosum
PathID_3140	0.279286	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_3197	0.305857	UNINTEGRATED|g__Eubacterium.s__Eubacterium_ventriosum
PathID_3351	0.233286	PWY-5989: stearate biosynthesis II (bacteria and plants)|unclassified
PathID_3373	0.246714	PWY-5097: L-lysine biosynthesis VI|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_3567	0.288429	PANTO-PWY: phosphopantothenate biosynthesis I
PathID_3656	0.181	PWY-6122: 5-aminoimidazole ribonucleotide biosynthesis II|g__Ruminococcus.s__Ruminococcus_callidus
PathID_3686	0.286	PWY-5121: superpathway of geranylgeranyl diphosphate biosynthesis II (via MEP)|unclassified
PathID_3719	0.235571	GLUCUROCAT-PWY: superpathway of &beta;-D-glucuronide and D-glucuronate degradation|unclassified
PathID_3902	0.244286	PWY-6936: seleno-amino acid biosynthesis|unclassified
PathID_3912	0.236571	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Parabacteroides.s__Parabacteroides_merdae
PathID_3983	0.200714	PWY0-862: (5Z)-dodec-5-enoate biosynthesis|unclassified
PathID_4139	0.174429	PWY-6163: chorismate biosynthesis from 3-dehydroquinate|g__Eubacterium.s__Eubacterium_ventriosum
PathID_4427	0.190143	PWY-621: sucrose degradation III (sucrose invertase)|unclassified
PathID_4435	0.248571	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_4498	0.181286	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_4567	0.232	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Bacteroides.s__Bacteroides_intestinalis
PathID_4582	0.251286	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_4608	0.208857	PWY-7664: oleate biosynthesis IV (anaerobic)|unclassified
PathID_4610	0.221286	PWY-6151: S-adenosyl-L-methionine cycle I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_4737	0.178571	UNINTEGRATED|g__Clostridium.s__Clostridium_clostridioforme
PathID_5235	0.207429	PWY-6282: palmitoleate biosynthesis I (from (5Z)-dodec-5-enoate)|unclassified
PathID_5311	0.262714	PWY-6590: superpathway of Clostridium acetobutylicum acidogenic fermentation
PathID_5390	0.190714	PWY-6527: stachyose degradation|g__Faecalibacterium.s__Faecalibacterium_prausnitzii
PathID_5494	0.172714	UNINTEGRATED|g__Dorea.s__Dorea_formicigenerans
PathID_5645	0.264571	ARO-PWY: chorismate biosynthesis I|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_5753	0.207	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Eubacterium.s__Eubacterium_ventriosum
PathID_5759	0.188	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Eubacterium.s__Eubacterium_ventriosum
PathID_6008	0.197286	1CMET2-PWY: N10-formyl-tetrahydrofolate biosynthesis|g__Eubacterium.s__Eubacterium_rectale
PathID_6055	0.202571	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Parabacteroides.s__Parabacteroides_merdae
PathID_6087	0.237	PANTO-PWY: phosphopantothenate biosynthesis I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_6197	0.234143	NONMEVIPP-PWY: methylerythritol phosphate pathway I|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_6198	0.199857	POLYAMINSYN3-PWY: superpathway of polyamine biosynthesis II|unclassified
PathID_6440	0.238143	PWY-6168: flavin biosynthesis III (fungi)|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_6527	0.199286	UNINTEGRATED|g__Barnesiella.s__Barnesiella_intestinihominis
PathID_6551	0.326714	CRNFORCAT-PWY: creatinine degradation I
PathID_6892	0.235143	PWY-2942: L-lysine biosynthesis III|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_6923	0.166571	PWY-6122: 5-aminoimidazole ribonucleotide biosynthesis II|g__Clostridium.s__Clostridium_clostridioforme
PathID_6955	0.280286	COMPLETE-ARO-PWY: superpathway of aromatic amino acid biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_7099	0.230714	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Bacteroides.s__Bacteroides_intestinalis
PathID_7102	0.211	PWY-6121: 5-aminoimidazole ribonucleotide biosynthesis I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_7239	0.181571	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Eubacterium.s__Eubacterium_ventriosum
PathID_7244	0.216143	PWY-6737: starch degradation V|g__Clostridium.s__Clostridium_clostridioforme
PathID_7343	0.205857	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Clostridium.s__Clostridium_clostridioforme
PathID_7354	0.180286	PWY-5100: pyruvate fermentation to acetate and lactate II|g__Eubacterium.s__Eubacterium_ventriosum
PathID_7399	0.223714	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Clostridium.s__Clostridium_clostridioforme
PathID_7429	0.174857	PWY-6737: starch degradation V|g__Eubacterium.s__Eubacterium_ventriosum
PathID_7593	0.199286	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Parabacteroides.s__Parabacteroides_merdae
PathID_8063	0.217571	PWYG-321: mycolate biosynthesis|unclassified
PathID_8389	0.289571	PWY-6590: superpathway of Clostridium acetobutylicum acidogenic fermentation|unclassified
PathID_8670	0.273857	PWY-6163: chorismate biosynthesis from 3-dehydroquinate|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_8685	0.205286	PWY-7222: guanosine deoxyribonucleotides de novo biosynthesis II|unclassified
PathID_8914	0.293	PWY-7211: superpathway of pyrimidine deoxyribonucleotides de novo biosynthesis|unclassified
PathID_9093	0.182714	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Eubacterium.s__Eubacterium_ventriosum
PathID_9468	0.288143	GALACT-GLUCUROCAT-PWY: superpathway of hexuronide and hexuronate degradation|unclassified
PathID_9507	0.299571	CENTFERM-PWY: pyruvate fermentation to butanoate|unclassified
PathID_9780	0.191857	PWY-6737: starch degradation V|g__Ruminococcus.s__Ruminococcus_bromii
PathID_9782	0.180857	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Ruminococcus.s__Ruminococcus_callidus
PathID_9903	0.245571	PWY-6151: S-adenosyl-L-methionine cycle I
PathID_9918	0.187857	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Eubacterium.s__Eubacterium_ventriosum
PathID_9948	0.277571	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_10367	0.230286	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Eubacterium.s__Eubacterium_ventriosum
PathID_10439	0.208286	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_10497	0.172143	ARO-PWY: chorismate biosynthesis I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_10764	0.174714	PWY-6609: adenine and adenosine salvage III|g__Eubacterium.s__Eubacterium_ventriosum
PathID_10875	0.188286	PWY-5686: UMP biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_11074	0.168571	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Clostridium.s__Clostridium_clostridioforme
PathID_11110	0.326	CRNFORCAT-PWY: creatinine degradation I|unclassified
PathID_11179	0.208	PRPP-PWY: superpathway of histidine, purine, and pyrimidine biosynthesis|unclassified
PathID_11323	0.245429	VALSYN-PWY: L-valine biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_11454	0.215143	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Bacteroides.s__Bacteroides_intestinalis
PathID_11571	0.168429	PWY-6163: chorismate biosynthesis from 3-dehydroquinate|g__Ruminococcus.s__Ruminococcus_callidus
PathID_11683	0.185857	PWY-6700: queuosine biosynthesis|g__Ruminococcus.s__Ruminococcus_callidus
PathID_11859	0.173143	PANTO-PWY: phosphopantothenate biosynthesis I|g__Butyrivibrio.s__Butyrivibrio_crossotus
PathID_12264	0.321429	PWY-7357: thiamin formation from pyrithiamine and oxythiamine (yeast)|unclassified
PathID_12502	0.204143	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Clostridium.s__Clostridium_clostridioforme

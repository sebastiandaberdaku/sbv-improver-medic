PathwayID	Importance_Optional	Description
PathID_98	0.204429	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Paraprevotella.s__Paraprevotella_xylaniphila
PathID_529	0.187429	ASPASN-PWY: superpathway of L-aspartate and L-asparagine biosynthesis|g__Bacteroides.s__Bacteroides_faecis
PathID_1364	0.265714	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Bacteroides.s__Bacteroides_uniformis
PathID_1390	0.276857	PWY-5686: UMP biosynthesis|g__Bacteroides.s__Bacteroides_uniformis
PathID_1815	0.248	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Bacteroides.s__Bacteroides_uniformis
PathID_1821	0.189429	PANTO-PWY: phosphopantothenate biosynthesis I|g__Bacteroides.s__Bacteroides_faecis
PathID_2079	0.199143	PWY-7663: gondoate biosynthesis (anaerobic)|g__Bacteroides.s__Bacteroides_faecis
PathID_2249	0.209714	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Paraprevotella.s__Paraprevotella_xylaniphila
PathID_2374	0.175714	PWY-7111: pyruvate fermentation to isobutanol (engineered)|g__Bacteroides.s__Bacteroides_finegoldii
PathID_2650	0.194429	PWY-3661: glycine betaine degradation I
PathID_2973	0.186429	PYRIDOXSYN-PWY: pyridoxal 5'-phosphate biosynthesis I|g__Bacteroides.s__Bacteroides_faecis
PathID_3577	0.232714	PWY-6151: S-adenosyl-L-methionine cycle I|g__Bacteroides.s__Bacteroides_uniformis
PathID_4161	0.218	PANTO-PWY: phosphopantothenate biosynthesis I|g__Bacteroides.s__Bacteroides_uniformis
PathID_4187	0.184286	1CMET2-PWY: N10-formyl-tetrahydrofolate biosynthesis|g__Bacteroides.s__Bacteroides_faecis
PathID_4210	0.187857	PYRIDOXSYN-PWY: pyridoxal 5'-phosphate biosynthesis I|g__Bacteroides.s__Bacteroides_uniformis
PathID_4349	0.197571	SER-GLYSYN-PWY: superpathway of L-serine and glycine biosynthesis I|g__Pseudomonas.s__Pseudomonas_aeruginosa
PathID_4700	0.245857	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Bacteroides.s__Bacteroides_uniformis
PathID_4869	0.220429	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Bacteroides.s__Bacteroides_uniformis
PathID_5909	0.183857	PWY-5695: urate biosynthesis/inosine 5'-phosphate degradation|g__Bacteroides.s__Bacteroides_faecis
PathID_6360	0.201714	ARGININE-SYN4-PWY: L-ornithine de novo  biosynthesis|g__Bacteroides.s__Bacteroides_faecis
PathID_6864	0.260286	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Bacteroides.s__Bacteroides_uniformis
PathID_6875	0.202286	PWY-6700: queuosine biosynthesis|g__Bacteroides.s__Bacteroides_faecis
PathID_6943	0.189429	PWY-1042: glycolysis IV (plant cytosol)|g__Bacteroides.s__Bacteroides_faecis
PathID_7290	0.251857	PWY-3841: folate transformations II|g__Bacteroides.s__Bacteroides_uniformis
PathID_7692	0.192286	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Bacteroides.s__Bacteroides_faecis
PathID_7987	0.205857	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Paraprevotella.s__Paraprevotella_xylaniphila
PathID_8117	0.24	PWY-7111: pyruvate fermentation to isobutanol (engineered)|g__Bacteroides.s__Bacteroides_dorei
PathID_8192	0.183143	PWY-2942: L-lysine biosynthesis III|g__Bacteroides.s__Bacteroides_faecis
PathID_8890	0.188714	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Bacteroides.s__Bacteroides_uniformis
PathID_8918	0.291714	PWY-6700: queuosine biosynthesis|g__Bacteroides.s__Bacteroides_uniformis
PathID_9196	0.222571	PWY-2942: L-lysine biosynthesis III|g__Bacteroides.s__Bacteroides_uniformis
PathID_9198	0.177857	HISTSYN-PWY: L-histidine biosynthesis|g__Bacteroides.s__Bacteroides_faecis
PathID_9199	0.211429	PWY0-845: superpathway of pyridoxal 5'-phosphate biosynthesis and salvage|g__Bacteroides.s__Bacteroides_uniformis
PathID_9383	0.239714	PWY-7242: D-fructuronate degradation|g__Eubacterium.s__Eubacterium_rectale
PathID_9728	0.228571	PWY-6385: peptidoglycan biosynthesis III (mycobacteria)|g__Bacteroides.s__Bacteroides_finegoldii
PathID_11280	0.260714	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Bacteroides.s__Bacteroides_uniformis
PathID_11322	0.180143	PWY0-845: superpathway of pyridoxal 5'-phosphate biosynthesis and salvage|g__Bacteroides.s__Bacteroides_faecis
PathID_11461	0.284286	1CMET2-PWY: N10-formyl-tetrahydrofolate biosynthesis|g__Bacteroides.s__Bacteroides_uniformis
PathID_11512	0.235714	PWY-5097: L-lysine biosynthesis VI|g__Bacteroides.s__Bacteroides_uniformis
PathID_11691	0.190286	PWY-1269: CMP-3-deoxy-D-manno-octulosonate biosynthesis I|g__Bacteroides.s__Bacteroides_faecis
PathID_11895	0.186143	PWY-5097: L-lysine biosynthesis VI|g__Bacteroides.s__Bacteroides_faecis
PathID_12177	0.291143	PWY-6703: preQ0 biosynthesis|g__Bacteroides.s__Bacteroides_uniformis
PathID_12600	0.193857	PWY0-166: superpathway of pyrimidine deoxyribonucleotides de novo biosynthesis (E. coli)

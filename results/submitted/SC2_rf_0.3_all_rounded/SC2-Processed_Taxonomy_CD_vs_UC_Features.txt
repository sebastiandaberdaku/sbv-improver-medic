TaxonomyID	Importance_Optional	Description
TaxID_90410	0.229714	Streptococcus virus DT1
TaxID_267375	0.196143	Pseudoalteromonas marina
TaxID_1505596	0.273714	Blochmannia endosymbiont of Polyrhachis (Hedomyrma) turneri
TaxID_853	0.213714	Faecalibacterium prausnitzii
TaxID_376219	0.208857	Arthrospira sp. PCC 8005
TaxID_1160	0.273857	Planktothrix agardhii
TaxID_1747	0.290429	Cutibacterium acnes
TaxID_1926494	0.157714	Paraburkholderia sp. SOS3
TaxID_200991	0.261143	Planococcus rifietoensis
TaxID_1879023	0.220714	Mycobacterium sp. djl-10
TaxID_311400	0.180714	Thermococcus kodakarensis
TaxID_1160721	0.231857	Ruminococcus bicirculans
TaxID_216851	0.213857	Faecalibacterium
TaxID_54304	0.262857	Planktothrix
TaxID_1912216	0.250143	Cutibacterium
TaxID_1263	0.219	Ruminococcus
TaxID_151340	0.258714	Papillomaviridae

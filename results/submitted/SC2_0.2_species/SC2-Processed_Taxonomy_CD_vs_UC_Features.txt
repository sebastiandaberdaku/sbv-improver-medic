TaxonomyID	Importance_Optional	Description
TaxID_28104	0.184857	Mesorhizobium huakuii
TaxID_1315974	0.129429	Sphingobium sp. TKS
TaxID_1678728	0.165	Flavobacterium kingsejongi
TaxID_296	0.147857	Pseudomonas fragi
TaxID_1513258	0.206	Gammapapillomavirus 13
TaxID_1580596	0.128571	Phaeobacter piscinae
TaxID_1933910	0.169857	Bartonella sp. A1379B
TaxID_1814957	0.168	Streptococcus virus 9871
TaxID_80869	0.153857	Acidovorax citrulli
TaxID_1327989	0.156143	Serratia sp. FS14
TaxID_1173283	0.118857	Pseudomonas sp. R3-18-08
TaxID_38288	0.158429	Corynebacterium genitalium
TaxID_1982917	0.210571	Mycobacterium virus Gadjet
TaxID_887061	0.089	Methylovorus sp. MP688
TaxID_112023	0.158286	Streptococcus virus 7201
TaxID_1636250	0.160857	Proteus phage vB_PmiM_Pm5461
TaxID_1288970	0.144857	Magnetospira sp. QH-2
TaxID_1922390	0.155286	Beihai hermit crab virus 3
TaxID_90410	0.229286	Streptococcus virus DT1
TaxID_267375	0.198571	Pseudoalteromonas marina
TaxID_1455075	0.164429	Citrobacter phage CR44b
TaxID_247523	0.153143	Pseudoalteromonas aliena
TaxID_1505596	0.276286	Blochmannia endosymbiont of Polyrhachis (Hedomyrma) turneri
TaxID_330734	0.118429	Marinobacter psychrophilus
TaxID_28090	0.112857	Acinetobacter lwoffii
TaxID_853	0.245	Faecalibacterium prausnitzii
TaxID_1814958	0.162286	Streptococcus virus 9872
TaxID_2072414	0.144	Stenotrophomonas sp. ESTM1D_MKCIP4_1
TaxID_1816218	0.208857	Colwellia sp. PAMC 20917
TaxID_208479	0.121143	[Clostridium] bolteae
TaxID_376219	0.215714	Arthrospira sp. PCC 8005
TaxID_56812	0.140857	Shewanella frigidimarina
TaxID_357276	0.155	Bacteroides dorei
TaxID_136241	0.163714	Mycoplasma haemocanis
TaxID_1306274	0.097	Nostoc flagelliforme
TaxID_55802	0.115857	Thermococcus barophilus
TaxID_1408191	0.113857	Corynebacterium deserti
TaxID_33011	0.160857	Cutibacterium granulosum
TaxID_1848755	0.160286	Actinobacteria bacterium IMCC26077
TaxID_585425	0.158286	Synechococcus sp. KORDI-52
TaxID_2133959	0.158857	Bartonella sp. 'Tel Aviv'
TaxID_2502843	0.120143	Erythrobacter sp. HKB08
TaxID_1211640	0.136571	Caulobacter phage CcrColossus
TaxID_246273	0.136143	Wolbachia endosymbiont of Cimex lectularius
TaxID_208962	0.165857	Escherichia albertii
TaxID_1406512	0.131429	Candidatus Methanomassiliicoccus intestinalis
TaxID_1206	0.124	Trichodesmium erythraeum
TaxID_71999	0.130714	Kocuria palustris
TaxID_1270	0.167429	Micrococcus luteus
TaxID_1160	0.293571	Planktothrix agardhii
TaxID_1747	0.315	Cutibacterium acnes
TaxID_1283071	0.17	Vibrio phage JA-1
TaxID_649756	0.123429	Anaerostipes hadrus
TaxID_1926494	0.182286	Paraburkholderia sp. SOS3
TaxID_200991	0.284143	Planococcus rifietoensis
TaxID_28452	0.102143	Leptospira alstonii
TaxID_1680	0.172857	Bifidobacterium adolescentis
TaxID_34018	0.108	Rhodospirillum centenum
TaxID_2169689	0.155571	Serratia virus MAM1
TaxID_2111	0.165714	Mycoplasma arthritidis
TaxID_1566990	0.164714	Streptococcus phage SpSL1
TaxID_1879023	0.240571	Mycobacterium sp. djl-10
TaxID_1528099	0.121714	Lawsonella clevelandensis
TaxID_1725232	0.113571	Candidatus Desulfovibrio trichonymphae
TaxID_881	0.126571	Desulfovibrio vulgaris
TaxID_311400	0.200143	Thermococcus kodakarensis
TaxID_72638	0.125571	Streptococcus virus Sfi19
TaxID_28123	0.148857	Porphyromonas asaccharolytica
TaxID_1850252	0.144	Tenacibaculum todarodis
TaxID_1160721	0.245714	Ruminococcus bicirculans

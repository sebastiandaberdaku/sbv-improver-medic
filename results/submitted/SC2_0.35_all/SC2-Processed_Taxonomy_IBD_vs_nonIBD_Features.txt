TaxonomyID	Importance_Optional	Description
TaxID_292800	0.221714	Flavonifractor plautii
TaxID_161675	0.254143	Tamana bat virus
TaxID_1273687	0.251286	Mycobacterium sp. VKM Ac-1817D
TaxID_2218628	0.223143	Photorhabdus laumondii
TaxID_204039	0.245714	Dickeya dianthicola
TaxID_1816219	0.349714	Colwellia sp. PAMC 21821
TaxID_1778262	0.221571	Candidatus Doolittlea endobia
TaxID_208479	0.284714	[Clostridium] bolteae
TaxID_76517	0.231429	Campylobacter hominis
TaxID_885867	0.212143	Prochlorococcus phage P-SSP10
TaxID_39491	0.312143	[Eubacterium] rectale
TaxID_1834196	0.327857	Lachnoclostridium sp. YL32
TaxID_143393	0.226	[Eubacterium] sulci
TaxID_43659	0.254714	Pseudoalteromonas tetraodonis
TaxID_649756	0.338571	Anaerostipes hadrus
TaxID_367190	0.218429	Yersinia similis
TaxID_1906657	0.227143	Candidatus Doolittlea
TaxID_1506553	0.304286	Lachnoclostridium
TaxID_11051	0.244857	Flavivirus
TaxID_456826	0.310286	Candidatus Cloacimonas
TaxID_207244	0.285571	Anaerostipes
TaxID_253238	0.295143	Ethanoligenens
TaxID_11050	0.249143	Flaviviridae
TaxID_84992	0.236429	Acidimicrobiia

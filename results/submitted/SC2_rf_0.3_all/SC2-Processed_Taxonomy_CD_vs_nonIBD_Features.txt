TaxonomyID	Importance_Optional	Description
TaxID_271098	0.214286	Shewanella halifaxensis
TaxID_292800	0.232143	Flavonifractor plautii
TaxID_1580596	0.181857	Phaeobacter piscinae
TaxID_161675	0.242143	Tamana bat virus
TaxID_1055487	0.245286	Methylotenera versatilis
TaxID_1273687	0.207143	Mycobacterium sp. VKM Ac-1817D
TaxID_2218628	0.218143	Photorhabdus laumondii
TaxID_204039	0.282	Dickeya dianthicola
TaxID_2081703	0.234286	Peptostreptococcaceae bacterium oral taxon 929
TaxID_1816219	0.308286	Colwellia sp. PAMC 21821
TaxID_853	0.371	Faecalibacterium prausnitzii
TaxID_1778262	0.195	Candidatus Doolittlea endobia
TaxID_208479	0.332714	[Clostridium] bolteae
TaxID_76517	0.213714	Campylobacter hominis
TaxID_1853276	0.178143	Neisseria sp. 10022
TaxID_885867	0.184143	Prochlorococcus phage P-SSP10
TaxID_39491	0.352286	[Eubacterium] rectale
TaxID_698828	0.217143	Kushneria konosiri
TaxID_1834196	0.341857	Lachnoclostridium sp. YL32
TaxID_143393	0.199	[Eubacterium] sulci
TaxID_43659	0.219143	Pseudoalteromonas tetraodonis
TaxID_158841	0.261571	Leminorella richardii
TaxID_44822	0.195143	Microcystis viridis
TaxID_649756	0.392286	Anaerostipes hadrus
TaxID_585423	0.211857	Synechococcus sp. KORDI-49
TaxID_39485	0.245429	[Eubacterium] eligens
TaxID_1680	0.218571	Bifidobacterium adolescentis
TaxID_1850252	0.255143	Tenacibaculum todarodis
TaxID_1160721	0.293714	Ruminococcus bicirculans
TaxID_216851	0.372286	Faecalibacterium
TaxID_1906657	0.199429	Candidatus Doolittlea
TaxID_1506553	0.327714	Lachnoclostridium
TaxID_1730	0.246143	Eubacterium
TaxID_11051	0.246	Flavivirus
TaxID_848	0.196714	Fusobacterium
TaxID_1847725	0.188714	Lawsonella
TaxID_456826	0.283857	Candidatus Cloacimonas
TaxID_946234	0.195143	Flavonifractor
TaxID_207244	0.348286	Anaerostipes
TaxID_253238	0.284571	Ethanoligenens
TaxID_1263	0.332	Ruminococcus
TaxID_186806	0.246143	Eubacteriaceae
TaxID_203492	0.201571	Fusobacteriaceae
TaxID_543	0.223714	Enterobacteriaceae
TaxID_11050	0.230286	Flaviviridae
TaxID_91347	0.229714	Enterobacterales
TaxID_203491	0.200286	Fusobacteriales
TaxID_1236	0.256714	Gammaproteobacteria
TaxID_203490	0.210571	Fusobacteriia
TaxID_1224	0.264714	Proteobacteria
TaxID_32066	0.222286	Fusobacteria

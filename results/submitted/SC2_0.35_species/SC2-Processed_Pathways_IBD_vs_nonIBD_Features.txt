PathwayID	Importance_Optional	Description
PathID_20	0.295571	PWY-7196: superpathway of pyrimidine ribonucleosides salvage|unclassified
PathID_799	0.32	UNINTEGRATED|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_993	0.236571	PWY-6277: superpathway of 5-aminoimidazole ribonucleotide biosynthesis|g__Eubacterium.s__Eubacterium_ventriosum
PathID_1073	0.253	PANTO-PWY: phosphopantothenate biosynthesis I|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_1096	0.206	PWY-7220: adenosine deoxyribonucleotides de novo biosynthesis II|unclassified
PathID_1221	0.271143	PWY-7111: pyruvate fermentation to isobutanol (engineered)|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_1522	0.214143	PWY0-1296: purine ribonucleosides degradation|g__Clostridium.s__Clostridium_clostridioforme
PathID_1935	0.215429	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Parabacteroides.s__Parabacteroides_merdae
PathID_2268	0.218143	FASYN-ELONG-PWY: fatty acid elongation -- saturated|unclassified
PathID_2359	0.263286	PWY-6737: starch degradation V|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_2503	0.231	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Ruminococcus.s__Ruminococcus_callidus
PathID_2806	0.189143	PWY-6163: chorismate biosynthesis from 3-dehydroquinate|g__Clostridium.s__Clostridium_clostridioforme
PathID_2823	0.246	PWY-6122: 5-aminoimidazole ribonucleotide biosynthesis II|g__Eubacterium.s__Eubacterium_ventriosum
PathID_3140	0.282714	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_3197	0.318429	UNINTEGRATED|g__Eubacterium.s__Eubacterium_ventriosum
PathID_3351	0.228714	PWY-5989: stearate biosynthesis II (bacteria and plants)|unclassified
PathID_3373	0.255429	PWY-5097: L-lysine biosynthesis VI|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_3686	0.294286	PWY-5121: superpathway of geranylgeranyl diphosphate biosynthesis II (via MEP)|unclassified
PathID_3719	0.255714	GLUCUROCAT-PWY: superpathway of &beta;-D-glucuronide and D-glucuronate degradation|unclassified
PathID_3902	0.258143	PWY-6936: seleno-amino acid biosynthesis|unclassified
PathID_3912	0.246857	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Parabacteroides.s__Parabacteroides_merdae
PathID_3983	0.214714	PWY0-862: (5Z)-dodec-5-enoate biosynthesis|unclassified
PathID_4435	0.257429	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_4582	0.252286	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_4608	0.217857	PWY-7664: oleate biosynthesis IV (anaerobic)|unclassified
PathID_4610	0.226	PWY-6151: S-adenosyl-L-methionine cycle I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_5235	0.210286	PWY-6282: palmitoleate biosynthesis I (from (5Z)-dodec-5-enoate)|unclassified
PathID_5390	0.186857	PWY-6527: stachyose degradation|g__Faecalibacterium.s__Faecalibacterium_prausnitzii
PathID_5645	0.273857	ARO-PWY: chorismate biosynthesis I|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_5753	0.217429	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Eubacterium.s__Eubacterium_ventriosum
PathID_5759	0.201714	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Eubacterium.s__Eubacterium_ventriosum
PathID_6055	0.213	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Parabacteroides.s__Parabacteroides_merdae
PathID_6087	0.251857	PANTO-PWY: phosphopantothenate biosynthesis I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_6197	0.249429	NONMEVIPP-PWY: methylerythritol phosphate pathway I|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_6440	0.244	PWY-6168: flavin biosynthesis III (fungi)|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_6892	0.232429	PWY-2942: L-lysine biosynthesis III|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_6955	0.281286	COMPLETE-ARO-PWY: superpathway of aromatic amino acid biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_7244	0.235571	PWY-6737: starch degradation V|g__Clostridium.s__Clostridium_clostridioforme
PathID_7343	0.234286	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Clostridium.s__Clostridium_clostridioforme
PathID_7399	0.248571	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Clostridium.s__Clostridium_clostridioforme
PathID_8063	0.224571	PWYG-321: mycolate biosynthesis|unclassified
PathID_8389	0.314571	PWY-6590: superpathway of Clostridium acetobutylicum acidogenic fermentation|unclassified
PathID_8670	0.288143	PWY-6163: chorismate biosynthesis from 3-dehydroquinate|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_8914	0.302714	PWY-7211: superpathway of pyrimidine deoxyribonucleotides de novo biosynthesis|unclassified
PathID_9468	0.305429	GALACT-GLUCUROCAT-PWY: superpathway of hexuronide and hexuronate degradation|unclassified
PathID_9507	0.326143	CENTFERM-PWY: pyruvate fermentation to butanoate|unclassified
PathID_9780	0.228143	PWY-6737: starch degradation V|g__Ruminococcus.s__Ruminococcus_bromii
PathID_9948	0.292	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_10367	0.231857	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Eubacterium.s__Eubacterium_ventriosum
PathID_10764	0.197143	PWY-6609: adenine and adenosine salvage III|g__Eubacterium.s__Eubacterium_ventriosum
PathID_10975	0.177143	COMPLETE-ARO-PWY: superpathway of aromatic amino acid biosynthesis|g__Clostridium.s__Clostridium_clostridioforme
PathID_11074	0.196143	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Clostridium.s__Clostridium_clostridioforme
PathID_11110	0.344286	CRNFORCAT-PWY: creatinine degradation I|unclassified
PathID_11323	0.253	VALSYN-PWY: L-valine biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_12264	0.359429	PWY-7357: thiamin formation from pyrithiamine and oxythiamine (yeast)|unclassified
PathID_12502	0.224857	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Clostridium.s__Clostridium_clostridioforme

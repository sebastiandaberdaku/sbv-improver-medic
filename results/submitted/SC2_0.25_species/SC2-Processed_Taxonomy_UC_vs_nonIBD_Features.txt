TaxonomyID	Importance_Optional	Description
TaxID_287	0.143286	Pseudomonas aeruginosa
TaxID_708186	0.170286	Arcobacter trophiarum
TaxID_441209	0.202	Rhodobaca barguzinensis
TaxID_1814957	0.167	Streptococcus virus 9871
TaxID_1825069	0.203429	Streptococcus marmotae
TaxID_132132	0.181714	Desulfomicrobium orale
TaxID_584	0.216286	Proteus mirabilis
TaxID_90410	0.188571	Streptococcus virus DT1
TaxID_33025	0.292286	Phascolarctobacterium faecium
TaxID_2218628	0.157143	Photorhabdus laumondii
TaxID_689	0.314	Vibrio mediterranei
TaxID_45658	0.199429	Vibrio scophthalmi
TaxID_879	0.174571	Desulfovibrio gigas
TaxID_76860	0.26	Streptococcus constellatus
TaxID_631220	0.288857	Desulfovibrio sp. G11
TaxID_247523	0.149571	Pseudoalteromonas aliena
TaxID_585	0.163714	Proteus vulgaris
TaxID_1145276	0.303714	Lysinibacillus varians
TaxID_122420	0.239857	Thermococcus sp. P6
TaxID_657445	0.210143	Francisella noatunensis
TaxID_376219	0.175286	Arthrospira sp. PCC 8005
TaxID_490	0.209429	Neisseria sicca
TaxID_1262533	0.164143	Lactococcus phage 340
TaxID_33011	0.205143	Cutibacterium granulosum
TaxID_1509	0.232143	Clostridium sporogenes
TaxID_452	0.227143	Legionella spiritensis
TaxID_2502843	0.177286	Erythrobacter sp. HKB08
TaxID_208962	0.154	Escherichia albertii
TaxID_1406512	0.154571	Candidatus Methanomassiliicoccus intestinalis
TaxID_47715	0.151	Lactobacillus rhamnosus
TaxID_1160	0.335143	Planktothrix agardhii
TaxID_1747	0.405143	Cutibacterium acnes
TaxID_1262537	0.192286	Lactococcus phage P680
TaxID_28118	0.170714	Odoribacter splanchnicus
TaxID_1597	0.182571	Lactobacillus paracasei
TaxID_1148157	0.156143	Acinetobacter oleivorans
TaxID_1307956	0.159429	Leucania separata nucleopolyhedrovirus
TaxID_1725232	0.232857	Candidatus Desulfovibrio trichonymphae
TaxID_881	0.342	Desulfovibrio vulgaris
TaxID_367190	0.212	Yersinia similis
TaxID_29546	0.190286	Lawsonia intracellularis
TaxID_656179	0.158571	Pandoraea faecigallinarum

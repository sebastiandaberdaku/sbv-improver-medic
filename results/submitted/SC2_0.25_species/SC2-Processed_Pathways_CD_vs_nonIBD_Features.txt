PathwayID	Importance_Optional	Description
PathID_20	0.244286	PWY-7196: superpathway of pyrimidine ribonucleosides salvage|unclassified
PathID_193	0.137286	PWY-6700: queuosine biosynthesis|g__Bacteroides.s__Bacteroides_sp_4_3_47FAA
PathID_292	0.155714	PWY-5686: UMP biosynthesis|g__Butyrivibrio.s__Butyrivibrio_crossotus
PathID_484	0.225857	PWY-6737: starch degradation V|unclassified
PathID_525	0.163286	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_594	0.177	PWY-5100: pyruvate fermentation to acetate and lactate II|g__Blautia.s__Ruminococcus_torques
PathID_799	0.285857	UNINTEGRATED|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_870	0.169143	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Eubacterium.s__Eubacterium_ventriosum
PathID_993	0.213286	PWY-6277: superpathway of 5-aminoimidazole ribonucleotide biosynthesis|g__Eubacterium.s__Eubacterium_ventriosum
PathID_1073	0.231429	PANTO-PWY: phosphopantothenate biosynthesis I|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_1077	0.141571	PWY-7111: pyruvate fermentation to isobutanol (engineered)|g__Parabacteroides.s__Parabacteroides_merdae
PathID_1096	0.158286	PWY-7220: adenosine deoxyribonucleotides de novo biosynthesis II|unclassified
PathID_1131	0.178429	PWY-6385: peptidoglycan biosynthesis III (mycobacteria)|g__Eubacterium.s__Eubacterium_ventriosum
PathID_1159	0.153571	PWY-5695: urate biosynthesis/inosine 5'-phosphate degradation|g__Barnesiella.s__Barnesiella_intestinihominis
PathID_1185	0.154429	METSYN-PWY: L-homoserine and L-methionine biosynthesis|unclassified
PathID_1221	0.238571	PWY-7111: pyruvate fermentation to isobutanol (engineered)|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_1275	0.139286	SER-GLYSYN-PWY: superpathway of L-serine and glycine biosynthesis I|g__Ruminococcus.s__Ruminococcus_bromii
PathID_1297	0.146714	PWY-6122: 5-aminoimidazole ribonucleotide biosynthesis II|g__Butyrivibrio.s__Butyrivibrio_crossotus
PathID_1339	0.151429	PWY-6121: 5-aminoimidazole ribonucleotide biosynthesis I|g__Butyrivibrio.s__Butyrivibrio_crossotus
PathID_1364	0.180286	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Bacteroides.s__Bacteroides_uniformis
PathID_1369	0.188429	HISTSYN-PWY: L-histidine biosynthesis|unclassified
PathID_1390	0.225286	PWY-5686: UMP biosynthesis|g__Bacteroides.s__Bacteroides_uniformis
PathID_1522	0.226	PWY0-1296: purine ribonucleosides degradation|g__Clostridium.s__Clostridium_clostridioforme
PathID_1535	0.126714	NONMEVIPP-PWY: methylerythritol phosphate pathway I|g__Bacteroides.s__Bacteroides_sp_4_3_47FAA
PathID_1571	0.195857	PWY-6122: 5-aminoimidazole ribonucleotide biosynthesis II|unclassified
PathID_1857	0.148714	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Butyrivibrio.s__Butyrivibrio_crossotus
PathID_1897	0.168714	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Parabacteroides.s__Parabacteroides_merdae
PathID_1935	0.248714	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Parabacteroides.s__Parabacteroides_merdae
PathID_2095	0.156714	PWY-5695: urate biosynthesis/inosine 5'-phosphate degradation|g__Bacteroides.s__Bacteroides_salyersiae
PathID_2155	0.159714	PWY-6151: S-adenosyl-L-methionine cycle I|g__Butyrivibrio.s__Butyrivibrio_crossotus
PathID_2268	0.188143	FASYN-ELONG-PWY: fatty acid elongation -- saturated|unclassified
PathID_2274	0.155	PWY-6277: superpathway of 5-aminoimidazole ribonucleotide biosynthesis|g__Butyrivibrio.s__Butyrivibrio_crossotus
PathID_2359	0.239857	PWY-6737: starch degradation V|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_2444	0.158714	UNINTEGRATED|g__Butyrivibrio.s__Butyrivibrio_crossotus
PathID_2445	0.186143	PWY-6317: galactose degradation I (Leloir pathway)|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_2501	0.146	NONOXIPENT-PWY: pentose phosphate pathway (non-oxidative branch)|g__Ruminococcus.s__Ruminococcus_bromii
PathID_2503	0.147	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Ruminococcus.s__Ruminococcus_callidus
PathID_2639	0.166143	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Oxalobacter.s__Oxalobacter_formigenes
PathID_2785	0.188714	PWY-6317: galactose degradation I (Leloir pathway)|g__Ruminococcus.s__Ruminococcus_bromii
PathID_2806	0.229571	PWY-6163: chorismate biosynthesis from 3-dehydroquinate|g__Clostridium.s__Clostridium_clostridioforme
PathID_2823	0.214571	PWY-6122: 5-aminoimidazole ribonucleotide biosynthesis II|g__Eubacterium.s__Eubacterium_ventriosum
PathID_2846	0.271429	PWY-5659: GDP-mannose biosynthesis|unclassified
PathID_2958	0.149429	PWY-6121: 5-aminoimidazole ribonucleotide biosynthesis I|g__Ruminococcus.s__Ruminococcus_bromii
PathID_3046	0.144857	VALSYN-PWY: L-valine biosynthesis|g__Parabacteroides.s__Parabacteroides_merdae
PathID_3099	0.175857	PWY0-1296: purine ribonucleosides degradation|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_3140	0.214714	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_3197	0.296429	UNINTEGRATED|g__Eubacterium.s__Eubacterium_ventriosum
PathID_3286	0.216857	PWY-6121: 5-aminoimidazole ribonucleotide biosynthesis I|g__Clostridium.s__Clostridium_clostridioforme
PathID_3351	0.215429	PWY-5989: stearate biosynthesis II (bacteria and plants)|unclassified
PathID_3373	0.225286	PWY-5097: L-lysine biosynthesis VI|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_3575	0.175857	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Ruminococcus.s__Ruminococcus_callidus
PathID_3577	0.198	PWY-6151: S-adenosyl-L-methionine cycle I|g__Bacteroides.s__Bacteroides_uniformis
PathID_3656	0.150714	PWY-6122: 5-aminoimidazole ribonucleotide biosynthesis II|g__Ruminococcus.s__Ruminococcus_callidus
PathID_3686	0.269286	PWY-5121: superpathway of geranylgeranyl diphosphate biosynthesis II (via MEP)|unclassified
PathID_3719	0.180714	GLUCUROCAT-PWY: superpathway of &beta;-D-glucuronide and D-glucuronate degradation|unclassified
PathID_3731	0.253286	UNINTEGRATED|g__Blautia.s__Ruminococcus_obeum
PathID_3758	0.182	PANTO-PWY: phosphopantothenate biosynthesis I|g__Eubacterium.s__Eubacterium_ramulus
PathID_3768	0.201	PWY-6277: superpathway of 5-aminoimidazole ribonucleotide biosynthesis|unclassified
PathID_3774	0.153286	PWY-6700: queuosine biosynthesis|g__Barnesiella.s__Barnesiella_intestinihominis
PathID_3809	0.125429	PWY-5686: UMP biosynthesis|g__Ruminococcus.s__Ruminococcus_bromii
PathID_3814	0.147857	VALSYN-PWY: L-valine biosynthesis|g__Butyrivibrio.s__Butyrivibrio_crossotus
PathID_3902	0.279571	PWY-6936: seleno-amino acid biosynthesis|unclassified
PathID_3912	0.263143	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Parabacteroides.s__Parabacteroides_merdae
PathID_3983	0.181	PWY0-862: (5Z)-dodec-5-enoate biosynthesis|unclassified
PathID_4123	0.207857	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Eubacterium.s__Eubacterium_ramulus
PathID_4139	0.152429	PWY-6163: chorismate biosynthesis from 3-dehydroquinate|g__Eubacterium.s__Eubacterium_ventriosum
PathID_4210	0.193857	PYRIDOXSYN-PWY: pyridoxal 5'-phosphate biosynthesis I|g__Bacteroides.s__Bacteroides_uniformis
PathID_4305	0.146143	PWY-6277: superpathway of 5-aminoimidazole ribonucleotide biosynthesis|g__Clostridium.s__Clostridium_bolteae
PathID_4427	0.244143	PWY-621: sucrose degradation III (sucrose invertase)|unclassified
PathID_4435	0.203143	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_4498	0.176286	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_4582	0.203714	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_4590	0.175714	PWY-7237: myo-, chiro- and scillo-inositol degradation|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_4604	0.118857	PWY-4242: pantothenate and coenzyme A biosynthesis III|g__Eubacterium.s__Eubacterium_ventriosum
PathID_4608	0.190286	PWY-7664: oleate biosynthesis IV (anaerobic)|unclassified
PathID_4610	0.193286	PWY-6151: S-adenosyl-L-methionine cycle I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_4653	0.144286	PWY-6700: queuosine biosynthesis|g__Butyrivibrio.s__Butyrivibrio_crossotus
PathID_4666	0.139571	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Bacteroides.s__Bacteroides_intestinalis
PathID_4737	0.229429	UNINTEGRATED|g__Clostridium.s__Clostridium_clostridioforme
PathID_4879	0.159	VALSYN-PWY: L-valine biosynthesis|g__Erysipelotrichaceae_noname.s__Clostridium_ramosum
PathID_4901	0.139286	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_4962	0.146857	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Butyrivibrio.s__Butyrivibrio_crossotus
PathID_4985	0.203571	PWY-7228: superpathway of guanosine nucleotides de novo biosynthesis I|g__Bacteroides.s__Bacteroides_intestinalis
PathID_5090	0.149714	PWY-6737: starch degradation V|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_5104	0.122143	UNINTEGRATED|g__Bacteroides.s__Bacteroides_sp_4_3_47FAA
PathID_5121	0.138857	PANTO-PWY: phosphopantothenate biosynthesis I|g__Bacteroides.s__Bacteroides_salyersiae
PathID_5152	0.228143	ARO-PWY: chorismate biosynthesis I|g__Clostridium.s__Clostridium_clostridioforme
PathID_5222	0.147571	PWY-5188: tetrapyrrole biosynthesis I (from glutamate)|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_5235	0.189857	PWY-6282: palmitoleate biosynthesis I (from (5Z)-dodec-5-enoate)|unclassified
PathID_5314	0.142286	PANTOSYN-PWY: pantothenate and coenzyme A biosynthesis I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_5354	0.148286	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Butyrivibrio.s__Butyrivibrio_crossotus
PathID_5455	0.178571	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Bacteroides.s__Bacteroides_intestinalis
PathID_5494	0.222429	UNINTEGRATED|g__Dorea.s__Dorea_formicigenerans
PathID_5595	0.190143	PWY66-422: D-galactose degradation V (Leloir pathway)|g__Ruminococcus.s__Ruminococcus_bromii
PathID_5603	0.183143	PWY-3841: folate transformations II|g__Eubacterium.s__Eubacterium_rectale
PathID_5645	0.209429	ARO-PWY: chorismate biosynthesis I|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_5753	0.188857	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Eubacterium.s__Eubacterium_ventriosum
PathID_5759	0.178714	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Eubacterium.s__Eubacterium_ventriosum
PathID_5763	0.200143	HSERMETANA-PWY: L-methionine biosynthesis III|unclassified
PathID_5791	0.166571	VALSYN-PWY: L-valine biosynthesis|g__Ruminococcus.s__Ruminococcus_bromii
PathID_5849	0.175714	P461-PWY: hexitol fermentation to lactate, formate, ethanol and acetate|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_5951	0.129429	PWY-5695: urate biosynthesis/inosine 5'-phosphate degradation|g__Bacteroides.s__Bacteroides_sp_4_3_47FAA
PathID_5971	0.142143	COMPLETE-ARO-PWY: superpathway of aromatic amino acid biosynthesis|g__Butyrivibrio.s__Butyrivibrio_crossotus
PathID_6008	0.173143	1CMET2-PWY: N10-formyl-tetrahydrofolate biosynthesis|g__Eubacterium.s__Eubacterium_rectale
PathID_6055	0.213857	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Parabacteroides.s__Parabacteroides_merdae
PathID_6087	0.213143	PANTO-PWY: phosphopantothenate biosynthesis I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_6197	0.198857	NONMEVIPP-PWY: methylerythritol phosphate pathway I|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_6198	0.202286	POLYAMINSYN3-PWY: superpathway of polyamine biosynthesis II|unclassified
PathID_6338	0.225857	PWY-2942: L-lysine biosynthesis III|g__Faecalibacterium.s__Faecalibacterium_prausnitzii
PathID_6434	0.147143	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Clostridium.s__Clostridium_clostridioforme
PathID_6440	0.193286	PWY-6168: flavin biosynthesis III (fungi)|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_6558	0.148714	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Bacteroides.s__Bacteroides_salyersiae
PathID_6655	0.18	PWY-5097: L-lysine biosynthesis VI|g__Parabacteroides.s__Parabacteroides_merdae
PathID_6737	0.156	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Eubacterium.s__Eubacterium_ramulus
PathID_6820	0.179571	PWY-5695: urate biosynthesis/inosine 5'-phosphate degradation|g__Bacteroides.s__Bacteroides_intestinalis
PathID_6864	0.197571	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Bacteroides.s__Bacteroides_uniformis
PathID_6892	0.2	PWY-2942: L-lysine biosynthesis III|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_6923	0.240143	PWY-6122: 5-aminoimidazole ribonucleotide biosynthesis II|g__Clostridium.s__Clostridium_clostridioforme
PathID_6924	0.159571	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Barnesiella.s__Barnesiella_intestinihominis
PathID_6955	0.231	COMPLETE-ARO-PWY: superpathway of aromatic amino acid biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_7027	0.158714	COA-PWY: coenzyme A biosynthesis I|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_7102	0.175	PWY-6121: 5-aminoimidazole ribonucleotide biosynthesis I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_7123	0.165286	PWY-6151: S-adenosyl-L-methionine cycle I|g__Ruminococcus.s__Ruminococcus_bromii
PathID_7223	0.167857	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Barnesiella.s__Barnesiella_intestinihominis
PathID_7239	0.182429	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Eubacterium.s__Eubacterium_ventriosum
PathID_7244	0.287714	PWY-6737: starch degradation V|g__Clostridium.s__Clostridium_clostridioforme
PathID_7326	0.148857	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Bacteroides.s__Bacteroides_salyersiae
PathID_7343	0.249714	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Clostridium.s__Clostridium_clostridioforme
PathID_7354	0.169429	PWY-5100: pyruvate fermentation to acetate and lactate II|g__Eubacterium.s__Eubacterium_ventriosum
PathID_7399	0.271143	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Clostridium.s__Clostridium_clostridioforme
PathID_7429	0.149	PWY-6737: starch degradation V|g__Eubacterium.s__Eubacterium_ventriosum
PathID_7492	0.136286	PWY-2942: L-lysine biosynthesis III|g__Erysipelotrichaceae_noname.s__Clostridium_ramosum
PathID_7592	0.234429	PWY-7234: inosine-5'-phosphate biosynthesis III|unclassified
PathID_7593	0.198571	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Parabacteroides.s__Parabacteroides_merdae
PathID_7595	0.185429	P162-PWY: L-glutamate degradation V (via hydroxyglutarate)|unclassified
PathID_7644	0.178429	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Parabacteroides.s__Parabacteroides_merdae
PathID_7704	0.158714	RHAMCAT-PWY: L-rhamnose degradation I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_7766	0.232714	THISYNARA-PWY: superpathway of thiamin diphosphate biosynthesis III (eukaryotes)|unclassified
PathID_7812	0.243429	PWY-6277: superpathway of 5-aminoimidazole ribonucleotide biosynthesis|g__Clostridium.s__Clostridium_clostridioforme
PathID_7829	0.115143	PWY-6703: preQ0 biosynthesis|g__Bacteroides.s__Bacteroides_sp_4_3_47FAA
PathID_8063	0.203429	PWYG-321: mycolate biosynthesis|unclassified
PathID_8082	0.171286	HISTSYN-PWY: L-histidine biosynthesis|g__Ruminococcus.s__Ruminococcus_bromii
PathID_8205	0.1	PWY-6125: superpathway of guanosine nucleotides de novo biosynthesis II|g__Bacteroides.s__Bacteroides_sp_4_3_47FAA
PathID_8389	0.259	PWY-6590: superpathway of Clostridium acetobutylicum acidogenic fermentation|unclassified
PathID_8570	0.156714	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Ruminococcus.s__Ruminococcus_bromii
PathID_8670	0.222714	PWY-6163: chorismate biosynthesis from 3-dehydroquinate|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_8738	0.199	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Bacteroides.s__Bacteroides_intestinalis
PathID_8820	0.145714	SO4ASSIM-PWY: sulfate reduction I (assimilatory)|g__Akkermansia.s__Akkermansia_muciniphila
PathID_8914	0.260286	PWY-7211: superpathway of pyrimidine deoxyribonucleotides de novo biosynthesis|unclassified
PathID_8943	0.188	UNINTEGRATED|g__Eubacterium.s__Eubacterium_rectale
PathID_8985	0.139286	PWY-5188: tetrapyrrole biosynthesis I (from glutamate)|g__Ruminococcus.s__Ruminococcus_bromii
PathID_9093	0.185429	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Eubacterium.s__Eubacterium_ventriosum
PathID_9196	0.196286	PWY-2942: L-lysine biosynthesis III|g__Bacteroides.s__Bacteroides_uniformis
PathID_9215	0.165143	PWY-6588: pyruvate fermentation to acetone|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_9289	0.160143	PWY-7111: pyruvate fermentation to isobutanol (engineered)|g__Ruminococcus.s__Ruminococcus_bromii
PathID_9312	0.281429	PWY-6305: putrescine biosynthesis IV|unclassified
PathID_9468	0.241	GALACT-GLUCUROCAT-PWY: superpathway of hexuronide and hexuronate degradation|unclassified
PathID_9507	0.273143	CENTFERM-PWY: pyruvate fermentation to butanoate|unclassified
PathID_9536	0.152143	PWY-6936: seleno-amino acid biosynthesis|g__Eubacterium.s__Eubacterium_ventriosum
PathID_9541	0.226286	PWY0-781: aspartate superpathway|unclassified
PathID_9552	0.143857	PWY-6609: adenine and adenosine salvage III|unclassified
PathID_9644	0.133429	COA-PWY: coenzyme A biosynthesis I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_9780	0.192857	PWY-6737: starch degradation V|g__Ruminococcus.s__Ruminococcus_bromii
PathID_9911	0.188286	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Parabacteroides.s__Parabacteroides_merdae
PathID_9918	0.187	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Eubacterium.s__Eubacterium_ventriosum
PathID_9926	0.176714	UNINTEGRATED|g__Ruminococcus.s__Ruminococcus_bromii
PathID_9948	0.274571	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_9954	0.207	UNINTEGRATED|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_9970	0.174429	HOMOSER-METSYN-PWY: L-methionine biosynthesis I|g__Ruminococcus.s__Ruminococcus_bromii
PathID_10244	0.158571	PWY-6121: 5-aminoimidazole ribonucleotide biosynthesis I|g__Faecalibacterium.s__Faecalibacterium_prausnitzii
PathID_10367	0.205429	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Eubacterium.s__Eubacterium_ventriosum
PathID_10439	0.177143	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_10497	0.164143	ARO-PWY: chorismate biosynthesis I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_10764	0.187857	PWY-6609: adenine and adenosine salvage III|g__Eubacterium.s__Eubacterium_ventriosum
PathID_10875	0.181714	PWY-5686: UMP biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_10941	0.155286	CALVIN-PWY: Calvin-Benson-Bassham cycle|g__Ruminococcus.s__Ruminococcus_bromii
PathID_10960	0.154	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Eubacterium.s__Eubacterium_ramulus
PathID_10975	0.237	COMPLETE-ARO-PWY: superpathway of aromatic amino acid biosynthesis|g__Clostridium.s__Clostridium_clostridioforme
PathID_11074	0.202286	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Clostridium.s__Clostridium_clostridioforme
PathID_11110	0.271857	CRNFORCAT-PWY: creatinine degradation I|unclassified
PathID_11151	0.127429	PWY-5695: urate biosynthesis/inosine 5'-phosphate degradation|g__Eubacterium.s__Eubacterium_ventriosum
PathID_11152	0.15	PWY-6703: preQ0 biosynthesis|g__Erysipelotrichaceae_noname.s__Clostridium_ramosum
PathID_11179	0.164	PRPP-PWY: superpathway of histidine, purine, and pyrimidine biosynthesis|unclassified
PathID_11280	0.173714	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Bacteroides.s__Bacteroides_uniformis
PathID_11323	0.250143	VALSYN-PWY: L-valine biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_11326	0.122714	ILEUSYN-PWY: L-isoleucine biosynthesis I (from threonine)|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_11512	0.237857	PWY-5097: L-lysine biosynthesis VI|g__Bacteroides.s__Bacteroides_uniformis
PathID_11797	0.192571	PWY-6121: 5-aminoimidazole ribonucleotide biosynthesis I|unclassified
PathID_11848	0.153286	PWY-5686: UMP biosynthesis|g__Bacteroides.s__Bacteroides_intestinalis
PathID_11859	0.151143	PANTO-PWY: phosphopantothenate biosynthesis I|g__Butyrivibrio.s__Butyrivibrio_crossotus
PathID_11909	0.134143	ARGININE-SYN4-PWY: L-ornithine de novo  biosynthesis|g__Bacteroides.s__Bacteroides_salyersiae
PathID_11942	0.143286	PWY-6703: preQ0 biosynthesis|g__Butyrivibrio.s__Butyrivibrio_crossotus
PathID_12072	0.158857	PWY-7111: pyruvate fermentation to isobutanol (engineered)|g__Erysipelotrichaceae_noname.s__Clostridium_ramosum
PathID_12257	0.133571	PWY0-1296: purine ribonucleosides degradation|g__Eubacterium.s__Eubacterium_ventriosum
PathID_12264	0.378286	PWY-7357: thiamin formation from pyrithiamine and oxythiamine (yeast)|unclassified
PathID_12364	0.215429	UNINTEGRATED|g__Parabacteroides.s__Parabacteroides_merdae
PathID_12502	0.252571	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Clostridium.s__Clostridium_clostridioforme
PathID_12520	0.191143	PWY-6305: putrescine biosynthesis IV|g__Faecalibacterium.s__Faecalibacterium_prausnitzii

TaxonomyID	Importance_Optional	Description
TaxID_1500686	0.11	Pseudomonas sp. Os17
TaxID_287	0.069	Pseudomonas aeruginosa
TaxID_28104	0.184857	Mesorhizobium huakuii
TaxID_2261	0.077286	Pyrococcus furiosus
TaxID_1789762	0.082	Methanosphaera sp. BMS
TaxID_712368	0.086	Leptotrichia sp. oral taxon 498
TaxID_766224	0.096286	Vibrio jasicida
TaxID_1392	0.071429	Bacillus anthracis
TaxID_788	0.078	Rickettsia canadensis
TaxID_2173	0.060571	Methanobrevibacter smithii
TaxID_1315974	0.129429	Sphingobium sp. TKS
TaxID_1163710	0.048429	Cronobacter condimenti
TaxID_1678728	0.165	Flavobacterium kingsejongi
TaxID_28119	0.054714	Bacteroides zoogleoformans
TaxID_296	0.147857	Pseudomonas fragi
TaxID_1597976	0.067571	Enterococcus phage EFDG1
TaxID_182210	0.040429	Pseudodesulfovibrio aespoeensis
TaxID_1513258	0.206	Gammapapillomavirus 13
TaxID_1580596	0.128571	Phaeobacter piscinae
TaxID_46429	0.077714	Sphingobium chlorophenolicum
TaxID_203263	0.048	Corynebacterium aquilae
TaxID_1262536	0.053714	Lactococcus phage jm3
TaxID_1933910	0.169857	Bartonella sp. A1379B
TaxID_1814957	0.168	Streptococcus virus 9871
TaxID_106654	0.102286	Acinetobacter nosocomialis
TaxID_80869	0.153857	Acidovorax citrulli
TaxID_29423	0.076714	Legionella oakridgensis
TaxID_217	0.075571	Helicobacter mustelae
TaxID_1051631	0.086571	Streptococcus phage YMC-2011
TaxID_1522092	0.087286	Clostridium phage phiCDHM19
TaxID_331696	0.059	Pseudolabrys taiwanensis
TaxID_2252	0.064857	Haloferax mediterranei
TaxID_297	0.075	Hydrogenophilus thermoluteolus
TaxID_1327989	0.156143	Serratia sp. FS14
TaxID_81475	0.071	Frateuria aurantia
TaxID_53952	0.077	Thermococcus peptonophilus
TaxID_1028989	0.042571	Pseudomonas sp. StFLB209
TaxID_1147	0.070857	Synechocystis sp. PCC 6714
TaxID_1173283	0.118857	Pseudomonas sp. R3-18-08
TaxID_1825069	0.082143	Streptococcus marmotae
TaxID_332056	0.051429	Sphingobium japonicum
TaxID_132132	0.047286	Desulfomicrobium orale
TaxID_135569	0.058571	Helicobacter apodemus
TaxID_83816	0.093857	Methanobrevibacter ruminantium
TaxID_2104	0.128571	Mycoplasma pneumoniae
TaxID_233316	0.106143	Alteromonas stellipolaris
TaxID_38288	0.158429	Corynebacterium genitalium
TaxID_60893	0.051	Desulfobacca acetoxidans
TaxID_1796613	0.060286	Bacteroides caecimuris
TaxID_1636603	0.078429	Acinetobacter sp. ACNIH1
TaxID_337	0.047143	Burkholderia glumae
TaxID_423605	0.036571	Elusimicrobium minutum
TaxID_53407	0.076143	Pseudomonas asplenii
TaxID_1982917	0.210571	Mycobacterium virus Gadjet
TaxID_887061	0.089	Methylovorus sp. MP688
TaxID_112023	0.158286	Streptococcus virus 7201
TaxID_29443	0.083571	Paucimonas lemoignei
TaxID_1636250	0.160857	Proteus phage vB_PmiM_Pm5461
TaxID_1288970	0.144857	Magnetospira sp. QH-2
TaxID_2501961	0.108714	Bat Hp-betacoronavirus Zhejiang2013
TaxID_1922390	0.155286	Beihai hermit crab virus 3
TaxID_1755504	0.096	Pseudomonas sp. DY-1
TaxID_145263	0.052286	Methanothermobacter marburgensis
TaxID_90410	0.229286	Streptococcus virus DT1
TaxID_1577791	0.053714	Candidatus Methanoplasma termitum
TaxID_33025	0.094857	Phascolarctobacterium faecium
TaxID_2303	0.068143	Thermoplasma acidophilum
TaxID_2107999	0.066571	Lactobacillus paragasseri
TaxID_45658	0.068714	Vibrio scophthalmi
TaxID_267375	0.198571	Pseudoalteromonas marina
TaxID_204039	0.057857	Dickeya dianthicola
TaxID_1178482	0.052429	Halomonas huangheensis
TaxID_1173026	0.057857	Gloeocapsa sp. PCC 7428
TaxID_485	0.049714	Neisseria gonorrhoeae
TaxID_33074	0.105286	Zymobacter palmae
TaxID_237609	0.056143	Pseudomonas alkylphenolica
TaxID_1564	0.061429	Desulfotomaculum ruminis
TaxID_1137606	0.053143	Leptospira mayottensis
TaxID_665914	0.052286	Mixta gaviniae
TaxID_1455075	0.164429	Citrobacter phage CR44b
TaxID_60548	0.072857	Paraburkholderia graminis
TaxID_739	0.070857	Aggregatibacter segnis
TaxID_142651	0.085	Mycoplasma phocidae
TaxID_28107	0.102571	Pseudoalteromonas espejiana
TaxID_76860	0.057857	Streptococcus constellatus
TaxID_247523	0.153143	Pseudoalteromonas aliena
TaxID_265883	0.095714	Hydrogenovibrio thermophilus
TaxID_1449437	0.090429	Pseudomonas phage vB_PaeP_Tr60_Ab31
TaxID_1718	0.056429	Corynebacterium glutamicum
TaxID_1505596	0.276286	Blochmannia endosymbiont of Polyrhachis (Hedomyrma) turneri
TaxID_330734	0.118429	Marinobacter psychrophilus
TaxID_28090	0.112857	Acinetobacter lwoffii
TaxID_64471	0.080143	Synechococcus sp. CC9311
TaxID_2172103	0.063143	Limnobaculum parvum
TaxID_58180	0.088714	Desulfovibrio alaskensis
TaxID_1837218	0.111	Salmonella phage phSE-2
TaxID_853	0.245	Faecalibacterium prausnitzii
TaxID_1987723	0.086286	Cellvibrio sp. PSBB006
TaxID_1561003	0.098286	bacterium 2013Arg42i
TaxID_63	0.070143	Vitreoscilla filiformis
TaxID_1159327	0.042429	Pontimonas salivibrio
TaxID_32051	0.079286	Synechococcus sp. WH 7803
TaxID_2170170	0.043714	Psipapillomavirus 2
TaxID_33995	0.066	Komagataeibacter europaeus
TaxID_1145276	0.043857	Lysinibacillus varians
TaxID_755307	0.057	Salinigranum rubrum
TaxID_1814958	0.162286	Streptococcus virus 9872
TaxID_28113	0.069714	Bacteroides heparinolyticus
TaxID_122420	0.089714	Thermococcus sp. P6
TaxID_28025	0.072571	Bifidobacterium animalis
TaxID_53399	0.050429	Hyphomicrobium denitrificans
TaxID_1908	0.089429	Streptomyces globisporus
TaxID_1977864	0.112	Candidatus Pelagibacter sp. RS39
TaxID_39775	0.079714	Methylomicrobium album
TaxID_2109558	0.113571	Halobacteriovorax sp. BALOs_7
TaxID_2072414	0.144	Stenotrophomonas sp. ESTM1D_MKCIP4_1
TaxID_1816218	0.208857	Colwellia sp. PAMC 20917
TaxID_208479	0.121143	[Clostridium] bolteae
TaxID_1986146	0.064857	Desulfobulbus oralis
TaxID_32054	0.049857	Calothrix parietina
TaxID_44930	0.061286	Natronobacterium gregoryi
TaxID_1796606	0.102714	Cupriavidus nantongensis
TaxID_376219	0.215714	Arthrospira sp. PCC 8005
TaxID_756277	0.072143	Synechococcus phage S-CBP2
TaxID_216465	0.056286	Polaromonas naphthalenivorans
TaxID_265959	0.056	Komagataeibacter saccharivorans
TaxID_1571470	0.089571	Rhizobium sp. ACO-34A
TaxID_28116	0.065143	Bacteroides ovatus
TaxID_56812	0.140857	Shewanella frigidimarina
TaxID_1450761	0.067571	Aneurinibacillus sp. XH2
TaxID_407020	0.045286	Sphingobium sp. MI1205
TaxID_357276	0.155	Bacteroides dorei
TaxID_698828	0.102571	Kushneria konosiri
TaxID_1288	0.071286	Staphylococcus xylosus
TaxID_1702221	0.067571	Faecalibaculum rodentium
TaxID_28109	0.048571	Pseudoalteromonas nigrifaciens
TaxID_2215	0.047286	Methanosarcina vacuolata
TaxID_1366	0.084571	Lactococcus raffinolactis
TaxID_136241	0.163714	Mycoplasma haemocanis
TaxID_2003121	0.070143	Sphingobacterium sp. G1-14
TaxID_1306274	0.097	Nostoc flagelliforme
TaxID_1262533	0.089286	Lactococcus phage 340
TaxID_754037	0.053429	Synechococcus phage S-CAM1
TaxID_55802	0.115857	Thermococcus barophilus
TaxID_158836	0.048429	Enterobacter hormaechei
TaxID_702114	0.063143	Methylomonas koyamae
TaxID_220697	0.076286	Thalassospira xiamenensis
TaxID_1434102	0.100857	Methanosarcina sp. WH1
TaxID_255248	0.066857	Leuconostoc garlicum
TaxID_106649	0.064286	Acinetobacter guillouiae
TaxID_1408191	0.113857	Corynebacterium deserti
TaxID_33011	0.160857	Cutibacterium granulosum
TaxID_1848755	0.160286	Actinobacteria bacterium IMCC26077
TaxID_217159	0.064143	Clostridium carboxidivorans
TaxID_864702	0.053857	Oscillatoriales cyanobacterium JSC-12
TaxID_585425	0.158286	Synechococcus sp. KORDI-52
TaxID_75612	0.095857	Pseudomonas mandelii
TaxID_10271	0.052714	Rabbit fibroma virus
TaxID_1766	0.1	Mycolicibacterium fortuitum
TaxID_1725	0.053143	Corynebacterium xerosis
TaxID_1981510	0.067857	Monoglobus pectinilyticus
TaxID_1763535	0.080429	Hydrogenophaga crassostreae
TaxID_1076588	0.092857	Thiolapillus brandeum
TaxID_2133959	0.158857	Bartonella sp. 'Tel Aviv'
TaxID_452	0.057857	Legionella spiritensis
TaxID_1141136	0.090286	Cronobacter phage vB_CsaM_GAP32
TaxID_2502843	0.120143	Erythrobacter sp. HKB08
TaxID_1211640	0.136571	Caulobacter phage CcrColossus
TaxID_246273	0.136143	Wolbachia endosymbiont of Cimex lectularius
TaxID_208962	0.165857	Escherichia albertii
TaxID_1462	0.073857	Geobacillus kaustophilus
TaxID_1406512	0.131429	Candidatus Methanomassiliicoccus intestinalis
TaxID_2490853	0.083286	Rhodococcus sp. NJ-530
TaxID_1206	0.124	Trichodesmium erythraeum
TaxID_562	0.071857	Escherichia coli
TaxID_36746	0.053143	Pseudomonas cichorii
TaxID_2487071	0.102714	Chryseobacterium sp. F5649
TaxID_33045	0.043857	Bartonella grahamii
TaxID_2176	0.055857	Methanohalophilus mahii
TaxID_2112	0.119571	Mycoplasma bovigenitalium
TaxID_71999	0.130714	Kocuria palustris
TaxID_2039	0.105	Tropheryma whipplei
TaxID_1270	0.167429	Micrococcus luteus
TaxID_1160	0.293571	Planktothrix agardhii
TaxID_1747	0.315	Cutibacterium acnes
TaxID_1283071	0.17	Vibrio phage JA-1
TaxID_183417	0.064143	Proteus hauseri
TaxID_797277	0.084571	Pseudomonas litoralis
TaxID_32049	0.072143	Synechococcus sp. PCC 7002
TaxID_1675603	0.088429	Citrobacter phage Michonne
TaxID_2014887	0.061429	Herbaspirillum robiniae
TaxID_395598	0.082429	Pseudomonas reinekei
TaxID_1262537	0.091857	Lactococcus phage P680
TaxID_246787	0.071714	Bacteroides cellulosilyticus
TaxID_450	0.095429	Legionella longbeachae
TaxID_1896196	0.053571	Porphyrobacter sp. LM 6
TaxID_1597	0.081286	Lactobacillus paracasei
TaxID_649756	0.123429	Anaerostipes hadrus
TaxID_585423	0.072429	Synechococcus sp. KORDI-49
TaxID_2264	0.108571	Thermococcus celer
TaxID_1926494	0.182286	Paraburkholderia sp. SOS3
TaxID_200991	0.284143	Planococcus rifietoensis
TaxID_1790137	0.079429	Wenyingzhuangia fucanilytica
TaxID_54077	0.087	Thermococcus barossii
TaxID_1982368	0.086571	Rhodococcus virus Pepy6
TaxID_64186	0.054571	Streptococcus virus Sfi21
TaxID_28452	0.102143	Leptospira alstonii
TaxID_2498451	0.113429	Rheinheimera sp. LHK132
TaxID_1867846	0.097429	Legionella clemsonensis
TaxID_1680	0.172857	Bifidobacterium adolescentis
TaxID_34018	0.108	Rhodospirillum centenum
TaxID_496866	0.086571	Thermoanaerobacter pseudethanolicus
TaxID_2257	0.090286	Natronomonas pharaonis
TaxID_640510	0.055429	Burkholderia sp. CCGE1001
TaxID_1717	0.057571	Corynebacterium diphtheriae
TaxID_795665	0.083714	Hydrogenophaga sp. PBC
TaxID_2169689	0.155571	Serratia virus MAM1
TaxID_2005388	0.080571	Pseudomonas sp. RU47
TaxID_2060307	0.062286	Enterococcus sp. FDAARGOS_375
TaxID_342949	0.128714	Pyrococcus sp. NA2
TaxID_2036206	0.083857	Aerococcaceae bacterium ZY16052
TaxID_2111	0.165714	Mycoplasma arthritidis
TaxID_86304	0.051857	Saccharophagus degradans
TaxID_1985873	0.058571	Sulfuriferula sp. AH1
TaxID_1567014	0.086286	Clostridium phage phiCT9441A
TaxID_1566990	0.164714	Streptococcus phage SpSL1
TaxID_1307956	0.077571	Leucania separata nucleopolyhedrovirus
TaxID_2231055	0.075143	Dechloromonas sp. HYN0024
TaxID_150123	0.082	Plantibacter flavus
TaxID_29430	0.057143	Acinetobacter haemolyticus
TaxID_1879023	0.240571	Mycobacterium sp. djl-10
TaxID_258224	0.050714	Corynebacterium resistens
TaxID_2093698	0.060714	Enterobacter sp. DKU_NT_01
TaxID_1528099	0.121714	Lawsonella clevelandensis
TaxID_40754	0.040857	Thioploca ingrica
TaxID_110164	0.056857	Thermococcus guaymasensis
TaxID_1725232	0.113571	Candidatus Desulfovibrio trichonymphae
TaxID_671223	0.087714	Propionibacterium sp. oral taxon 193
TaxID_2110	0.063429	Mycoplasma agalactiae
TaxID_881	0.126571	Desulfovibrio vulgaris
TaxID_1814960	0.055571	Streptococcus virus 9874
TaxID_28049	0.099429	Acidothermus cellulolyticus
TaxID_51161	0.071857	Actinobacillus delphinicola
TaxID_311400	0.200143	Thermococcus kodakarensis
TaxID_72638	0.125571	Streptococcus virus Sfi19
TaxID_29546	0.096714	Lawsonia intracellularis
TaxID_28123	0.148857	Porphyromonas asaccharolytica
TaxID_1850252	0.144	Tenacibaculum todarodis
TaxID_1936029	0.107571	Mycobacterium sp. MS1601
TaxID_1160721	0.245714	Ruminococcus bicirculans
TaxID_197575	0.049571	Haemophilus aegyptius

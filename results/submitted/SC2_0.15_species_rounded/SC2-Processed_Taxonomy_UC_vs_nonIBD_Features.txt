TaxonomyID	Importance_Optional	Description
TaxID_192843	0.147429	Rhodoferax ferrireducens
TaxID_287	0.143286	Pseudomonas aeruginosa
TaxID_28104	0.097857	Mesorhizobium huakuii
TaxID_222805	0.132429	Mycobacterium chimaera
TaxID_708186	0.170286	Arcobacter trophiarum
TaxID_1336794	0.076429	Formosa sp. Hel1_33_131
TaxID_754075	0.164571	Vibrio phage 11895-B1
TaxID_2109687	0.144571	Oscillibacter sp. PEA192
TaxID_1315974	0.082857	Sphingobium sp. TKS
TaxID_1678728	0.133	Flavobacterium kingsejongi
TaxID_296	0.153	Pseudomonas fragi
TaxID_1597976	0.104286	Enterococcus phage EFDG1
TaxID_441209	0.202	Rhodobaca barguzinensis
TaxID_118562	0.119143	Arthrospira platensis
TaxID_470	0.105	Acinetobacter baumannii
TaxID_1262536	0.116571	Lactococcus phage jm3
TaxID_1933910	0.168429	Bartonella sp. A1379B
TaxID_1814957	0.167	Streptococcus virus 9871
TaxID_1051631	0.102143	Streptococcus phage YMC-2011
TaxID_161675	0.070714	Tamana bat virus
TaxID_2479393	0.064571	Pseudomonas sp. LTGT-11-2Z
TaxID_1965382	0.118	Escherichia virus VpaE1
TaxID_1579	0.118714	Lactobacillus acidophilus
TaxID_1825069	0.203429	Streptococcus marmotae
TaxID_417	0.138429	Methylomonas clara
TaxID_132132	0.181714	Desulfomicrobium orale
TaxID_584	0.216286	Proteus mirabilis
TaxID_489	0.081857	Neisseria polysaccharea
TaxID_944547	0.08	Arcobacter sp. L
TaxID_53407	0.113857	Pseudomonas asplenii
TaxID_665884	0.107857	Lactococcus virus CB14
TaxID_1250153	0.095571	Maribacter sp. MAR_2009_60
TaxID_112023	0.124571	Streptococcus virus 7201
TaxID_1636250	0.164	Proteus phage vB_PmiM_Pm5461
TaxID_1930546	0.113143	Sporosarcina sp. P37
TaxID_1288970	0.136857	Magnetospira sp. QH-2
TaxID_2501961	0.161286	Bat Hp-betacoronavirus Zhejiang2013
TaxID_2267618	0.076714	Klebsiella sp. P1CD1
TaxID_1922390	0.151571	Beihai hermit crab virus 3
TaxID_1303	0.072571	Streptococcus oralis
TaxID_187327	0.103857	Acidaminococcus intestini
TaxID_54736	0.089143	Salmonella bongori
TaxID_1500757	0.077571	Pseudomonas phage phiPSA1
TaxID_90410	0.188571	Streptococcus virus DT1
TaxID_665883	0.102571	Lactococcus virus CB13
TaxID_33025	0.292286	Phascolarctobacterium faecium
TaxID_2218628	0.157143	Photorhabdus laumondii
TaxID_689	0.314	Vibrio mediterranei
TaxID_45658	0.199429	Vibrio scophthalmi
TaxID_879	0.174571	Desulfovibrio gigas
TaxID_59754	0.165571	Alcanivorax borkumensis
TaxID_1570939	0.074286	Rhodococcus sp. 2G
TaxID_485	0.100857	Neisseria gonorrhoeae
TaxID_1455075	0.157857	Citrobacter phage CR44b
TaxID_1873990	0.124286	Escherichia phage vB_EcoM_Alf5
TaxID_1848904	0.134286	Capnocytophaga stomatis
TaxID_28107	0.092	Pseudoalteromonas espejiana
TaxID_1873959	0.074571	Pectobacterium phage PP90
TaxID_76860	0.26	Streptococcus constellatus
TaxID_631220	0.288857	Desulfovibrio sp. G11
TaxID_247523	0.149571	Pseudoalteromonas aliena
TaxID_174633	0.108143	Candidatus Kuenenia stuttgartiensis
TaxID_1449437	0.112143	Pseudomonas phage vB_PaeP_Tr60_Ab31
TaxID_1505596	0.115714	Blochmannia endosymbiont of Polyrhachis (Hedomyrma) turneri
TaxID_585	0.163714	Proteus vulgaris
TaxID_28090	0.131	Acinetobacter lwoffii
TaxID_64471	0.079571	Synechococcus sp. CC9311
TaxID_58180	0.103	Desulfovibrio alaskensis
TaxID_1837218	0.165	Salmonella phage phSE-2
TaxID_37692	0.105571	Candidatus Phytoplasma mali
TaxID_33940	0.115571	Geobacillus thermodenitrificans
TaxID_488142	0.097286	Serratia sp. SCBI
TaxID_1965383	0.119143	Salmonella virus UAB87
TaxID_35790	0.128714	Rickettsia japonica
TaxID_33995	0.095	Komagataeibacter europaeus
TaxID_1150389	0.130857	Flavobacteriaceae bacterium UJ101
TaxID_1231	0.092714	Nitrosospira multiformis
TaxID_1145276	0.303714	Lysinibacillus varians
TaxID_63118	0.132429	Lactococcus virus bIL170
TaxID_1596	0.117857	Lactobacillus gasseri
TaxID_1914992	0.089143	Streptomyces sp. DUT11
TaxID_1814958	0.160429	Streptococcus virus 9872
TaxID_122420	0.239857	Thermococcus sp. P6
TaxID_546	0.130429	Citrobacter freundii
TaxID_657445	0.210143	Francisella noatunensis
TaxID_1862962	0.061286	Lactococcus phage M6165
TaxID_44930	0.121143	Natronobacterium gregoryi
TaxID_1898684	0.098286	Pseudomonas sp. LPH1
TaxID_39950	0.090286	Dialister pneumosintes
TaxID_885867	0.111857	Prochlorococcus phage P-SSP10
TaxID_376219	0.175286	Arthrospira sp. PCC 8005
TaxID_1863007	0.114429	Shigella phage SHBML-50-1
TaxID_754	0.127714	Pasteurella dagmatis
TaxID_756277	0.143714	Synechococcus phage S-CBP2
TaxID_216465	0.093857	Polaromonas naphthalenivorans
TaxID_1571470	0.153571	Rhizobium sp. ACO-34A
TaxID_28116	0.107143	Bacteroides ovatus
TaxID_504	0.100571	Kingella kingae
TaxID_44577	0.125286	Nitrosomonas ureae
TaxID_1965385	0.116571	Escherichia virus wV8
TaxID_1262535	0.117143	Lactococcus phage jm2
TaxID_1761016	0.102	Paraburkholderia caffeinilytica
TaxID_2215	0.122143	Methanosarcina vacuolata
TaxID_490	0.209429	Neisseria sicca
TaxID_1262533	0.164143	Lactococcus phage 340
TaxID_754037	0.135857	Synechococcus phage S-CAM1
TaxID_1857100	0.111	Salmonella phage BPS15Q2
TaxID_158836	0.134429	Enterobacter hormaechei
TaxID_213775	0.134143	Lactococcus virus jj50
TaxID_33011	0.205143	Cutibacterium granulosum
TaxID_1965378	0.117429	Escherichia virus AYO145A
TaxID_494269	0.101143	Lactococcus virus Bibb29
TaxID_10271	0.133	Rabbit fibroma virus
TaxID_1509	0.232143	Clostridium sporogenes
TaxID_584609	0.115286	Tenacibaculum jejuense
TaxID_1961713	0.083429	Streptomyces sp. MOE7
TaxID_452	0.227143	Legionella spiritensis
TaxID_2502843	0.177286	Erythrobacter sp. HKB08
TaxID_1211640	0.13	Caulobacter phage CcrColossus
TaxID_208962	0.154	Escherichia albertii
TaxID_2494234	0.085	Sutterella megalosphaeroides
TaxID_1406512	0.154571	Candidatus Methanomassiliicoccus intestinalis
TaxID_1871681	0.134857	Lactococcus phage 50101
TaxID_2051905	0.105143	Enterobacter sp. CRENT-193
TaxID_67825	0.101571	Citrobacter rodentium
TaxID_409322	0.149571	Treponema pedis
TaxID_31532	0.107286	Lactococcus virus sk1
TaxID_47715	0.151	Lactobacillus rhamnosus
TaxID_169430	0.075857	Paraburkholderia hospita
TaxID_71999	0.151857	Kocuria palustris
TaxID_287412	0.108714	Lactococcus virus Sl4
TaxID_1270	0.140286	Micrococcus luteus
TaxID_1157948	0.132	Thermotoga sp. 2812B
TaxID_1160	0.335143	Planktothrix agardhii
TaxID_1747	0.405143	Cutibacterium acnes
TaxID_1283071	0.148571	Vibrio phage JA-1
TaxID_183417	0.101	Proteus hauseri
TaxID_1675603	0.114143	Citrobacter phage Michonne
TaxID_395598	0.130571	Pseudomonas reinekei
TaxID_1262537	0.192286	Lactococcus phage P680
TaxID_278153	0.093143	Mesorhizobium sp. WSM1497
TaxID_28118	0.170714	Odoribacter splanchnicus
TaxID_40216	0.162143	Acinetobacter radioresistens
TaxID_1597	0.182571	Lactobacillus paracasei
TaxID_2025949	0.082286	Herbaspirillum sp. meg3
TaxID_480	0.098857	Moraxella catarrhalis
TaxID_39765	0.074571	Hydrogenovibrio crunogenus
TaxID_75105	0.132286	Paraburkholderia caribensis
TaxID_1484693	0.110286	Rhodoferax saidenbachensis
TaxID_1965376	0.122857	Escherichia virus EC6
TaxID_1982368	0.115429	Rhodococcus virus Pepy6
TaxID_2498451	0.143571	Rheinheimera sp. LHK132
TaxID_948107	0.136571	Paraburkholderia sprentiae
TaxID_1965379	0.117714	Escherichia virus JH2
TaxID_456163	0.162571	Thermus parvatiensis
TaxID_640510	0.100857	Burkholderia sp. CCGE1001
TaxID_2015795	0.146	Klebsiella sp. LY
TaxID_1148157	0.156143	Acinetobacter oleivorans
TaxID_1965377	0.115571	Salmonella virus HB2014
TaxID_795665	0.12	Hydrogenophaga sp. PBC
TaxID_2169689	0.133429	Serratia virus MAM1
TaxID_658666	0.095857	Bacillus bombysepticus
TaxID_213769	0.135286	Lactococcus virus 712
TaxID_2099789	0.092714	Lactobacillus sp. CBA3606
TaxID_1532555	0.104143	Brevundimonas sp. DS20
TaxID_83129	0.126857	Lactococcus virus P008
TaxID_1307956	0.159429	Leucania separata nucleopolyhedrovirus
TaxID_2231055	0.101857	Dechloromonas sp. HYN0024
TaxID_28251	0.106571	Ornithobacterium rhinotracheale
TaxID_1879023	0.082571	Mycobacterium sp. djl-10
TaxID_1613	0.088857	Lactobacillus fermentum
TaxID_119394	0.153571	Pseudothermotoga thermarum
TaxID_2093698	0.110143	Enterobacter sp. DKU_NT_01
TaxID_1725232	0.232857	Candidatus Desulfovibrio trichonymphae
TaxID_1985310	0.115571	Salmonella virus FelixO1
TaxID_881	0.342	Desulfovibrio vulgaris
TaxID_1814960	0.165429	Streptococcus virus 9874
TaxID_1165134	0.109	Lactococcus virus ASCC191
TaxID_367190	0.212	Yersinia similis
TaxID_1262538	0.112429	Lactococcus phage phi7
TaxID_1985308	0.116571	Escherichia virus HY02
TaxID_72638	0.130714	Streptococcus virus Sfi19
TaxID_29546	0.190286	Lawsonia intracellularis
TaxID_656179	0.158571	Pandoraea faecigallinarum
TaxID_694060	0.117143	Staphylococcus phage SA1
TaxID_1936029	0.098	Mycobacterium sp. MS1601
TaxID_983548	0.083857	Dokdonia sp. 4H-3-7-5

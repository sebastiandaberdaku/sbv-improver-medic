TaxonomyID	Importance_Optional	Description
TaxID_441209	0.202	Rhodobaca barguzinensis
TaxID_132132	0.181714	Desulfomicrobium orale
TaxID_584	0.216286	Proteus mirabilis
TaxID_33025	0.292286	Phascolarctobacterium faecium
TaxID_689	0.314	Vibrio mediterranei
TaxID_76860	0.26	Streptococcus constellatus
TaxID_631220	0.288857	Desulfovibrio sp. G11
TaxID_1145276	0.303714	Lysinibacillus varians
TaxID_122420	0.239857	Thermococcus sp. P6
TaxID_657445	0.210143	Francisella noatunensis
TaxID_490	0.209429	Neisseria sicca
TaxID_33011	0.205143	Cutibacterium granulosum
TaxID_1509	0.232143	Clostridium sporogenes
TaxID_452	0.227143	Legionella spiritensis
TaxID_2502843	0.177286	Erythrobacter sp. HKB08
TaxID_1160	0.335143	Planktothrix agardhii
TaxID_1747	0.405143	Cutibacterium acnes
TaxID_1262537	0.192286	Lactococcus phage P680
TaxID_1597	0.182571	Lactobacillus paracasei
TaxID_1725232	0.232857	Candidatus Desulfovibrio trichonymphae
TaxID_881	0.342	Desulfovibrio vulgaris
TaxID_367190	0.212	Yersinia similis

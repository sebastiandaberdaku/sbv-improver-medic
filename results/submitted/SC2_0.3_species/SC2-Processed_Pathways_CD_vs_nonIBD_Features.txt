PathwayID	Importance_Optional	Description
PathID_20	0.244286	PWY-7196: superpathway of pyrimidine ribonucleosides salvage|unclassified
PathID_484	0.225857	PWY-6737: starch degradation V|unclassified
PathID_799	0.285857	UNINTEGRATED|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_870	0.169143	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Eubacterium.s__Eubacterium_ventriosum
PathID_993	0.213286	PWY-6277: superpathway of 5-aminoimidazole ribonucleotide biosynthesis|g__Eubacterium.s__Eubacterium_ventriosum
PathID_1073	0.231429	PANTO-PWY: phosphopantothenate biosynthesis I|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_1131	0.178429	PWY-6385: peptidoglycan biosynthesis III (mycobacteria)|g__Eubacterium.s__Eubacterium_ventriosum
PathID_1221	0.238571	PWY-7111: pyruvate fermentation to isobutanol (engineered)|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_1369	0.188429	HISTSYN-PWY: L-histidine biosynthesis|unclassified
PathID_1390	0.225286	PWY-5686: UMP biosynthesis|g__Bacteroides.s__Bacteroides_uniformis
PathID_1522	0.226	PWY0-1296: purine ribonucleosides degradation|g__Clostridium.s__Clostridium_clostridioforme
PathID_1571	0.195857	PWY-6122: 5-aminoimidazole ribonucleotide biosynthesis II|unclassified
PathID_1935	0.248714	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Parabacteroides.s__Parabacteroides_merdae
PathID_2268	0.188143	FASYN-ELONG-PWY: fatty acid elongation -- saturated|unclassified
PathID_2359	0.239857	PWY-6737: starch degradation V|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_2445	0.186143	PWY-6317: galactose degradation I (Leloir pathway)|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_2806	0.229571	PWY-6163: chorismate biosynthesis from 3-dehydroquinate|g__Clostridium.s__Clostridium_clostridioforme
PathID_2823	0.214571	PWY-6122: 5-aminoimidazole ribonucleotide biosynthesis II|g__Eubacterium.s__Eubacterium_ventriosum
PathID_2846	0.271429	PWY-5659: GDP-mannose biosynthesis|unclassified
PathID_3140	0.214714	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_3197	0.296429	UNINTEGRATED|g__Eubacterium.s__Eubacterium_ventriosum
PathID_3286	0.216857	PWY-6121: 5-aminoimidazole ribonucleotide biosynthesis I|g__Clostridium.s__Clostridium_clostridioforme
PathID_3351	0.215429	PWY-5989: stearate biosynthesis II (bacteria and plants)|unclassified
PathID_3373	0.225286	PWY-5097: L-lysine biosynthesis VI|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_3686	0.269286	PWY-5121: superpathway of geranylgeranyl diphosphate biosynthesis II (via MEP)|unclassified
PathID_3719	0.180714	GLUCUROCAT-PWY: superpathway of &beta;-D-glucuronide and D-glucuronate degradation|unclassified
PathID_3731	0.253286	UNINTEGRATED|g__Blautia.s__Ruminococcus_obeum
PathID_3768	0.201	PWY-6277: superpathway of 5-aminoimidazole ribonucleotide biosynthesis|unclassified
PathID_3902	0.279571	PWY-6936: seleno-amino acid biosynthesis|unclassified
PathID_3912	0.263143	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Parabacteroides.s__Parabacteroides_merdae
PathID_3983	0.181	PWY0-862: (5Z)-dodec-5-enoate biosynthesis|unclassified
PathID_4123	0.207857	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Eubacterium.s__Eubacterium_ramulus
PathID_4139	0.152429	PWY-6163: chorismate biosynthesis from 3-dehydroquinate|g__Eubacterium.s__Eubacterium_ventriosum
PathID_4427	0.244143	PWY-621: sucrose degradation III (sucrose invertase)|unclassified
PathID_4435	0.203143	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_4498	0.176286	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_4582	0.203714	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_4608	0.190286	PWY-7664: oleate biosynthesis IV (anaerobic)|unclassified
PathID_4610	0.193286	PWY-6151: S-adenosyl-L-methionine cycle I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_4737	0.229429	UNINTEGRATED|g__Clostridium.s__Clostridium_clostridioforme
PathID_4985	0.203571	PWY-7228: superpathway of guanosine nucleotides de novo biosynthesis I|g__Bacteroides.s__Bacteroides_intestinalis
PathID_5152	0.228143	ARO-PWY: chorismate biosynthesis I|g__Clostridium.s__Clostridium_clostridioforme
PathID_5235	0.189857	PWY-6282: palmitoleate biosynthesis I (from (5Z)-dodec-5-enoate)|unclassified
PathID_5494	0.222429	UNINTEGRATED|g__Dorea.s__Dorea_formicigenerans
PathID_5595	0.190143	PWY66-422: D-galactose degradation V (Leloir pathway)|g__Ruminococcus.s__Ruminococcus_bromii
PathID_5603	0.183143	PWY-3841: folate transformations II|g__Eubacterium.s__Eubacterium_rectale
PathID_5645	0.209429	ARO-PWY: chorismate biosynthesis I|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_5753	0.188857	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Eubacterium.s__Eubacterium_ventriosum
PathID_5759	0.178714	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Eubacterium.s__Eubacterium_ventriosum
PathID_5763	0.200143	HSERMETANA-PWY: L-methionine biosynthesis III|unclassified
PathID_5791	0.166571	VALSYN-PWY: L-valine biosynthesis|g__Ruminococcus.s__Ruminococcus_bromii
PathID_6055	0.213857	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Parabacteroides.s__Parabacteroides_merdae
PathID_6087	0.213143	PANTO-PWY: phosphopantothenate biosynthesis I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_6197	0.198857	NONMEVIPP-PWY: methylerythritol phosphate pathway I|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_6198	0.202286	POLYAMINSYN3-PWY: superpathway of polyamine biosynthesis II|unclassified
PathID_6338	0.225857	PWY-2942: L-lysine biosynthesis III|g__Faecalibacterium.s__Faecalibacterium_prausnitzii
PathID_6434	0.147143	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Clostridium.s__Clostridium_clostridioforme
PathID_6440	0.193286	PWY-6168: flavin biosynthesis III (fungi)|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_6655	0.18	PWY-5097: L-lysine biosynthesis VI|g__Parabacteroides.s__Parabacteroides_merdae
PathID_6923	0.240143	PWY-6122: 5-aminoimidazole ribonucleotide biosynthesis II|g__Clostridium.s__Clostridium_clostridioforme
PathID_6955	0.231	COMPLETE-ARO-PWY: superpathway of aromatic amino acid biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_7102	0.175	PWY-6121: 5-aminoimidazole ribonucleotide biosynthesis I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_7239	0.182429	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Eubacterium.s__Eubacterium_ventriosum
PathID_7244	0.287714	PWY-6737: starch degradation V|g__Clostridium.s__Clostridium_clostridioforme
PathID_7343	0.249714	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Clostridium.s__Clostridium_clostridioforme
PathID_7354	0.169429	PWY-5100: pyruvate fermentation to acetate and lactate II|g__Eubacterium.s__Eubacterium_ventriosum
PathID_7399	0.271143	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Clostridium.s__Clostridium_clostridioforme
PathID_7429	0.149	PWY-6737: starch degradation V|g__Eubacterium.s__Eubacterium_ventriosum
PathID_7592	0.234429	PWY-7234: inosine-5'-phosphate biosynthesis III|unclassified
PathID_7593	0.198571	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Parabacteroides.s__Parabacteroides_merdae
PathID_7644	0.178429	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Parabacteroides.s__Parabacteroides_merdae
PathID_7704	0.158714	RHAMCAT-PWY: L-rhamnose degradation I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_7766	0.232714	THISYNARA-PWY: superpathway of thiamin diphosphate biosynthesis III (eukaryotes)|unclassified
PathID_7812	0.243429	PWY-6277: superpathway of 5-aminoimidazole ribonucleotide biosynthesis|g__Clostridium.s__Clostridium_clostridioforme
PathID_8063	0.203429	PWYG-321: mycolate biosynthesis|unclassified
PathID_8389	0.259	PWY-6590: superpathway of Clostridium acetobutylicum acidogenic fermentation|unclassified
PathID_8670	0.222714	PWY-6163: chorismate biosynthesis from 3-dehydroquinate|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_8738	0.199	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Bacteroides.s__Bacteroides_intestinalis
PathID_8914	0.260286	PWY-7211: superpathway of pyrimidine deoxyribonucleotides de novo biosynthesis|unclassified
PathID_9093	0.185429	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Eubacterium.s__Eubacterium_ventriosum
PathID_9312	0.281429	PWY-6305: putrescine biosynthesis IV|unclassified
PathID_9468	0.241	GALACT-GLUCUROCAT-PWY: superpathway of hexuronide and hexuronate degradation|unclassified
PathID_9507	0.273143	CENTFERM-PWY: pyruvate fermentation to butanoate|unclassified
PathID_9541	0.226286	PWY0-781: aspartate superpathway|unclassified
PathID_9780	0.192857	PWY-6737: starch degradation V|g__Ruminococcus.s__Ruminococcus_bromii
PathID_9911	0.188286	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Parabacteroides.s__Parabacteroides_merdae
PathID_9918	0.187	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Eubacterium.s__Eubacterium_ventriosum
PathID_9948	0.274571	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_10367	0.205429	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Eubacterium.s__Eubacterium_ventriosum
PathID_10497	0.164143	ARO-PWY: chorismate biosynthesis I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_10764	0.187857	PWY-6609: adenine and adenosine salvage III|g__Eubacterium.s__Eubacterium_ventriosum
PathID_10975	0.237	COMPLETE-ARO-PWY: superpathway of aromatic amino acid biosynthesis|g__Clostridium.s__Clostridium_clostridioforme
PathID_11074	0.202286	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Clostridium.s__Clostridium_clostridioforme
PathID_11110	0.271857	CRNFORCAT-PWY: creatinine degradation I|unclassified
PathID_11323	0.250143	VALSYN-PWY: L-valine biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_11512	0.237857	PWY-5097: L-lysine biosynthesis VI|g__Bacteroides.s__Bacteroides_uniformis
PathID_12264	0.378286	PWY-7357: thiamin formation from pyrithiamine and oxythiamine (yeast)|unclassified
PathID_12364	0.215429	UNINTEGRATED|g__Parabacteroides.s__Parabacteroides_merdae
PathID_12502	0.252571	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Clostridium.s__Clostridium_clostridioforme

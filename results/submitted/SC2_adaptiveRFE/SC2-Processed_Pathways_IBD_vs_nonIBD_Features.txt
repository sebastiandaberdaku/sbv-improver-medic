PathwayID	Importance_Optional	Description
PathID_799	0.008787	PWY-7196: superpathway of pyrimidine ribonucleosides salvage|unclassified
PathID_3197	0.007915	PWY0-1297: superpathway of purine deoxyribonucleosides degradation
PathID_9948	0.00666	PWY0-1298: superpathway of pyrimidine deoxyribonucleosides degradation|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_8670	0.006248	URDEGR-PWY: superpathway of allantoin degradation in plants
PathID_6955	0.006239	PWY-6703: preQ0 biosynthesis
PathID_3140	0.00615	PWY-5097: L-lysine biosynthesis VI|g__Ruminococcus.s__Ruminococcus_callidus
PathID_2359	0.005455	PWY-6124: inosine-5'-phosphate biosynthesis II|g__Coprococcus.s__Coprococcus_catus
PathID_2503	0.005433	UNINTEGRATED|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_5645	0.005359	PWY-6277: superpathway of 5-aminoimidazole ribonucleotide biosynthesis|g__Ruminococcus.s__Ruminococcus_callidus
PathID_9507	0.005109	PWY-6151: S-adenosyl-L-methionine cycle I|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_8914	0.004691	PWY-6277: superpathway of 5-aminoimidazole ribonucleotide biosynthesis|g__Eubacterium.s__Eubacterium_ventriosum
PathID_3373	0.004625	PANTO-PWY: phosphopantothenate biosynthesis I|g__Ruminococcus.s__Ruminococcus_callidus
PathID_9782	0.004481	PANTO-PWY: phosphopantothenate biosynthesis I|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_2531	0.004448	PWY-7111: pyruvate fermentation to isobutanol (engineered)|g__Parabacteroides.s__Parabacteroides_merdae
PathID_11683	0.004205	PWY-7220: adenosine deoxyribonucleotides de novo biosynthesis II|unclassified
PathID_1073	0.004187	PWY-5695: urate biosynthesis/inosine 5'-phosphate degradation|g__Barnesiella.s__Barnesiella_intestinihominis
PathID_1221	0.004142	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_8389	0.004133	METSYN-PWY: L-homoserine and L-methionine biosynthesis|unclassified
PathID_3351	0.004096	PWY-7111: pyruvate fermentation to isobutanol (engineered)|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_6197	0.004091	CENTFERM-PWY: pyruvate fermentation to butanoate
PathID_4582	0.004077	SER-GLYSYN-PWY: superpathway of L-serine and glycine biosynthesis I|g__Ruminococcus.s__Ruminococcus_bromii
PathID_995	0.004037	PWY66-409: superpathway of purine nucleotide salvage
PathID_585	0.004027	PWY0-1296: purine ribonucleosides degradation|g__Clostridium.s__Clostridium_clostridioforme
PathID_4435	0.00401	PWY-6122: 5-aminoimidazole ribonucleotide biosynthesis II|unclassified
PathID_11571	0.003963	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_8063	0.003878	HSERMETANA-PWY: L-methionine biosynthesis III
PathID_11323	0.003836	PWY-7357: thiamin formation from pyrithiamine and oxythiamine (yeast)|g__Roseburia.s__Roseburia_inulinivorans
PathID_11441	0.003674	PWY-6151: S-adenosyl-L-methionine cycle I|g__Ruminococcus.s__Ruminococcus_callidus
PathID_6892	0.003651	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Parabacteroides.s__Parabacteroides_merdae
PathID_6440	0.003565	PWY-5304: superpathway of sulfur oxidation (Acidianus ambivalens)
PathID_2268	0.003532	UNINTEGRATED|g__Erysipelotrichaceae_noname.s__Eubacterium_dolichum
PathID_9468	0.003437	FASYN-ELONG-PWY: fatty acid elongation -- saturated|unclassified
PathID_8685	0.003432	PWY-6737: starch degradation V|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_12502	0.003421	DTDPRHAMSYN-PWY: dTDP-L-rhamnose biosynthesis I|g__Clostridium.s__Clostridium_clostridioforme
PathID_10439	0.0034	NONOXIPENT-PWY: pentose phosphate pathway (non-oxidative branch)|g__Ruminococcus.s__Ruminococcus_bromii
PathID_3656	0.003391	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Ruminococcus.s__Ruminococcus_callidus
PathID_7343	0.003361	PWY-5861: superpathway of demethylmenaquinol-8 biosynthesis
PathID_5311	0.003331	PWY-6121: 5-aminoimidazole ribonucleotide biosynthesis I|g__Ruminococcus.s__Ruminococcus_callidus
PathID_7399	0.003323	PWY-6317: galactose degradation I (Leloir pathway)|g__Ruminococcus.s__Ruminococcus_bromii
PathID_1096	0.003316	PWY-6163: chorismate biosynthesis from 3-dehydroquinate|g__Clostridium.s__Clostridium_clostridioforme
PathID_4608	0.003306	PWY-6122: 5-aminoimidazole ribonucleotide biosynthesis II|g__Eubacterium.s__Eubacterium_ventriosum
PathID_828	0.003299	PWY-1042: glycolysis IV (plant cytosol)|g__Ruminococcus.s__Ruminococcus_bromii
PathID_3912	0.00328	VALSYN-PWY: L-valine biosynthesis|g__Parabacteroides.s__Parabacteroides_merdae
PathID_5235	0.003163	PWY0-1296: purine ribonucleosides degradation|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_1229	0.003154	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_3983	0.003095	UNINTEGRATED|g__Eubacterium.s__Eubacterium_ventriosum
PathID_4562	0.002941	PWY-6121: 5-aminoimidazole ribonucleotide biosynthesis I|g__Clostridium.s__Clostridium_clostridioforme
PathID_976	0.002941	PWY-6737: starch degradation V|g__Coprococcus.s__Coprococcus_catus
PathID_4427	0.002935	PWY-5989: stearate biosynthesis II (bacteria and plants)|unclassified
PathID_2136	0.002928	PWY-5097: L-lysine biosynthesis VI|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_10875	0.002927	PWY-5022: 4-aminobutanoate degradation V
PathID_3575	0.002793	1CMET2-PWY: N10-formyl-tetrahydrofolate biosynthesis
PathID_5699	0.00278	PANTO-PWY: phosphopantothenate biosynthesis I
PathID_1935	0.002722	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Ruminococcus.s__Ruminococcus_callidus
PathID_3719	0.002669	PWY-6122: 5-aminoimidazole ribonucleotide biosynthesis II|g__Ruminococcus.s__Ruminococcus_callidus
PathID_1880	0.002642	PWY-5121: superpathway of geranylgeranyl diphosphate biosynthesis II (via MEP)|unclassified
PathID_11179	0.00259	GLUCUROCAT-PWY: superpathway of &beta;-D-glucuronide and D-glucuronate degradation|unclassified
PathID_11074	0.002578	PWY-6277: superpathway of 5-aminoimidazole ribonucleotide biosynthesis|unclassified
PathID_171	0.002574	PWY-6936: seleno-amino acid biosynthesis|unclassified
PathID_6087	0.002563	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Parabacteroides.s__Parabacteroides_merdae
PathID_573	0.00255	PWY-6737: starch degradation V|g__Roseburia.s__Roseburia_inulinivorans
PathID_4561	0.002535	PWY0-862: (5Z)-dodec-5-enoate biosynthesis|unclassified
PathID_7027	0.002494	1CMET2-PWY: N10-formyl-tetrahydrofolate biosynthesis|g__Parabacteroides.s__Parabacteroides_merdae
PathID_9780	0.002475	PWY-7111: pyruvate fermentation to isobutanol (engineered)|g__Clostridium.s__Clostridium_clostridioforme
PathID_1522	0.002457	PWY-621: sucrose degradation III (sucrose invertase)|unclassified
PathID_7244	0.002433	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_1601	0.002428	PANTOSYN-PWY: pantothenate and coenzyme A biosynthesis I|unclassified
PathID_5222	0.002415	PWY-5188: tetrapyrrole biosynthesis I (from glutamate)|g__Ruminococcus.s__Ruminococcus_callidus
PathID_12520	0.002398	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Bacteroides.s__Bacteroides_intestinalis
PathID_7985	0.002396	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_9609	0.002374	PWY-7664: oleate biosynthesis IV (anaerobic)|unclassified
PathID_3099	0.002337	PWY-6151: S-adenosyl-L-methionine cycle I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_1173	0.002319	UNINTEGRATED|g__Clostridium.s__Clostridium_clostridioforme
PathID_11429	0.002304	PWY0-1296: purine ribonucleosides degradation|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_6055	0.002302	PWY-3841: folate transformations II|g__Parabacteroides.s__Parabacteroides_merdae
PathID_12264	0.002256	HISTSYN-PWY: L-histidine biosynthesis|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_7099	0.00223	ARO-PWY: chorismate biosynthesis I|g__Clostridium.s__Clostridium_clostridioforme
PathID_2806	0.002157	PWY-5188: tetrapyrrole biosynthesis I (from glutamate)|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_10367	0.002156	PWY-6282: palmitoleate biosynthesis I (from (5Z)-dodec-5-enoate)|unclassified
PathID_4567	0.002152	PWY-6590: superpathway of Clostridium acetobutylicum acidogenic fermentation
PathID_9614	0.002145	PWY-6527: stachyose degradation|g__Faecalibacterium.s__Faecalibacterium_prausnitzii
PathID_6923	0.002123	PWY-6609: adenine and adenosine salvage III|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_7812	0.002122	HOMOSER-METSYN-PWY: L-methionine biosynthesis I|unclassified
PathID_7593	0.002115	BRANCHED-CHAIN-AA-SYN-PWY: superpathway of branched amino acid biosynthesis|g__Pseudoflavonifractor.s__Pseudoflavonifractor_capillosus
PathID_11454	0.002092	PWY66-422: D-galactose degradation V (Leloir pathway)|g__Ruminococcus.s__Ruminococcus_bromii
PathID_8258	0.002053	ARO-PWY: chorismate biosynthesis I|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_563	0.002026	UNINTEGRATED|g__Ruminococcus.s__Ruminococcus_callidus
PathID_6434	0.001983	VALSYN-PWY: L-valine biosynthesis|g__Ruminococcus.s__Ruminococcus_bromii
PathID_9911	0.001975	MET-SAM-PWY: superpathway of S-adenosyl-L-methionine biosynthesis|unclassified
PathID_10975	0.001966	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Parabacteroides.s__Parabacteroides_merdae
PathID_3686	0.001949	PANTO-PWY: phosphopantothenate biosynthesis I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_8903	0.001937	NONMEVIPP-PWY: methylerythritol phosphate pathway I|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_5152	0.001925	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Clostridium.s__Clostridium_clostridioforme
PathID_6455	0.001914	PWY-6168: flavin biosynthesis III (fungi)|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_4372	0.001913	VALSYN-PWY: L-valine biosynthesis|g__Clostridium.s__Clostridium_clostridioforme
PathID_4737	0.001908	UNINTEGRATED|g__Barnesiella.s__Barnesiella_intestinihominis
PathID_12070	0.001907	CRNFORCAT-PWY: creatinine degradation I
PathID_6527	0.001898	PWY-6703: preQ0 biosynthesis|g__Bacteroides.s__Bacteroides_cellulosilyticus
PathID_5390	0.001874	PWY-2942: L-lysine biosynthesis III|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_3286	0.00184	PWY-6122: 5-aminoimidazole ribonucleotide biosynthesis II|g__Clostridium.s__Clostridium_clostridioforme
PathID_7123	0.001829	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Barnesiella.s__Barnesiella_intestinihominis
PathID_9215	0.001803	COMPLETE-ARO-PWY: superpathway of aromatic amino acid biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_9649	0.0018	COA-PWY: coenzyme A biosynthesis I|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_1841	0.001797	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Bacteroides.s__Bacteroides_intestinalis
PathID_3925	0.001784	UNINTEGRATED|g__Roseburia.s__Roseburia_inulinivorans
PathID_10941	0.001747	PWY-6151: S-adenosyl-L-methionine cycle I|g__Ruminococcus.s__Ruminococcus_bromii
PathID_5118	0.001733	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Barnesiella.s__Barnesiella_intestinihominis
PathID_9954	0.001686	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Eubacterium.s__Eubacterium_ventriosum
PathID_11110	0.001663	PWY-6737: starch degradation V|g__Clostridium.s__Clostridium_clostridioforme
PathID_1185	0.001626	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Clostridium.s__Clostridium_clostridioforme
PathID_6644	0.001566	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Clostridium.s__Clostridium_clostridioforme
PathID_279	0.001518	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Parabacteroides.s__Parabacteroides_merdae
PathID_4802	0.001499	PWY-6277: superpathway of 5-aminoimidazole ribonucleotide biosynthesis|g__Clostridium.s__Clostridium_clostridioforme
PathID_9007	0.00149	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_2484	0.001475	PWY-5103: L-isoleucine biosynthesis III|g__Pseudoflavonifractor.s__Pseudoflavonifractor_capillosus
PathID_3526	0.001473	PWYG-321: mycolate biosynthesis|unclassified
PathID_9494	0.001473	PWY-5692: allantoin degradation to glyoxylate II
PathID_10764	0.001462	PWY0-1296: purine ribonucleosides degradation|g__Roseburia.s__Roseburia_inulinivorans
PathID_10859	0.001452	PWY-6590: superpathway of Clostridium acetobutylicum acidogenic fermentation|unclassified
PathID_1392	0.001445	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Ruminococcus.s__Ruminococcus_bromii
PathID_6551	0.001443	PWY-7229: superpathway of adenosine nucleotides de novo biosynthesis I|g__Bacteroides.s__Bacteroides_cellulosilyticus
PathID_20	0.001431	PWY-6163: chorismate biosynthesis from 3-dehydroquinate|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_3457	0.001428	PWY-7222: guanosine deoxyribonucleotides de novo biosynthesis II|unclassified
PathID_1626	0.001427	UNINTEGRATED|g__Alistipes.s__Alistipes_shahii
PathID_661	0.001427	PWY-7211: superpathway of pyrimidine deoxyribonucleotides de novo biosynthesis|unclassified
PathID_3046	0.001427	PWY-5188: tetrapyrrole biosynthesis I (from glutamate)|g__Ruminococcus.s__Ruminococcus_bromii
PathID_1077	0.001426	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_9289	0.001425	PWY-6588: pyruvate fermentation to acetone|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_5791	0.001424	PWY-7111: pyruvate fermentation to isobutanol (engineered)|g__Ruminococcus.s__Ruminococcus_bromii
PathID_2501	0.001413	GALACT-GLUCUROCAT-PWY: superpathway of hexuronide and hexuronate degradation|unclassified
PathID_12364	0.001412	PWY-5838: superpathway of menaquinol-8 biosynthesis I
PathID_2177	0.0014	CENTFERM-PWY: pyruvate fermentation to butanoate|unclassified
PathID_5421	0.001397	PWY-6700: queuosine biosynthesis|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_3567	0.001381	PWY-7211: superpathway of pyrimidine deoxyribonucleotides de novo biosynthesis
PathID_2878	0.001378	PANTO-PWY: phosphopantothenate biosynthesis I|unclassified
PathID_3768	0.001377	PWY-6737: starch degradation V|g__Ruminococcus.s__Ruminococcus_bromii
PathID_7239	0.001375	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Ruminococcus.s__Ruminococcus_callidus
PathID_11266	0.001369	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Parabacteroides.s__Parabacteroides_merdae
PathID_1571	0.001369	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_4610	0.001358	UNINTEGRATED|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_4841	0.001336	HOMOSER-METSYN-PWY: L-methionine biosynthesis I|g__Ruminococcus.s__Ruminococcus_bromii
PathID_11499	0.001328	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Eubacterium.s__Eubacterium_ventriosum
PathID_2520	0.001327	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_11470	0.001319	PWY-6609: adenine and adenosine salvage III|g__Eubacterium.s__Eubacterium_ventriosum
PathID_2823	0.001318	PWY-5686: UMP biosynthesis|g__Parabacteroides.s__Parabacteroides_merdae
PathID_993	0.001317	PWY-5686: UMP biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_9970	0.001316	CALVIN-PWY: Calvin-Benson-Bassham cycle|g__Ruminococcus.s__Ruminococcus_bromii
PathID_1275	0.001315	COMPLETE-ARO-PWY: superpathway of aromatic amino acid biosynthesis|g__Clostridium.s__Clostridium_clostridioforme
PathID_7223	0.001285	PWY-7388: octanoyl-[acyl-carrier protein] biosynthesis (mitochondria, yeast)|unclassified
PathID_6924	0.001277	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Clostridium.s__Clostridium_clostridioforme
PathID_4241	0.001274	PWY-6151: S-adenosyl-L-methionine cycle I|g__Clostridium.s__Clostridium_clostridioforme
PathID_3902	0.001264	CRNFORCAT-PWY: creatinine degradation I|unclassified
PathID_8985	0.001263	PRPP-PWY: superpathway of histidine, purine, and pyrimidine biosynthesis|unclassified
PathID_8641	0.00126	PWY-6507: 4-deoxy-L-threo-hex-4-enopyranuronate degradation|unclassified
PathID_5491	0.00126	VALSYN-PWY: L-valine biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_7115	0.001248	ARG+POLYAMINE-SYN: superpathway of arginine and polyamine biosynthesis|unclassified
PathID_3342	0.001229	PWY-6703: preQ0 biosynthesis|g__Ruminococcus.s__Ruminococcus_callidus
PathID_5595	0.00122	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Bacteroides.s__Bacteroides_intestinalis
PathID_1159	0.001217	PANTOSYN-PWY: pantothenate and coenzyme A biosynthesis I|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_2785	0.001217	PYRIDNUCSYN-PWY: NAD biosynthesis I (from aspartate)|g__Ruminococcus.s__Ruminococcus_bromii
PathID_5539	0.001208	PWY-6163: chorismate biosynthesis from 3-dehydroquinate|g__Ruminococcus.s__Ruminococcus_callidus
PathID_11096	0.001203	PWY-6700: queuosine biosynthesis|g__Ruminococcus.s__Ruminococcus_callidus
PathID_5824	0.001201	P124-PWY: Bifidobacterium shunt|unclassified
PathID_8570	0.001195	PWY-7357: thiamin formation from pyrithiamine and oxythiamine (yeast)|unclassified
PathID_10986	0.001189	UNINTEGRATED|g__Parabacteroides.s__Parabacteroides_merdae
PathID_8013	0.001188	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Clostridium.s__Clostridium_clostridioforme
PathID_8278	0.00118	PWY-6305: putrescine biosynthesis IV|g__Faecalibacterium.s__Faecalibacterium_prausnitzii

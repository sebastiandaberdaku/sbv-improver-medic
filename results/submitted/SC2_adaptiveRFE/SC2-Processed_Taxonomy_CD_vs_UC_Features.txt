TaxonomyID	Importance_Optional	Description
TaxID_1879023	0.021561	Limnohabitans sp. 103DPR2
TaxID_1926494	0.012832	Pseudomonas sp. Os17
TaxID_56812	0.009056	Pseudomonas aeruginosa
TaxID_1816218	0.008759	Desulfohalobium retbaense
TaxID_200991	0.007679	Mesorhizobium huakuii
TaxID_1506553	0.006255	Micromonospora zamorensis
TaxID_1725232	0.006026	Actinobacillus succinogenes
TaxID_881	0.005416	Bacillus anthracis
TaxID_541000	0.005152	Sphingobium sp. TKS
TaxID_38288	0.005036	Flavobacterium kingsejongi
TaxID_208479	0.004939	Bacteroides zoogleoformans
TaxID_28452	0.004918	Pseudomonas fragi
TaxID_1867846	0.004828	Hymenobacter sedentarius
TaxID_1847725	0.00474	Caulobacter sp. K31
TaxID_1850252	0.004709	Leptospira interrogans
TaxID_220697	0.00469	Pseudodesulfovibrio aespoeensis
TaxID_186650	0.004339	Gammapapillomavirus 13
TaxID_1729679	0.004287	Phaeobacter piscinae
TaxID_2487071	0.00422	Acinetobacter defluvii
TaxID_887061	0.004115	Sphingobium chlorophenolicum
TaxID_1072256	0.004056	Corynebacterium aquilae
TaxID_33024	0.004036	Stenotrophomonas sp. ZAC14D2_NAIMI4_7
TaxID_1160	0.003983	Pararhodospirillum photometricum
TaxID_146785	0.003825	Butyricimonas faecalis
TaxID_2100422	0.003813	Pectobacterium polaris
TaxID_853	0.003725	Herminiimonas arsenitoxidans
TaxID_216851	0.003648	Steroidobacter denitrificans
TaxID_28113	0.003618	Acinetobacter nosocomialis
TaxID_33025	0.003503	Acidovorax citrulli
TaxID_795665	0.003484	Nitrospirillum amazonense
TaxID_80869	0.003414	Helicobacter mustelae
TaxID_58180	0.003308	Pseudomonas azotoformans
TaxID_33074	0.003285	Bordetella petrii
TaxID_1270	0.003251	Pseudolabrys taiwanensis
TaxID_585425	0.00317	Thiohalobacter thiocyanaticus
TaxID_54304	0.003101	Blastochloris viridis
TaxID_28119	0.003028	Hydrogenophilus thermoluteolus
TaxID_1528099	0.003018	Serratia sp. FS14
TaxID_909932	0.003011	Frateuria aurantia
TaxID_1206	0.002987	Pseudomonas sp. StFLB209
TaxID_41707	0.002937	Marinobacter sp. CP1
TaxID_29546	0.002895	Corynebacterium uterequi
TaxID_274539	0.002871	Salmonella enterica
TaxID_160660	0.002852	Enterobacter sp. R4-368
TaxID_1987723	0.002818	Listeria grayi
TaxID_1205	0.002798	Desulfomicrobium orale
TaxID_762954	0.002753	Methylotenera versatilis
TaxID_28049	0.002676	Gemmata sp. SH-PL17
TaxID_2502843	0.002656	Moraxella osloensis
TaxID_33073	0.002583	Methanobrevibacter ruminantium
TaxID_1936029	0.002581	Ignavibacterium album
TaxID_106649	0.002581	Streptomyces albireticuli
TaxID_649756	0.002549	Alteromonas stellipolaris
TaxID_452	0.002533	Corynebacterium genitalium
TaxID_1986146	0.002508	Desulfobacca acetoxidans
TaxID_2082949	0.002504	Granulosicoccus antarcticus
TaxID_1725	0.002431	Microvirga sp. 17 mud 1-3
TaxID_893	0.00236	Azotobacter vinelandii
TaxID_1306274	0.002302	Elusimicrobium minutum
TaxID_288793	0.002302	Methylovorus sp. MP688
TaxID_60548	0.002287	Paucimonas lemoignei
TaxID_1160721	0.002286	Burkholderiales bacterium YL45
TaxID_1630693	0.00226	Propionibacterium freudenreichii
TaxID_585423	0.002254	Magnetospira sp. QH-2
TaxID_864702	0.002245	Vibrio rumoiensis
TaxID_1571470	0.002232	Pseudomonas sp. DY-1
TaxID_1263	0.002226	Bathymodiolus septemdierum thioautotrophic gill symbiont
TaxID_1744	0.002226	Xanthomonas cassavae
TaxID_40990	0.002176	Deinococcus proteolyticus
TaxID_151340	0.002162	Phascolarctobacterium faecium
TaxID_1411621	0.002159	Delftia sp. HK171
TaxID_1315974	0.002138	Vibrio mediterranei
TaxID_55802	0.002127	Dickeya dianthicola
TaxID_1580596	0.002122	Halomonas huangheensis
TaxID_1107	0.002118	Gloeocapsa sp. PCC 7428
TaxID_207244	0.002095	Acetobacter pasteurianus
TaxID_76258	0.002091	Flavobacterium sp. CJ74
TaxID_1834196	0.002076	Cupriavidus gilardii
TaxID_32051	0.002074	Rhodococcus sp. 2G
TaxID_1796606	0.002067	Arcobacter nitrofigilis
TaxID_247523	0.002067	Zymobacter palmae
TaxID_2257	0.002043	Acinetobacter bereziniae
TaxID_290053	0.002036	Acetobacter ascendens
TaxID_75612	0.002025	Desulfotomaculum ruminis
TaxID_2014887	0.00199	Leptospira mayottensis
TaxID_52	0.001983	Murdochiella vaginalis
TaxID_102231	0.001962	Paraburkholderia graminis
TaxID_33045	0.001941	Waddlia chondrophila
TaxID_203263	0.001934	Pseudonocardia sp. HH130629-09
TaxID_50	0.001933	Pseudorhodoplanes sinuspersici
TaxID_28104	0.001925	Pseudoalteromonas sp. Bsw20308
TaxID_1755504	0.001916	Pseudoalteromonas espejiana
TaxID_585455	0.001916	Streptococcus constellatus
TaxID_1224	0.001915	Pseudoalteromonas aliena
TaxID_2231055	0.001903	Borrelia miyamotoi
TaxID_1173026	0.001896	Acinetobacter lwoffii
TaxID_150829	0.001895	Deinococcus peraridilitoris
TaxID_2072405	0.001877	Burkholderia sp. CCGE1003
TaxID_81475	0.001872	Desulfovibrio alaskensis
TaxID_83816	0.001862	Faecalibacterium prausnitzii
TaxID_55148	0.001821	Cellvibrio sp. PSBB006
TaxID_404011	0.00182	Pseudodesulfovibrio piezophilus
TaxID_364317	0.001816	Vitreoscilla filiformis
TaxID_1500686	0.001794	Pontimonas salivibrio
TaxID_62322	0.001788	Planococcus plakortidis
TaxID_278153	0.001781	Negativicoccus massiliensis
TaxID_1288970	0.00178	Synechococcus sp. WH 7803
TaxID_1450761	0.001775	Arthrobacter alpinus
TaxID_1269	0.001765	Lysinibacillus varians
TaxID_1734920	0.001764	Salinigranum rubrum
TaxID_1106	0.001748	Microbacterium hominis
TaxID_1597	0.001721	Aerococcus urinae
TaxID_2093856	0.001719	Bacteroides heparinolyticus
TaxID_2281	0.001713	Hyphomicrobium denitrificans
TaxID_118883	0.001711	Corynebacterium ammoniagenes
TaxID_204039	0.001708	Treponema azotonutricium
TaxID_1327989	0.001698	Streptomyces globisporus
TaxID_60892	0.001694	Candidatus Pelagibacter sp. RS39
TaxID_60893	0.001693	Methylomicrobium album
TaxID_482564	0.00169	Halobacteriovorax sp. BALOs_7
TaxID_46429	0.001688	Stenotrophomonas sp. ESTM1D_MKCIP4_1
TaxID_32054	0.001679	Colwellia sp. PAMC 20917
TaxID_1182780	0.001668	[Clostridium] bolteae
TaxID_132132	0.001639	Desulfobulbus oralis
TaxID_331696	0.001637	Calothrix parietina
TaxID_1577788	0.001632	Allokutzneria albata
TaxID_92947	0.001631	Gemella sp. oral taxon 928
TaxID_47670	0.001626	Salinimonas sp. HMF8227
TaxID_1080709	0.001624	Treponema denticola
TaxID_1678129	0.001614	Lautropia mirabilis
TaxID_1912216	0.001605	Chloroflexus aurantiacus
TaxID_183967	0.001602	Acidihalobacter prosperus
TaxID_1834205	0.001594	Natronobacterium gregoryi
TaxID_2256	0.00158	Cupriavidus nantongensis
TaxID_562	0.001578	Allochromatium vinosum
TaxID_574697	0.001577	Kangiella koreensis
TaxID_44930	0.001574	Micromonospora echinofusca
TaxID_481146	0.001567	Streptomyces sp. SCSIO 03032
TaxID_138072	0.001562	Rhizobium sp. ACO-34A
TaxID_797277	0.001559	Shewanella frigidimarina
TaxID_656366	0.001556	Ehrlichia chaffeensis
TaxID_1049	0.001552	Advenella kashmirensis
TaxID_1778653	0.001551	Aneurinibacillus sp. XH2
TaxID_395598	0.001545	Kutzneria albida
TaxID_871006	0.001544	Deinococcus maricopensis
TaxID_1513258	0.001539	Bacteroides dorei
TaxID_34062	0.001538	Komagataeibacter hansenii
TaxID_47716	0.001535	Rhizobium sp. IE4771
TaxID_1747	0.001531	Simiduia agarivorans
TaxID_283168	0.001529	Staphylococcus xylosus
TaxID_94624	0.001516	Faecalibaculum rodentium
TaxID_203492	0.001515	Fluoribacter dumoffii
TaxID_32066	0.001509	Pseudoalteromonas nigrifaciens
TaxID_2042057	0.001506	Methanosarcina vacuolata
TaxID_848	0.001506	Neisseria sicca
TaxID_29303	0.001501	Yersinia pestis
TaxID_31957	0.001497	Sphingobacterium sp. G1-14
TaxID_28264	0.001496	Streptomyces sp. P3
TaxID_456826	0.001494	Nostoc flagelliforme
TaxID_1564	0.001493	Thermococcus barophilus
TaxID_203490	0.001491	Streptomyces sp. TLI_053
TaxID_447471	0.001489	Tatlockia micdadei
TaxID_203491	0.001484	Shewanella baltica
TaxID_1765964	0.001477	Chondromyces crocatus
TaxID_1392	0.001475	Thalassospira xiamenensis
TaxID_39775	0.00147	Pseudomonas xinjiangensis
TaxID_157932	0.001469	Stenotrophomonas sp. WZN-1
TaxID_1934947	0.001468	Acinetobacter guillouiae
TaxID_795750	0.001467	Dialister sp. Marseille-P5638
TaxID_32049	0.001466	[Polyangium] brachysporum
TaxID_314282	0.001464	Cutibacterium granulosum
TaxID_1109743	0.001458	Massilia armeniaca
TaxID_1671721	0.001455	Oscillatoriales cyanobacterium JSC-12
TaxID_909930	0.001448	Microlunatus phosphovorus
TaxID_1843488	0.001445	Synechococcus sp. KORDI-52
TaxID_456327	0.001443	Hydrogenophaga sp. RAC07
TaxID_43356	0.001441	Pseudomonas mandelii
TaxID_297	0.001438	Bartonella sp. OE 1-1
TaxID_1434032	0.001438	Bacillus sp. X1(2014)
TaxID_1159327	0.001435	Lachnoclostridium sp. YL32
TaxID_2008785	0.001429	Corynebacterium xerosis
TaxID_2215	0.001427	Shewanella piezotolerans
TaxID_119069	0.001427	Hydrogenophaga crassostreae
TaxID_43357	0.001424	Arcobacter sp. LPB0137
TaxID_1678728	0.001395	Nitratifractor salsuginis
TaxID_1038856	0.001394	Deinococcus deserti
TaxID_28118	0.001382	Legionella spiritensis
TaxID_1235850	0.001377	Verminephrobacter eiseniae
TaxID_59	0.001367	Erythrobacter sp. HKB08
TaxID_1934946	0.001361	Wolbachia endosymbiont of Cimex lectularius
TaxID_1714848	0.00136	Halobiforma lacisalsi
TaxID_354	0.001357	Streptomyces cattleya
TaxID_2109558	0.001354	Candidatus Methanomassiliicoccus intestinalis
TaxID_84565	0.001351	Trichodesmium erythraeum
TaxID_1028989	0.001348	Escherichia coli
TaxID_364316	0.001347	Treponema pedis
TaxID_1977864	0.001337	Paraburkholderia hospita
TaxID_591197	0.00133	Chryseobacterium sp. F5649
TaxID_309887	0.001328	Fluviicola taffensis
TaxID_401618	0.001327	Bartonella grahamii
TaxID_28077	0.001324	Spiroplasma litorale
TaxID_1204360	0.001319	Myroides profundi
TaxID_81682	0.001319	Micrococcus luteus
TaxID_47763	0.001318	Planktothrix agardhii
TaxID_1108	0.001315	Bartonella quintana
TaxID_47671	0.001314	Nakamurella multipartita
TaxID_432329	0.00131	Cutibacterium acnes
TaxID_632	0.001308	Pseudomonas litoralis
TaxID_1896196	0.001303	Aquimarina sp. AD1
TaxID_1137606	0.001303	Synechococcus sp. PCC 7002
TaxID_2183582	0.001302	Herbaspirillum robiniae
TaxID_1785995	0.001302	Pseudomonas reinekei
TaxID_53399	0.001299	Mesorhizobium sp. WSM1497
TaxID_1738655	0.001298	Bacteroides cellulosilyticus
TaxID_1908	0.001296	Odoribacter splanchnicus
TaxID_1790137	0.001273	Legionella longbeachae
TaxID_2135430	0.001266	Bacteroides helcogenes
TaxID_803	0.001265	Porphyrobacter sp. LM 6
TaxID_82541	0.001259	Lactobacillus paracasei
TaxID_1940	0.001258	Oscillatoria nigro-viridis
TaxID_370405	0.001255	Paraburkholderia phenoliruptrix
TaxID_755307	0.001253	Anaerostipes hadrus
TaxID_28199	0.001249	Synechococcus sp. KORDI-49
TaxID_182210	0.001245	Trichormus azollae
TaxID_90627	0.001242	Streptococcus pasteurianus
TaxID_447467	0.001241	Paraburkholderia sp. SOS3
TaxID_2005046	0.00124	Sodalis endosymbiont of Henestaris halophilus
TaxID_1871111	0.001234	Planococcus rifietoensis
TaxID_29443	0.001234	Wenyingzhuangia fucanilytica
TaxID_1145276	0.001233	Leptospira alstonii
TaxID_1853230	0.001231	Rheinheimera sp. LHK132
TaxID_1929246	0.00123	Xenorhabdus doucetiae
TaxID_437505	0.001224	Legionella clemsonensis
TaxID_2072414	0.001222	Natronomonas pharaonis
TaxID_709883	0.00122	Streptomyces lydicus
TaxID_543	0.001216	Corynebacterium diphtheriae
TaxID_169430	0.001215	Hydrogenophaga sp. PBC
TaxID_1644057	0.001203	Arcanobacterium haemolyticum
TaxID_1084	0.001198	Corynebacterium riegelii
TaxID_91347	0.001193	Stanieria sp. NIES-3757
TaxID_197614	0.001184	Rhodococcus ruber
TaxID_180541	0.001178	Pelobacter acetylenicus
TaxID_2072590	0.001175	Saccharophagus degradans
TaxID_640512	0.001171	Ketogulonicigenium robustum
TaxID_246787	0.00117	Fusobacterium varium
TaxID_332102	0.001164	Dickeya sp. NCPPB 569
TaxID_106654	0.001162	Saccharomonospora glauca
TaxID_689	0.001161	Gallionella capsiferriformans
TaxID_216942	0.001151	Sulfuriferula sp. AH1
TaxID_357276	0.001142	Tenericutes bacterium MO-XQ
TaxID_437504	0.001139	Dechloromonas sp. HYN0024
TaxID_1236	0.001138	Halomonas sp. hl-4
TaxID_742031	0.001137	Histophilus somni
TaxID_731	0.001133	Sphingobium indicum
TaxID_106648	0.001125	Plantibacter flavus
TaxID_1570939	0.001125	Acinetobacter haemolyticus
TaxID_469322	0.001118	Mycobacterium sp. djl-10
TaxID_2268	0.001115	Nocardioides sp. JS614
TaxID_86304	0.001114	Siansivirga zeaxanthinifaciens
TaxID_2003121	0.001107	Candidatus Hamiltonella defensa
TaxID_409322	0.001107	Lawsonella clevelandensis
TaxID_196914	0.001103	Methylocystis rosea
TaxID_450	0.0011	Candidatus Desulfovibrio trichonymphae
TaxID_47466	0.001098	Xanthomonas euvesicatoria
TaxID_47858	0.001097	Desulfovibrio vulgaris
TaxID_568768	0.001096	Acidothermus cellulolyticus
TaxID_114378	0.001095	Delftia acidovorans
TaxID_191579	0.001093	Aeromonas media
TaxID_283699	0.001092	Actinoplanes friuliensis
TaxID_1295609	0.001092	Psychromonas sp. CNPT3
TaxID_269261	0.001091	Woeseia oceani
TaxID_1235591	0.001091	Thermodesulfobacterium geofontis
TaxID_1288	0.001089	Leuconostoc mesenteroides
TaxID_1985873	0.001084	Lawsonia intracellularis
TaxID_556257	0.001083	Pseudomonas sp. S-6-2
TaxID_70774	0.001083	Streptomyces olivaceus
TaxID_292913	0.00108	Tenacibaculum todarodis
TaxID_1079	0.001078	Mycobacterium sp. MS1601
TaxID_28090	0.001078	Ruminococcus bicirculans
TaxID_233316	0.001075	Sphingopyxis sp. 113P3
TaxID_206349	0.001075	Bacillus cellulasensis
TaxID_1406512	0.001073	Chlorobaculum parvum
TaxID_436	0.001073	Idiomarina sp. OT37-5b
TaxID_1548547	0.001071	Faecalibacterium
TaxID_196137	0.001071	Microvirga
TaxID_1738654	0.00107	Wenyingzhuangia
TaxID_1763535	0.001067	Candidatus Riesia
TaxID_630749	0.001063	Allokutzneria
TaxID_463	0.001063	Thermofilum
TaxID_461	0.001063	Escherichia
TaxID_96	0.001063	Lawsonia
TaxID_1543705	0.001062	Salinimonas
TaxID_63	0.001059	Desulfobulbus
TaxID_53461	0.001054	Trichodesmium
TaxID_310575	0.001052	Salinigranum
TaxID_1702221	0.00105	Vitreoscilla
TaxID_1809410	0.001049	Zymobacter
TaxID_1164	0.001047	Magnetospira
TaxID_33011	0.001044	Pontimonas
TaxID_213115	0.001042	Desulfobacca
TaxID_351671	0.001042	Hydrogenophilus
TaxID_1612157	0.00104	Immundisolibacter
TaxID_1518147	0.001039	Pararhodospirillum
TaxID_71667	0.001037	Micrococcus
TaxID_1641402	0.001036	Propionibacterium
TaxID_1641	0.001035	Methylomicrobium
TaxID_183924	0.001035	Simiduia
TaxID_945	0.001035	Desulfohalobium
TaxID_1920191	0.001035	Sulfuriferula
TaxID_480520	0.001033	Salmonella
TaxID_1807358	0.00103	Psychromonas
TaxID_186803	0.001029	Butyricimonas
TaxID_1751872	0.001028	Pseudolabrys
TaxID_310783	0.001027	Murdochiella
TaxID_246273	0.001027	Acidihalobacter
TaxID_173	0.001025	Granulosicoccus
TaxID_211114	0.001019	Saccharophagus
TaxID_698776	0.001019	Gloeocapsa
TaxID_1855352	0.001016	Sodalis
TaxID_194924	0.001016	Gallionella
TaxID_366602	0.001015	Methylovorus
TaxID_173366	0.001015	Verminephrobacter
TaxID_316625	0.001014	Geosporobacter
TaxID_1178482	0.001013	Hyphomicrobium
TaxID_1376	0.001012	Lachnoclostridium
TaxID_1931241	0.001011	Salinisphaera
TaxID_203193	0.00101	Odoribacter
TaxID_229731	0.001009	Lacimicrobium
TaxID_28107	0.001008	Pseudorhodoplanes
TaxID_81	0.001007	Cellulosilyticum
TaxID_1697	0.001006	Faecalibaculum
TaxID_1137960	0.001005	Kushneria
TaxID_45663	0.001004	Woeseia
TaxID_213117	0.001002	Ignavibacterium
TaxID_196162	0.001	Fusobacterium
TaxID_856	0.000998	Tatlockia
TaxID_423605	0.000997	Lautropia
TaxID_2161821	0.000995	Lawsonella
TaxID_39773	0.000995	Spongiibacter
TaxID_267894	0.000995	Candidatus Cloacimonas
TaxID_1842537	0.000993	Planktothrix
TaxID_252970	0.000992	Steroidobacter
TaxID_1852373	0.00099	Nitrospirillum
TaxID_1830	0.000989	Thermosynechococcus
TaxID_92714	0.000989	Kutzneria
TaxID_1717	0.000987	Phascolarctobacterium
TaxID_1565991	0.000981	Siansivirga
TaxID_56450	0.000977	Methanomassiliicoccus
TaxID_67854	0.000976	Lonsdalea
TaxID_879567	0.000976	Melioribacter
TaxID_1850254	0.000972	Anaerostipes
TaxID_545612	0.000972	Cutibacterium
TaxID_1134403	0.000972	Chloroflexus
TaxID_487184	0.000971	Fluviicola
TaxID_296	0.000964	Fluoribacter
TaxID_1761789	0.000964	Natronobacterium
TaxID_413882	0.000963	Halobiforma
TaxID_2294119	0.000962	Desulfovibrio
TaxID_162426	0.00096	Paucimonas
TaxID_45662	0.000958	Ruminococcus
TaxID_504090	0.000957	Chondromyces
TaxID_217	0.000956	Hydrogenophilaceae
TaxID_10292	0.000954	Chloroflexaceae
TaxID_1245	0.000953	Immundisolibacteraceae
TaxID_156976	0.000953	Methanomassiliicoccaceae
TaxID_1301032	0.00095	Ruminococcaceae
TaxID_1702287	0.000949	Propionibacteriaceae
TaxID_438	0.000948	Lachnospiraceae
TaxID_1082702	0.000947	Methanospirillaceae
TaxID_287	0.000947	Psychromonadaceae
TaxID_47878	0.000947	Fusobacteriaceae
TaxID_465	0.000947	Waddliaceae
TaxID_872	0.000946	Desulfohalobiaceae
TaxID_451	0.000945	Desulfovibrionaceae
TaxID_465721	0.000945	Sulfolobaceae
TaxID_1743	0.000942	Papillomaviridae
TaxID_1049581	0.000942	Thermofilaceae
TaxID_28901	0.00094	Crocinitomicaceae
TaxID_1166130	0.000939	Salinisphaeraceae
TaxID_84992	0.000938	Herpesviridae
TaxID_67572	0.000934	Enterobacteriaceae
TaxID_80866	0.000933	Gallionellaceae
TaxID_76860	0.000933	Acidaminococcaceae
TaxID_490	0.000932	Woeseiaceae
TaxID_261964	0.000928	Enterobacterales
TaxID_651	0.000927	Methanomassiliicoccales
TaxID_1911684	0.000926	Acidaminococcales
TaxID_150123	0.000926	Fusobacteriales
TaxID_28109	0.000925	Desulfovibrionales
TaxID_332055	0.000925	Acidilobales
TaxID_2498451	0.000924	Sulfolobales
TaxID_561	0.000922	Hydrogenophilales
TaxID_29430	0.00092	Negativicutes
TaxID_1161127	0.00092	Thermoplasmata
TaxID_113267	0.000918	Hydrogenophilalia
TaxID_29542	0.000917	Thermoprotei
TaxID_590	0.000915	Gammaproteobacteria
TaxID_1055487	0.000915	Fusobacteriia
TaxID_158	0.000914	Acidimicrobiia
TaxID_390805	0.00091	Proteobacteria
TaxID_29405	0.000908	Fusobacteria

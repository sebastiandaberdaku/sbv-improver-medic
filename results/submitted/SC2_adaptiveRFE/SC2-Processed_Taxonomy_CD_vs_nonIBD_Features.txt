TaxonomyID	Importance_Optional	Description
TaxID_649756	0.028563	Limnohabitans sp. 103DPR2
TaxID_1834196	0.020881	Pseudomonas sp. Os17
TaxID_1506553	0.017612	Mycobacterium intracellulare
TaxID_204039	0.01252	Rhodoferax ferrireducens
TaxID_208479	0.011583	Mesorhizobium sp. M2A.F.Ca.ET.046.03.2.1
TaxID_207244	0.010076	Desulfohalobium retbaense
TaxID_32066	0.007389	Mycobacterium chimaera
TaxID_253238	0.007296	Halomonas sp. HL-93
TaxID_1850252	0.007116	Candidatus Fonsibacter ubiquis
TaxID_1580596	0.006721	Mesorhizobium sp. M1D.F.Ca.ET.043.01.1.1
TaxID_203490	0.006627	Dactylococcopsis salina
TaxID_292800	0.006505	Spiroplasma citri
TaxID_1273687	0.006056	Simonsiella muelleri
TaxID_216851	0.006054	Maribacter sp. T28
TaxID_1263	0.005958	Bifidobacterium longum
TaxID_143393	0.005848	Hydrogenobaculum sp. Y04AAS1
TaxID_53408	0.00582	Vibrio tubiashii
TaxID_271098	0.005755	Pseudomonas citronellolis
TaxID_853	0.005755	Shewanella halifaxensis
TaxID_367190	0.005641	Rhizobium sp. NXC14
TaxID_797277	0.005158	Thermoplasmatales archaeon BRNA1
TaxID_1816219	0.005042	Oscillibacter sp. PEA192
TaxID_1236	0.005012	Azospira oryzae
TaxID_946234	0.004977	Candidatus Ruthia magnifica
TaxID_1878942	0.004957	Fermentimonas caenicola
TaxID_203491	0.004921	Cronobacter turicensis
TaxID_39485	0.004862	endosymbiont of unidentified scaly snail isolate Monju
TaxID_186806	0.00484	Dolichospermum compactum
TaxID_1160721	0.004832	Pseudomonas fragi
TaxID_203492	0.004805	Opitutus sp. GAS368
TaxID_456826	0.004736	Flavonifractor plautii
TaxID_848	0.004607	Borrelia crocidurae
TaxID_39491	0.004596	Caulobacter sp. K31
TaxID_1224	0.004474	Marinitoga sp. 1137
TaxID_1055487	0.004414	Streptococcus dysgalactiae
TaxID_1730	0.004398	Gammapapillomavirus 13
TaxID_76517	0.004354	Phaeobacter piscinae
TaxID_91347	0.004116	Algoriphagus sp. M8-2
TaxID_1680	0.004035	Acinetobacter defluvii
TaxID_158841	0.004033	Streptomyces atratus
TaxID_1761016	0.003973	Fuerstia marisgermanicae
TaxID_28250	0.003929	Stenotrophomonas sp. ZAC14D2_NAIMI4_7
TaxID_543	0.003722	Mycoplasma cloacale
TaxID_28251	0.003655	Exiguobacterium antarcticum
TaxID_1509	0.003417	Pectobacterium polaris
TaxID_1931241	0.003349	Candidatus Methylopumilus turicensis
TaxID_413502	0.003326	Herminiimonas arsenitoxidans
TaxID_2483033	0.003293	Steroidobacter denitrificans
TaxID_861	0.003247	Bacillus vallismortis
TaxID_1778262	0.003171	Bradyrhizobiaceae bacterium SG-6C
TaxID_1541818	0.003146	Erysipelotrichaceae bacterium I46
TaxID_585423	0.003005	Nitrospirillum amazonense
TaxID_318147	0.002913	Streptomyces reticuli
TaxID_401618	0.002883	Helicobacter mustelae
TaxID_44822	0.002854	Pseudomonas sp. M30-35
TaxID_398	0.002824	Enterococcus faecium
TaxID_1906657	0.002809	Deinococcus wulumuqiensis
TaxID_382	0.002613	Sulfitobacter sp. SK025
TaxID_231683	0.002608	Ezakiella massiliensis
TaxID_28901	0.002499	Komagataeibacter medellinensis
TaxID_562	0.002484	Mariniflexile sp. TRM1-10
TaxID_1927833	0.002465	Methylobacterium sp. AMS5
TaxID_1853276	0.002459	Serratia sp. FS14
TaxID_31932	0.002374	Corynebacterium flavescens
TaxID_1847725	0.002354	Frateuria aurantia
TaxID_338190	0.002334	Pseudomonas sp. LTGT-11-2Z
TaxID_1879049	0.002329	Rhodococcus qingshengii
TaxID_856	0.00232	Fusobacterium ulcerans
TaxID_590	0.002311	Saccharospirillum mangrovi
TaxID_1581680	0.002304	Pseudomonas sp. StFLB209
TaxID_45972	0.002244	Mesorhizobium opportunistum
TaxID_2042057	0.002226	Pseudoalteromonas translucida
TaxID_561	0.002201	Staphylococcus pasteuri
TaxID_1898684	0.002195	Vibrio chagasii
TaxID_58138	0.002147	Lactobacillus acidophilus
TaxID_1809410	0.002133	Polaribacter sp. KT 15
TaxID_1294	0.002114	Stenotrophomonas sp. G4
TaxID_515393	0.002093	Sphingobium herbicidovorans
TaxID_541000	0.002088	Spiroplasma chrysopicola
TaxID_51365	0.002079	Limnohabitans sp. 63ED37-2
TaxID_1644131	0.00207	Aeromicrobium erythreum
TaxID_1233873	0.002011	Wolbachia endosymbiont of Drosophila simulans
TaxID_413501	0.002002	Salmonella enterica
TaxID_1802984	0.001999	Methylomonas clara
TaxID_157226	0.001957	Enterobacter sp. R4-368
TaxID_864702	0.001945	Listeria grayi
TaxID_1747776	0.001935	Exiguobacterium sp. ZWU0009
TaxID_1802983	0.00192	Methylotenera versatilis
TaxID_1528099	0.001916	Pseudomonas moraviensis
TaxID_1980513	0.001856	Gemmata sp. SH-PL17
TaxID_101564	0.00184	Enterococcus casseliflavus
TaxID_45662	0.001835	Rickettsia helvetica
TaxID_435	0.00181	Cronobacter universalis
TaxID_573	0.001809	Helicobacter apodemus
TaxID_1630141	0.001801	Psychroflexus torquis
TaxID_47678	0.001788	Lonsdalea britannica
TaxID_859	0.001778	Plantactinospora sp. BB1
TaxID_1678	0.001777	Methanobrevibacter ruminantium
TaxID_1302	0.001765	Ignavibacterium album
TaxID_570	0.001756	Corynebacterium genitalium
TaxID_113267	0.001726	Microcystis sp. MC19
TaxID_2218628	0.001721	Desulfobacca acetoxidans
TaxID_31953	0.001711	Pseudomonas sp. 31-12
TaxID_1264	0.001705	Acetobacter ghanensis
TaxID_47865	0.001696	Mariprofundus ferrinatatus
TaxID_85004	0.001684	Xanthomonas gardneri
TaxID_60893	0.001667	Crinalium epipsammum
TaxID_1394	0.001657	Pseudomonas asplenii
TaxID_1981174	0.001643	Propionimicrobium sp. Marseille-P3275
TaxID_1827146	0.001638	Nitrosococcus watsonii
TaxID_2109558	0.001619	Enterococcus cecorum
TaxID_1105171	0.001614	Maribacter sp. MAR_2009_60
TaxID_39765	0.001611	Paucimonas lemoignei
TaxID_1145276	0.001588	Burkholderiales bacterium YL45
TaxID_32051	0.001587	Clostridium ljungdahlii
TaxID_2303750	0.001584	Paraphotobacterium marinum
TaxID_1747777	0.001564	Thermofilum adornatus
TaxID_43659	0.001555	Bacteroides fragilis
TaxID_1482074	0.001534	Desulfofarcimen acetoxidans
TaxID_818	0.001531	Pseudomonas yamanorum
TaxID_1965282	0.00151	Mycoplasma flocculare
TaxID_164516	0.001508	Chelatococcus sp. CO-6
TaxID_1346	0.001502	Thermoplasma volcanium
TaxID_134536	0.001488	Streptomyces sp. 3211
TaxID_2133	0.001486	Acidithiobacillus ferridurans
TaxID_246787	0.001472	Brevundimonas naejangsanensis
TaxID_1842533	0.001471	Paenibacillus chitinolyticus
TaxID_1987723	0.001468	Photobacterium profundum
TaxID_1389011	0.001467	Acinetobacter sp. WCHA45
TaxID_1166130	0.001455	Pseudothermotoga hypogea
TaxID_1028989	0.001445	Butyrivibrio fibrisolvens
TaxID_43305	0.001428	Gammaproteobacteria bacterium ESL0073
TaxID_1499973	0.001422	Bathymodiolus septemdierum thioautotrophic gill symbiont
TaxID_192843	0.00142	Marinithermus hydrothermalis
TaxID_480520	0.001418	Candidatus Methanoplasma termitum
TaxID_208962	0.001406	Mycobacterium sp. VKM Ac-1817D
TaxID_2493672	0.001405	Lactobacillus agilis
TaxID_693074	0.001398	Pragia fontium
TaxID_74201	0.001393	Thermoplasma acidophilum
TaxID_698828	0.001392	Photorhabdus laumondii
TaxID_948107	0.001392	Delftia sp. HK171
TaxID_1522	0.001385	Fusobacterium necrophorum
TaxID_1313	0.001383	Streptomyces sp. ETH9427
TaxID_1173026	0.001363	Vibrio mediterranei
TaxID_1648	0.001355	Streptococcus pseudopneumoniae
TaxID_1850246	0.001346	Pseudomonas sp. Z003-0.4C(8344-21)
TaxID_99179	0.001341	Butyrivibrio hungatei
TaxID_95300	0.001339	Pseudomonas fuscovaginae
TaxID_1150389	0.001338	Dickeya dianthicola
TaxID_1231	0.001327	Brachyspira pilosicoli
TaxID_239934	0.001326	Methylocella silvestris
TaxID_980427	0.001325	Halomonas huangheensis
TaxID_49894	0.001323	Actinobacillus lignieresii
TaxID_29555	0.001317	Gloeocapsa sp. PCC 7428
TaxID_152682	0.001316	Zoogloeaceae bacteirum Par-f-2
TaxID_1485	0.001309	Rhodococcus sp. MTM3W5.2
TaxID_246273	0.001308	Rhodococcus sp. 2G
TaxID_80866	0.001306	Acidithiobacillus ferrooxidans
TaxID_165096	0.001298	Lactococcus phage D4410
TaxID_239935	0.001293	Ruminococcus champanellensis
TaxID_1508404	0.001275	Arcobacter nitrofigilis
TaxID_203494	0.001273	Caldisericum exile
TaxID_69220	0.00127	Aeromonas sp. CU5
TaxID_234612	0.001269	Paracoccus sp. CBA4604
TaxID_200450	0.001268	Sandaracinus amylolyticus
TaxID_1747	0.001264	Lactobacillus kefiranofaciens
TaxID_983548	0.00126	Altererythrobacter sp. B11
TaxID_48461	0.001257	Pseudanabaena sp. ABRG5-3
TaxID_216816	0.001252	Chlamydia gallinacea
TaxID_742031	0.001251	Thermogutta terrifontis
TaxID_217	0.001248	[Pseudomonas] mesoacidophila
TaxID_1647988	0.001247	Oceanobacillus sp. 160
TaxID_627192	0.001246	Metakosakonia sp. MRY16-398
TaxID_70774	0.001237	Pseudomonas protegens
TaxID_2081703	0.001231	Actinoalloteichus hoggarensis
TaxID_201174	0.00123	Synechococcus sp. PCC 7502
TaxID_92401	0.001228	Peptostreptococcaceae bacterium oral taxon 929
TaxID_1760	0.001221	Sphingomonas sp. JJ-A5
TaxID_46659	0.001217	Jeotgalibacillus malaysiensis
TaxID_180541	0.001217	Waddlia chondrophila
TaxID_2302	0.001216	Vibrio sp. 2521-89
TaxID_257758	0.00121	Sulfuricurvum kujiense
TaxID_640512	0.001208	Paraburkholderia caledonica
TaxID_39950	0.001204	Lactobacillus coryniformis
TaxID_60892	0.001201	Legionella sainthelensi
TaxID_1834207	0.0012	Methanosarcina thermophila
TaxID_2042066	0.001196	Paracoccus yeei
TaxID_431306	0.001194	Vogesella sp. LIG4
TaxID_337090	0.001173	Cyanothece sp. PCC 8801
TaxID_1582879	0.001171	Janthinobacterium sp. 1_2014MBL_MicDiv
TaxID_1484693	0.00117	Bartonella elizabethae
TaxID_44577	0.001169	Mesorhizobium sp. M1E.F.Ca.ET.045.02.1.1
TaxID_1161942	0.001165	Synechococcus sp. JA-2-3B'a(2-13)
TaxID_36814	0.001164	Burkholderia ubonensis
TaxID_1652132	0.001163	Streptococcus mitis
TaxID_720	0.001162	Chromatiaceae bacterium 2141T.STBD.0c.01a
TaxID_830	0.001162	Bacteroides caccae
TaxID_871006	0.001159	Acinetobacter lwoffii
TaxID_685565	0.001159	Thermocrinis albus
TaxID_83816	0.001154	Colwellia sp. PAMC 21821
TaxID_589382	0.001151	Pseudomonas alcaliphila
TaxID_50340	0.001148	Burkholderia sp. CCGE1003
TaxID_1100891	0.001147	Lelliottia nimipressuralis
TaxID_1934947	0.001144	Mycoplasma canis
TaxID_1192162	0.001136	Methylobacterium phyllosphaerae
TaxID_86664	0.001131	Faecalibacterium prausnitzii
TaxID_222805	0.001126	Staphylococcus simulans
TaxID_187880	0.001126	Methanohalobium evestigatum
TaxID_1940790	0.001123	Streptococcus sanguinis
TaxID_698776	0.001121	Arachidicoccus sp. BS20
TaxID_38288	0.001118	Cellvibrio sp. PSBB006
TaxID_35799	0.001115	Geobacillus thermodenitrificans
TaxID_1967666	0.001115	Brevundimonas vesicularis
TaxID_1767	0.001114	Dokdonia sp. Dokd-P16
TaxID_74109	0.001112	Dokdonia sp. MED134
TaxID_1570339	0.00111	Anabaena sp. WA102
TaxID_1335757	0.001107	Planococcus plakortidis
TaxID_2083786	0.001099	Synechococcus sp. WH 7803
TaxID_82980	0.001098	Synechococcus sp. PCC 73109
TaxID_33940	0.001097	Ruminococcus albus
TaxID_1505663	0.001097	Komagataeibacter europaeus
TaxID_79880	0.001091	Butyrivibrio proteoclasticus
TaxID_1851395	0.001091	Flavobacteriaceae bacterium UJ101
TaxID_2486578	0.001087	Nitrosospira multiformis
TaxID_942	0.001086	Mycoplasma bovis
TaxID_1082704	0.001082	Lysinibacillus varians
TaxID_2070539	0.001081	Methylovorus glucosotrophus
TaxID_651137	0.00108	Methylocystis bryophila
TaxID_1912216	0.001079	Candidatus Doolittlea endobia
TaxID_2067572	0.001074	Lactobacillus gasseri
TaxID_1678128	0.001074	Psychromonas ingrahamii
TaxID_2161747	0.001072	Weeksella virosa
TaxID_2282742	0.00107	Vibrio anguillarum
TaxID_59737	0.00107	Microbacterium hominis
TaxID_1505597	0.001066	Streptomyces sp. DUT11
TaxID_1416803	0.001066	Chamaesiphon minutus
TaxID_161154	0.001062	Aerococcus urinae
TaxID_286130	0.001056	Lactococcus garvieae
TaxID_543311	0.001056	Bacteroides phage B40-8
TaxID_357276	0.001056	Muricauda lutaonensis
TaxID_28037	0.001055	Bathymodiolus thermophilus thioautotrophic gill symbiont
TaxID_2161821	0.001053	Acetomicrobium mobile
TaxID_655015	0.001053	Acinetobacter sp. WCHAc010034
TaxID_436	0.001053	Chromobacterium rhizoryzae
TaxID_47834	0.001044	Tateyamaria omphalii
TaxID_33011	0.001043	Vibrio parahaemolyticus
TaxID_213117	0.001041	Halobacteriovorax sp. BALOs_7
TaxID_2360	0.00104	Comamonas thiooxydans
TaxID_334542	0.00104	Enterococcus durans
TaxID_47716	0.001038	Escherichia marmotae
TaxID_1108	0.001037	Rhizobium tropici
TaxID_915	0.001035	[Clostridium] bolteae
TaxID_40272	0.001031	Acidiphilium cryptum
TaxID_148813	0.001029	Synechococcus sp. PCC 6312
TaxID_2033033	0.001026	Gemella sp. oral taxon 928
TaxID_277	0.001017	Campylobacter hominis
TaxID_313985	0.001014	Alcelaphine gammaherpesvirus 1
TaxID_637	0.001012	Chloroflexus aurantiacus
TaxID_44001	0.00101	Halomonas sp. A3H3
TaxID_147645	0.001009	Acetobacter oryzifermentans
TaxID_110321	0.001006	Octadecabacter antarcticus
TaxID_526944	0.001003	Acidovorax sp. RAC01
TaxID_55802	0.001002	Pseudomonas sp. LPH1
TaxID_476157	0.000999	Intestinimonas butyriciproducens
TaxID_135569	0.000998	Geobacillus sp. GHH01
TaxID_162426	0.000994	Dialister pneumosintes
TaxID_103816	0.000993	Sedimentisphaera cyanobacteriorum
TaxID_37734	0.000992	Neisseria sp. 10022
TaxID_535744	0.000992	Parabacteroides distasonis
TaxID_1921125	0.000992	Frankia sp. QA3
TaxID_59310	0.000988	Exiguobacterium sp. AT1b
TaxID_1652133	0.000983	Spiroplasma diminutum
TaxID_849	0.000975	Lutibacter sp. LPB0138
TaxID_330922	0.000972	Streptococcus gallolyticus
TaxID_1018	0.00097	Bacteroides ovatus
TaxID_220622	0.000964	Shewanella frigidimarina
TaxID_1500686	0.000963	Burkholderia sp. BDU8
TaxID_316625	0.000963	Staphylococcus equorum
TaxID_54304	0.000959	[Bacillus] caldolyticus
TaxID_40214	0.000958	Tetragenococcus osmophilus
TaxID_1637853	0.000958	Rhodococcus sp. S2-17
TaxID_188908	0.000956	Streptococcus pneumoniae
TaxID_670	0.000956	Bacteroides thetaiotaomicron
TaxID_2041	0.000954	Bacillus clausii
TaxID_1678129	0.000953	Nitrosomonas ureae
TaxID_53407	0.00095	Streptococcus intermedius
TaxID_1639	0.000947	Saccharomonospora cyanea
TaxID_29471	0.000944	Eubacterium callanderi
TaxID_28028	0.000942	[Eubacterium] rectale
TaxID_33033	0.000942	Desulfuromonas soudanensis
TaxID_77038	0.000942	Streptococcus gordonii
TaxID_374982	0.000941	Megasphaera stantonii
TaxID_1570939	0.000939	Microvirgula aerodenitrificans
TaxID_1126	0.000938	Weissella jogaejeotgali
TaxID_1453352	0.000938	Bacteroides dorei
TaxID_47735	0.000936	Sulfurospirillum sp. JPD-1
TaxID_395962	0.000935	Planococcus faecalis
TaxID_1666906	0.000934	Komagataeibacter hansenii
TaxID_29485	0.000931	Kushneria konosiri
TaxID_45663	0.00093	Rhodococcus pyridinivorans
TaxID_28446	0.00093	Lachnoclostridium phocaeense
TaxID_156976	0.00093	Paraburkholderia caffeinilytica
TaxID_72	0.000928	Streptomyces sp. Tue 6075
TaxID_871007	0.000928	Shewanella sp. TH2012
TaxID_1934946	0.000926	Leuconostoc kimchii
TaxID_1048757	0.000926	Methylobacterium sp. DM1
TaxID_359407	0.000925	Bibersteinia trehalosi
TaxID_229731	0.000925	Acidovorax sp. KKS102
TaxID_315358	0.000923	Neisseria sicca
TaxID_1882831	0.000922	Yersinia pestis
TaxID_543877	0.000922	Neisseria weaveri
TaxID_1737405	0.000922	Oleispira antarctica
TaxID_390805	0.000922	Weissella koreensis
TaxID_850	0.000921	Ruegeria pomeroyi
TaxID_102231	0.000919	Acidilobus sp. 7A
TaxID_2072405	0.000918	Thermococcus barophilus
TaxID_190893	0.000916	Corynebacterium halotolerans
TaxID_47884	0.000915	Corynebacterium pseudotuberculosis
TaxID_921	0.000913	Sulfitobacter sp. AM1-D1
TaxID_1346287	0.00091	Blattabacterium clevelandi
TaxID_1785128	0.000908	Winogradskyella sp. PC-19
TaxID_516051	0.000907	Meiothermus ruber
TaxID_2109687	0.000905	Pectobacterium atrosepticum
TaxID_2211357	0.000903	Flavisolibacter tropicus
TaxID_216572	0.000902	Pseudomonas trivialis
TaxID_321662	0.000899	Spiroplasma eriocheiris
TaxID_1250153	0.000897	Herbinix luporum
TaxID_1761891	0.000894	Acinetobacter guillouiae
TaxID_143786	0.000893	Acinetobacter lactucae
TaxID_1893	0.000893	Rhodococcus jostii
TaxID_114627	0.000893	Dialister sp. Marseille-P5638
TaxID_1643686	0.000892	[Polyangium] brachysporum
TaxID_224379	0.000892	Cutibacterium granulosum
TaxID_1929246	0.000889	Halogeometricum borinquense
TaxID_2016361	0.000887	Actinobacteria bacterium IMCC26077
TaxID_224719	0.000887	Erysipelothrix rhusiopathiae
TaxID_614	0.000886	Mycobacterium sp. YC-RL4
TaxID_670486	0.00088	Afipia sp. GAS231
TaxID_79263	0.000878	Oscillatoriales cyanobacterium JSC-12
TaxID_84565	0.000877	Candidatus Nitrosoglobus terrae
TaxID_1903416	0.000876	Mycoplasma yeatsii
TaxID_380021	0.000875	Staphylococcus muscae
TaxID_513050	0.000874	Thermococcus sp. EXT12c
TaxID_1719	0.00087	Enterococcus gallinarum
TaxID_1517	0.000869	Fusobacterium mortiferum
TaxID_1287055	0.000868	Mycolicibacterium rhodesiae
TaxID_380749	0.000864	Klebsiella pneumoniae
TaxID_53461	0.000863	Rickettsiales endosymbiont of Stachyamoeba lipophora
TaxID_1882747	0.000862	Stenotrophomonas sp. Pemsol
TaxID_450	0.000859	Actinomadura amylolytica
TaxID_1737404	0.000856	Synechococcus sp. KORDI-52
TaxID_1232575	0.000856	Nitrosomonas europaea
TaxID_328814	0.000856	Rhizobium acidisoli
TaxID_1014	0.000853	Rickettsia massiliae
TaxID_65058	0.000852	Streptomyces albus
TaxID_266009	0.000852	Listeria monocytogenes
TaxID_1600	0.000851	Paenibacillus sp. DCT19
TaxID_693075	0.000851	Capnocytophaga sp. ChDC OS43
TaxID_146939	0.000851	Calothrix sp. PCC 7507
TaxID_693073	0.000851	Singulisphaera acidiphila
TaxID_1263550	0.000851	Weissella paramesenteroides
TaxID_1193095	0.00085	Clostridium sp. JN-9
TaxID_693072	0.00085	Lachnoclostridium sp. YL32
TaxID_693071	0.000849	Clostridium sporogenes
TaxID_67814	0.000849	Pseudomonas sp. MT-1
TaxID_136094	0.000849	Corynebacterium testudinoris
TaxID_263819	0.000847	Thalassotalea crassostreae
TaxID_29521	0.000846	Spiroplasma floricola
TaxID_591197	0.000846	Acetobacter senegalensis
TaxID_1038856	0.000845	Campylobacter ureolyticus
TaxID_44008	0.000844	Bordetella genomosp. 9
TaxID_319236	0.000844	Alteromonas sp. BL110
TaxID_912630	0.000841	[Eubacterium] sulci
TaxID_157932	0.00084	Thermotoga profunda
TaxID_35789	0.00084	Microcystis panniformis
TaxID_41276	0.00084	Wolbachia endosymbiont of Cimex lectularius
TaxID_1871034	0.000838	Microbacterium paludicola
TaxID_265293	0.000837	Halobiforma lacisalsi
TaxID_389486	0.000837	Nonlabens sediminis
TaxID_168697	0.000837	Thermococcus radiotolerans
TaxID_59	0.000836	Bacillus flexus
TaxID_336261	0.000835	Arcobacter molluscorum
TaxID_472569	0.000834	Acetobacter aceti
TaxID_823	0.000833	Escherichia albertii
TaxID_183924	0.000831	Nocardioides sp. 603
TaxID_1363	0.000831	Pseudoalteromonas tetraodonis
TaxID_459786	0.000831	Pseudomonas taetrolens
TaxID_195253	0.00083	Gemella sp. ND 6198
TaxID_46354	0.000828	Thermovirga lienii
TaxID_1579	0.000827	Pseudoalteromonas sp. 13-15
TaxID_71667	0.000826	Enterobacter sp. CRENT-193
TaxID_363852	0.000825	Leminorella richardii
TaxID_2493674	0.000823	Stenotrophomonas sp. LM091
TaxID_273677	0.000823	Caldicellulosiruptor kronotskyensis
TaxID_479978	0.000823	Thermoanaerobacterium thermosaccharolyticum
TaxID_203133	0.000822	Escherichia coli
TaxID_136857	0.00082	Nocardiopsis alba
TaxID_203193	0.000817	Pseudomonas viridiflava
TaxID_2210	0.000815	Nitrosospira briensis
TaxID_1978339	0.00081	Thermanaeromonas toyohensis
TaxID_246432	0.000808	Streptomyces qaidamensis
TaxID_1655645	0.000807	Janthinobacterium svalbardensis
TaxID_216936	0.000807	Sedimentitalea sp. W43
TaxID_2019482	0.000806	Bordetella sp. N
TaxID_1921087	0.000806	Pluralibacter gergoviae
TaxID_1148157	0.000803	Myroides profundi
TaxID_50339	0.000803	Devriesea agamarum
TaxID_132919	0.000803	Altererythrobacter marensis
TaxID_1630693	0.000802	Clostridiales bacterium 70B-A
TaxID_2483798	0.000801	Friedmanniella luteola
TaxID_186192	8e-04	Candidatus Babela massiliensis
TaxID_1353	0.000798	Caldicellulosiruptor saccharolyticus
TaxID_1508657	0.000797	Halomonas sp. N3-2A
TaxID_111955	0.000796	Halorubrum lacusprofundi
TaxID_1871111	0.000796	Thermotoga sp. 2812B
TaxID_669357	0.000795	secondary endosymbiont of Ctenarytaina eucalypti
TaxID_41431	0.000795	Streptococcus iniae
TaxID_1211417	0.000794	Spiroplasma kunkelii
TaxID_1846169	0.000792	Planktothrix agardhii
TaxID_1327989	0.000791	Microcystis viridis
TaxID_186532	0.00079	Dictyoglomus turgidum
TaxID_56812	0.00079	Rathayibacter iranicus
TaxID_325455	0.00079	Nakamurella multipartita
TaxID_585425	0.000789	Cutibacterium acnes
TaxID_174	0.000786	Parachlamydia acanthamoebae
TaxID_2128	0.000786	Streptomyces xiamenensis
TaxID_1376	0.000785	Brevundimonas subvibrioides
TaxID_644652	0.000784	Pseudomonas litoralis
TaxID_1178482	0.000784	Blastococcus saxobsidens
TaxID_1331910	0.000783	Bacillus phage AR9
TaxID_1338	0.000783	Streptococcus macedonicus
TaxID_47880	0.000783	Edwardsiella piscicida
TaxID_143361	0.000782	Bacteroides cellulosilyticus
TaxID_90270	0.000782	Legionella longbeachae
TaxID_1598147	0.000778	Candidatus Hepatoplasma crinochetorum
TaxID_873	0.000778	Mesoplasma tabanidae
TaxID_109232	0.000776	Mannheimia sp. USDA-ARS-USMARC-1261
TaxID_2493669	0.000774	Psychrobacter cryohalolentis
TaxID_2321	0.000774	Acinetobacter johnsonii
TaxID_722731	0.000772	Anaerostipes hadrus
TaxID_49546	0.000769	Actinobaculum sp. 313
TaxID_197614	0.000766	Synechococcus sp. KORDI-49
TaxID_2737	0.000766	Moorella thermoacetica
TaxID_2184083	0.000765	Spiroplasma apis
TaxID_2065379	0.000765	Streptococcus pasteurianus
TaxID_1508420	0.000761	[Eubacterium] eligens
TaxID_1778675	0.00076	Spiribacter curvatus
TaxID_28090	0.00076	Sodalis endosymbiont of Henestaris halophilus
TaxID_61651	0.00076	Microbacterium oleivorans
TaxID_2247	0.000759	Pseudomonas vancouverensis
TaxID_1890436	0.000756	Mycobacterium shigaense
TaxID_28077	0.000756	Yersinia aleksiciae
TaxID_29520	0.000754	Burkholderia sp. NRF60-BP8
TaxID_100715	0.000754	Hydrogenovibrio crunogenus
TaxID_754417	0.000752	Serratia sp. FDAARGOS_506
TaxID_53945	0.000752	Rhodoferax saidenbachensis
TaxID_206349	0.000751	Lactobacillus hokkaidonensis
TaxID_57487	0.00075	Cyanothece sp. PCC 8802
TaxID_28199	0.000749	Geobacter lovleyi
TaxID_1601	0.000747	Serratia ficaria
TaxID_1610	0.000747	Shimwellia blattae
TaxID_100716	0.000744	Rickettsia monacensis
TaxID_1677050	0.000744	Sphingobium sp. SYK-6
TaxID_2497860	0.000743	Legionella clemsonensis
TaxID_53345	0.000742	Vibrio coralliilyticus
TaxID_748770	0.00074	Akkermansia muciniphila
TaxID_200644	0.000739	Bifidobacterium adolescentis
TaxID_1882749	0.000738	Micromonospora tulbaghiae
TaxID_546871	0.000736	Devosia sp. I507
TaxID_1646498	0.000733	Paracoccus aminovorans
TaxID_1199245	0.000733	Paraburkholderia sprentiae
TaxID_2137	0.000731	Brucella sp. 09RB8910
TaxID_366602	0.000729	Fusobacterium gonidiaformans
TaxID_99598	0.000729	Actinomyces sp. Chiba101
TaxID_1834205	0.000729	Streptomyces lydicus
TaxID_72361	0.000728	Yersinia rohdei
TaxID_44000	0.000728	Pseudomonas fulva
TaxID_53442	0.000726	Bacillus sp. IHB B 7164
TaxID_33995	0.000726	Nitrosospira lacus
TaxID_1661694	0.000725	Geminocystis sp. NIES-3709
TaxID_1827580	0.000725	Streptomyces sp. TN58
TaxID_1385592	0.000725	Candidatus Fukatsuia symbiotica
TaxID_1855380	0.000725	Fusobacterium periodonticum
TaxID_31979	0.000725	Acinetobacter oleivorans
TaxID_827	0.000723	Streptomyces noursei
TaxID_33069	0.000721	Finegoldia magna
TaxID_1286	0.00072	Cronobacter muytjensii
TaxID_216933	0.00072	Lactobacillus acetotolerans
TaxID_57479	0.000718	Yersinia kristensenii
TaxID_2497645	0.000717	Corynebacterium riegelii
TaxID_2497644	0.000717	Agromyces flavus
TaxID_423604	0.000717	Oceanisphaera avium
TaxID_472568	0.000717	Pseudomonas sp. K2W31S-8
TaxID_860	0.000716	Chlorobium chlorochromatii
TaxID_2303	0.000714	Microcystis aeruginosa
TaxID_1867846	0.000714	Bifidobacterium pseudocatenulatum
TaxID_1226968	0.000712	Alcanivorax pacificus
TaxID_2173169	0.00071	Arthrobacter sp. QXT-31
TaxID_263377	0.000709	Serratia liquefaciens
TaxID_1538158	0.000706	Fusobacterium varium
TaxID_150396	0.000706	Saccharomonospora glauca
TaxID_70411	0.000705	Sinorhizobium meliloti
TaxID_1925548	0.000704	Starkeya novella
TaxID_446692	0.000704	Gallionella capsiferriformans
TaxID_1160	0.000703	Corynebacterium ulcerans
TaxID_2027857	0.000703	Corynebacterium pseudopelargi
TaxID_1616789	0.000701	Chlamydia muridarum
TaxID_89184	0.000701	Candidatus Fokinia solitaria
TaxID_241425	0.000701	Cellulosilyticum sp. WCF-2
TaxID_35791	7e-04	Erysipelotrichaceae bacterium GAM147
TaxID_92712	0.000698	Aciduliprofundum boonei
TaxID_10240	0.000698	Ornithobacterium rhinotracheale
TaxID_81475	0.000698	Hartmannibacter diazotrophicus
TaxID_52584	0.000698	Chloroherpeton thalassium
TaxID_1702325	0.000698	Filifactor alocis
TaxID_1054217	0.000697	Streptomyces nigra
TaxID_57480	0.000696	Pseudomonas sp. NC02
TaxID_1676125	0.000696	Verrucomicrobium sp. GAS474
TaxID_2322	0.000696	Sediminicola sp. YIK13
TaxID_1288494	0.000696	Anaerotignum propionicum
TaxID_1682113	0.000695	Parvimonas micra
TaxID_357794	0.000695	Azospirillum humicireducens
TaxID_925818	0.000695	Scytonema sp. HK-05
TaxID_408015	0.000695	Altererythrobacter ishigakiensis
TaxID_1785995	0.000694	Lawsonella clevelandensis
TaxID_55601	0.000694	Desulfovibrio africanus
TaxID_360911	0.000691	Thermococcus guaymasensis
TaxID_2066070	0.000691	Parabacteroides sp. CT06
TaxID_40989	0.00069	Brevefilum fermentans
TaxID_1848755	0.00069	Bartonella sp. 1-1C
TaxID_1357915	0.00069	Capnocytophaga ochracea
TaxID_2040586	0.000688	Allofrancisella guangzhouensis
TaxID_1082702	0.000687	[Clostridium] innocuum
TaxID_1152	0.000686	Thauera hydrothermalis
TaxID_1784836	0.000685	Owenweeksia hongkongensis
TaxID_1297617	0.000685	Phenylobacterium sp. HYN0004
TaxID_831	0.000685	Candidatus Moranella endobia
TaxID_1679721	0.000683	Delftia acidovorans
TaxID_136073	0.000682	Yersinia similis
TaxID_76833	0.000682	Alistipes shahii
TaxID_515256	0.000682	Thermoanaerobacter wiegelii
TaxID_1964449	0.000682	Streptococcus sp. ChDC B345
TaxID_1562970	0.000681	Sinorhizobium medicae
TaxID_1137095	0.000681	Microbacterium sp. LKL04
TaxID_56106	0.00068	Sulfurisphaera tokodaii
TaxID_110164	0.00068	Thermodesulfobacterium geofontis
TaxID_2071627	0.000679	Leptospira borgpetersenii
TaxID_370405	0.000678	Pantoea agglomerans
TaxID_199596	0.000678	Bacteroides phage B124-14
TaxID_632	0.000677	Pseudomonas sp. S-6-2
TaxID_158481	0.000675	Parabacteroides phage YZ-2015b
TaxID_466153	0.000675	Blochmannia endosymbiont of Camponotus (Colobopsis) obliquus
TaxID_927083	0.000675	Brachyspira hampsonii
TaxID_28152	0.000675	Streptomyces olivaceus
TaxID_117743	0.000674	Methanobrevibacter sp. AbM4
TaxID_1457153	0.000674	Tenacibaculum todarodis
TaxID_1647413	0.000674	uncultured crAssphage
TaxID_944644	0.000674	Macaca nemestrina herpesvirus 7
TaxID_379547	0.000673	Dokdonia sp. 4H-3-7-5
TaxID_2183	0.000673	Ruminococcus bicirculans
TaxID_2182	0.000672	Arcobacter bivalviorum
TaxID_34093	0.000672	Sphingomonas melonis
TaxID_146937	0.000671	Salinispora arenicola
TaxID_34004	0.000671	Micromonospora inositola
TaxID_2495645	0.00067	Acinetobacter sp. TTH0-4
TaxID_2420306	0.00067	Mesorhizobium japonicum
TaxID_413889	0.00067	Idiomarina sp. OT37-5b
TaxID_375288	0.00067	Pedobacter ginsengisoli
TaxID_28903	0.00067	Faecalibacterium
TaxID_321332	0.000669	Microvirga
TaxID_47763	0.000668	Methylocella
TaxID_1492898	0.000667	Azospira
TaxID_1896175	0.000667	Candidatus Fokinia
TaxID_28116	0.000666	Frateuria
TaxID_1746199	0.000666	Candidatus Riesia
TaxID_2005525	0.000666	Escherichia
TaxID_76947	0.000665	Brevefilum
TaxID_363952	0.000665	Clostridium
TaxID_28087	0.000665	Opitutus
TaxID_1888	0.000665	Alistipes
TaxID_1755811	0.000664	Salipiger
TaxID_1871021	0.000663	Methylotenera
TaxID_178440	0.000662	Elusimicrobium
TaxID_83560	0.00066	Parasaccharibacter
TaxID_217161	0.00066	Bifidobacterium
TaxID_61647	0.00066	Oscillatoria
TaxID_1470176	0.000659	Devriesea
TaxID_2173034	0.000658	Solitalea
TaxID_1513258	0.000658	Vitreoscilla
TaxID_85032	0.000658	Pseudanabaena
TaxID_795750	0.000658	Photorhabdus
TaxID_1643683	0.000658	Candidatus Moranella
TaxID_255472	0.000657	Methanohalobium
TaxID_473531	0.000657	Simkania
TaxID_67575	0.000657	Desulfobacca
TaxID_593909	0.000657	Chamaesiphon
TaxID_1617448	0.000657	Erysipelatoclostridium
TaxID_46255	0.000657	Hydrogenophilus
TaxID_2025876	0.000656	Immundisolibacter
TaxID_292566	0.000656	Geminocystis
TaxID_1602345	0.000655	Dolichospermum
TaxID_101571	0.000654	Thalassotalea
TaxID_1971	0.000653	Croceibacter
TaxID_929509	0.000653	Caldisericum
TaxID_313590	0.000651	Propionimicrobium
TaxID_2080757	0.00065	Weissella
TaxID_689	0.00065	Novibacillus
TaxID_1714845	0.00065	Desulfohalobium
TaxID_10239	0.000646	Planctopirus
TaxID_1763536	0.000645	Salmonella
TaxID_709797	0.000645	Candidatus Fukatsuia
TaxID_469322	0.000644	Paenalcaligenes
TaxID_1890431	0.000643	Microvirgula
TaxID_418223	0.000643	Agarivorans
TaxID_1032072	0.000643	Saccharophagus
TaxID_1577685	0.000643	Gloeocapsa
TaxID_315405	0.000643	Brachyspira
TaxID_1055686	0.000642	Sodalis
TaxID_2067957	0.000642	Mycoavidus
TaxID_166935	0.000642	Finegoldia
TaxID_1921510	0.000642	Halobacteriovorax
TaxID_105850	0.000641	Geosporobacter
TaxID_1917485	0.000641	Ezakiella
TaxID_594679	0.00064	Iodobacter
TaxID_132920	0.000639	Rheinheimera
TaxID_1217908	0.000639	Herbinix
TaxID_185008	0.000639	C2virus
TaxID_28026	0.000638	Candidatus Doolittlea
TaxID_1603606	0.000638	Lachnoclostridium
TaxID_1433513	0.000637	Paucibacter
TaxID_296	0.000637	Oscillibacter
TaxID_2320867	0.000637	Salinisphaera
TaxID_1810868	0.000636	Eubacterium
TaxID_29443	0.000636	Arsenophonus
TaxID_1525	0.000634	Methylacidiphilum
TaxID_358220	0.000634	Chloroherpeton
TaxID_2201350	0.000633	Roseolovirus
TaxID_2497643	0.000631	Dehalogenimonas
TaxID_1914992	0.000631	Cellulosilyticum
TaxID_1805827	0.00063	Actinobaculum
TaxID_1375	0.00063	Kushneria
TaxID_74313	0.00063	Butyrivibrio
TaxID_256845	0.000629	Nitrobacter
TaxID_34084	0.000629	Thermoplasma
TaxID_1538	0.000629	Ornithobacterium
TaxID_2211212	0.000629	Ignavibacterium
TaxID_449719	0.000628	Fusobacterium
TaxID_1836467	0.000628	Caldicellulosiruptor
TaxID_368607	0.000628	Lawsonella
TaxID_2060312	0.000628	Salinispira
TaxID_57029	0.000628	Vogesella
TaxID_417	0.000626	Filifactor
TaxID_663364	0.000626	Hahella
TaxID_239759	0.000625	Candidatus Cloacimonas
TaxID_300019	0.000625	Planktothrix
TaxID_1663717	0.000624	Steroidobacter
TaxID_1334	0.000623	Cuniculiplasma
TaxID_84992	0.000621	Desulfofarcimen
TaxID_504090	0.000621	Cylindrospermum
TaxID_253245	0.000621	Afipia
TaxID_1844051	0.000621	Entomoplasma
TaxID_46238	0.00062	Aerococcus
TaxID_97477	0.00062	Bibersteinia
TaxID_2100422	0.000618	Akkermansia
TaxID_209053	0.000618	Leminorella
TaxID_1411117	0.000618	Candidatus Methanoplasma
TaxID_1850526	0.000617	Klebsiella
TaxID_1177712	0.000615	Phascolarctobacterium
TaxID_2014541	0.000615	Flavonifractor
TaxID_1048758	0.000615	Lonsdalea
TaxID_1926	0.000615	Parabacteroides
TaxID_170679	0.000614	Alkaliphilus
TaxID_413882	0.000614	Paraphotobacterium
TaxID_2045213	0.000613	Sulfuricurvum
TaxID_138336	0.000613	Thermogutta
TaxID_2040624	0.000613	Vagococcus
TaxID_1892255	0.000612	Saccharospirillum
TaxID_2109692	0.000612	Gordonibacter
TaxID_563	0.000611	Acetomicrobium
TaxID_1306787	0.000611	Gammapapillomavirus
TaxID_807	0.000609	Acidilobus
TaxID_1157948	0.000609	Ruminiclostridium
TaxID_513223	0.000606	Psychroflexus
TaxID_57739	0.000606	Anaerostipes
TaxID_1986204	0.000606	Cutibacterium
TaxID_1607807	0.000605	Ethanoligenens
TaxID_588932	0.000604	Halobiforma
TaxID_120652	0.000604	Sedimentitalea
TaxID_35252	0.000603	Candidatus Nitrosoglobus
TaxID_1240483	0.000601	Paucimonas
TaxID_2052660	0.000601	Candidatus Fonsibacter
TaxID_216431	0.000599	Jeotgalibacillus
TaxID_2014742	0.000599	Octadecabacter
TaxID_1981173	0.000599	Ruminococcus
TaxID_1249	0.000598	Fermentimonas
TaxID_1158	0.000598	Sediminicola
TaxID_136609	0.000597	Riemerella
TaxID_171550	0.000597	P70virus
TaxID_1815509	0.000597	Parvimonas
TaxID_1352	0.000597	Hydrogenophilaceae
TaxID_1920191	0.000597	Immundisolibacteraceae
TaxID_1631871	0.000597	Hahellaceae
TaxID_920	0.000596	Orbaceae
TaxID_219745	0.000596	Acidothermaceae
TaxID_44259	0.000595	Ruminococcaceae
TaxID_476528	0.000595	Peptoniphilaceae
TaxID_2080469	0.000595	Granulosicoccaceae
TaxID_10841	0.000595	Simkaniaceae
TaxID_255474	0.000594	Parvularculaceae
TaxID_255473	0.000594	Rikenellaceae
TaxID_1173032	0.000593	Anaplasmataceae
TaxID_549	0.000593	Methanococcaceae
TaxID_1904944	0.000593	Eubacteriaceae
TaxID_83612	0.000593	Fusobacteriaceae
TaxID_186826	0.000593	Lactobacillaceae
TaxID_2051905	0.000592	Desulfohalobiaceae
TaxID_1649480	0.000592	Bifidobacteriaceae
TaxID_2479393	0.000592	Akkermansiaceae
TaxID_40990	0.000592	Sandaracinaceae
TaxID_1260	0.000592	Nitrosopumilaceae
TaxID_29487	0.000592	Halobacteriovoraceae
TaxID_2201356	0.000591	Budviciaceae
TaxID_673862	0.000591	Candidatus Babeliaceae
TaxID_83552	0.00059	Clostridiaceae
TaxID_1633874	0.00059	Tannerellaceae
TaxID_33958	0.000589	Caldisphaeraceae
TaxID_1596	0.000589	Marseilleviridae
TaxID_186650	0.000589	Pseudanabaenaceae
TaxID_1432056	0.000589	Brachyspiraceae
TaxID_1295609	0.000589	Gomontiellaceae
TaxID_1305	0.000589	Microviridae
TaxID_1783515	0.000589	Thermoanaerobacterales Family III. Incertae Sedis
TaxID_1852374	0.000589	Poxviridae
TaxID_524	0.000588	Enterococcaceae
TaxID_106649	0.000588	Flavobacteriaceae
TaxID_2487150	0.000588	Caldisericaceae
TaxID_490	0.000586	Thermoplasmataceae
TaxID_29498	0.000585	Chamaesiphonaceae
TaxID_53437	0.000583	Salinisphaeraceae
TaxID_1862958	0.000583	Acidilobaceae
TaxID_1545835	0.000583	Enterobacteriaceae
TaxID_32014	0.000582	Cuniculiplasmataceae
TaxID_82985	0.000581	Oscillospiraceae
TaxID_386487	0.000581	Enterobacterales
TaxID_299262	0.000581	Parvularculales
TaxID_267818	0.00058	Fusobacteriales
TaxID_2004644	0.000579	Lactobacillales
TaxID_1891926	0.000578	Brachyspirales
TaxID_2144175	0.000578	Caldisericales
TaxID_1173263	0.000577	Acidilobales
TaxID_543371	0.000577	Candidatus Babeliales
TaxID_28091	0.000576	Flavobacteriales
TaxID_1577791	0.000576	Nitrosopumilales
TaxID_911	0.000576	Acidothermales
TaxID_1365176	0.000576	Tissierellales
TaxID_1727163	0.000576	Bifidobacteriales
TaxID_1033	0.000575	Methanococcales
TaxID_1248727	0.000575	Verrucomicrobiales
TaxID_817	0.000575	Actinobacteria
TaxID_511745	0.000575	Verrucomicrobiae
TaxID_1903694	0.000574	Thermoprotei
TaxID_216937	0.000574	Caldisericia
TaxID_295596	0.000574	Flavobacteriia
TaxID_710111	0.000574	Gammaproteobacteria
TaxID_60847	0.000574	Tissierellia
TaxID_1518149	0.000574	Fusobacteriia
TaxID_1641	0.000573	Candidatus Babeliae
TaxID_465721	0.000573	Acidimicrobiia
TaxID_2507159	0.000573	Actinobacteria
TaxID_261825	0.000572	Verrucomicrobia
TaxID_225326	0.000572	Proteobacteria
TaxID_150022	0.000572	Fusobacteria
TaxID_1224749	0.000571	Caldiserica
TaxID_81852	0.000571	Thaumarchaeota
TaxID_33024	0.000571	Lentisphaerae
TaxID_1990687	0.00057	Viruses

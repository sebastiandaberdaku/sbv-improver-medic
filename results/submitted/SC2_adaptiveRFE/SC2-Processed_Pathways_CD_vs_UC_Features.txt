PathwayID	Importance_Optional	Description
PathID_11461	0.005935	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Parasutterella.s__Parasutterella_excrementihominis
PathID_8918	0.005646	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Bacteroides.s__Bacteroides_xylanisolvens
PathID_4700	0.005445	PWY-6700: queuosine biosynthesis|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_6864	0.005402	PWY0-1297: superpathway of purine deoxyribonucleosides degradation
PathID_4869	0.00533	UNINTEGRATED|g__Dorea.s__Dorea_longicatena
PathID_12177	0.005305	PWY-6122: 5-aminoimidazole ribonucleotide biosynthesis II|g__Dorea.s__Dorea_longicatena
PathID_1815	0.005254	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Clostridium.s__Clostridium_bolteae
PathID_11280	0.005231	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Bacteroides.s__Bacteroides_xylanisolvens
PathID_1390	0.005155	PWY-6124: inosine-5'-phosphate biosynthesis II|g__Bacteroides.s__Bacteroides_finegoldii
PathID_9383	0.005107	UNINTEGRATED|g__Parabacteroides.s__Parabacteroides_johnsonii
PathID_1364	0.005033	UNINTEGRATED|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_3577	0.004923	ASPASN-PWY: superpathway of L-aspartate and L-asparagine biosynthesis|g__Bacteroides.s__Bacteroides_faecis
PathID_9199	0.004779	PWY-5097: L-lysine biosynthesis VI|g__Bacteroides.s__Bacteroides_finegoldii
PathID_7290	0.004536	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_12305	0.004232	PWY-5188: tetrapyrrole biosynthesis I (from glutamate)|g__Bilophila.s__Bilophila_wadsworthia
PathID_9196	0.004229	PWY-7184: pyrimidine deoxyribonucleotides de novo biosynthesis I|g__Parasutterella.s__Parasutterella_excrementihominis
PathID_11512	0.00419	PWY-6123: inosine-5'-phosphate biosynthesis I|g__Bacteroides.s__Bacteroides_salyersiae
PathID_8275	0.004126	DTDPRHAMSYN-PWY: dTDP-L-rhamnose biosynthesis I|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_4161	0.004077	THRESYN-PWY: superpathway of L-threonine biosynthesis|g__Bifidobacterium.s__Bifidobacterium_adolescentis
PathID_4210	0.0039	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Bifidobacterium.s__Bifidobacterium_adolescentis
PathID_10000	0.003742	ARGININE-SYN4-PWY: L-ornithine de novo  biosynthesis|g__Bacteroides.s__Bacteroides_uniformis
PathID_8890	0.003623	PWY-6608: guanosine nucleotides degradation III|g__Parabacteroides.s__Parabacteroides_johnsonii
PathID_933	0.003422	NAGLIPASYN-PWY: lipid IVA biosynthesis|g__Parasutterella.s__Parasutterella_excrementihominis
PathID_8647	0.002863	PWY-5097: L-lysine biosynthesis VI|g__Roseburia.s__Roseburia_intestinalis
PathID_8117	0.002452	PWY-5695: urate biosynthesis/inosine 5'-phosphate degradation|g__Bacteroides.s__Bacteroides_finegoldii
PathID_11096	0.002441	HOMOSER-METSYN-PWY: L-methionine biosynthesis I|g__Anaerostipes.s__Anaerostipes_hadrus
PathID_5870	0.002413	CENTFERM-PWY: pyruvate fermentation to butanoate
PathID_7784	0.002351	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Bilophila.s__Bilophila_wadsworthia
PathID_9728	0.002296	PWY-3001: superpathway of L-isoleucine biosynthesis I|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_1961	0.002231	RHAMCAT-PWY: L-rhamnose degradation I|g__Roseburia.s__Roseburia_intestinalis
PathID_1573	0.002178	ASPASN-PWY: superpathway of L-aspartate and L-asparagine biosynthesis|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_4055	0.002176	PYRIDNUCSYN-PWY: NAD biosynthesis I (from aspartate)|g__Roseburia.s__Roseburia_intestinalis
PathID_3150	0.002127	VALSYN-PWY: L-valine biosynthesis|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_12600	0.002055	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Bacteroides.s__Bacteroides_uniformis
PathID_7824	0.002049	PWY-5686: UMP biosynthesis|g__Bacteroides.s__Bacteroides_uniformis
PathID_3730	0.002022	PWY66-409: superpathway of purine nucleotide salvage
PathID_2374	0.001971	UNINTEGRATED|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_5567	0.00197	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Bilophila.s__Bilophila_wadsworthia
PathID_2276	0.001957	PWY-6123: inosine-5'-phosphate biosynthesis I|g__Bacteroides.s__Bacteroides_xylanisolvens
PathID_171	0.00192	PWY0-1296: purine ribonucleosides degradation|g__Clostridium.s__Clostridium_clostridioforme
PathID_12534	0.001763	NONMEVIPP-PWY: methylerythritol phosphate pathway I|g__Parasutterella.s__Parasutterella_excrementihominis
PathID_2687	0.00176	UNINTEGRATED|g__Clostridium.s__Clostridium_citroniae
PathID_6360	0.001751	PANTO-PWY: phosphopantothenate biosynthesis I|g__Parabacteroides.s__Parabacteroides_johnsonii
PathID_1242	0.001751	PWY-6737: starch degradation V|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_7_1_58FAA
PathID_3136	0.00174	BRANCHED-CHAIN-AA-SYN-PWY: superpathway of branched amino acid biosynthesis|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_5481	0.001713	GLYCOGENSYNTH-PWY: glycogen biosynthesis I (from ADP-D-Glucose)|g__Anaerostipes.s__Anaerostipes_hadrus
PathID_6875	0.001712	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Bacteroides.s__Bacteroides_intestinalis
PathID_4743	0.001708	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Bacteroides.s__Bacteroides_uniformis
PathID_7231	0.001681	PANTO-PWY: phosphopantothenate biosynthesis I|g__Bacteroides.s__Bacteroides_faecis
PathID_12488	0.001674	PWY-3841: folate transformations II|g__Bacteroides.s__Bacteroides_finegoldii
PathID_7621	0.001653	PWY-5188: tetrapyrrole biosynthesis I (from glutamate)
PathID_3286	0.001648	PWY-6124: inosine-5'-phosphate biosynthesis II|g__Bacteroides.s__Bacteroides_xylanisolvens
PathID_11687	0.00164	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Bacteroides.s__Bacteroides_salyersiae
PathID_11172	0.001624	PWY-6124: inosine-5'-phosphate biosynthesis II|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_2230	0.001621	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Holdemania.s__Holdemania_filiformis
PathID_820	0.001619	PWY-6121: 5-aminoimidazole ribonucleotide biosynthesis I|g__Holdemania.s__Holdemania_filiformis
PathID_7812	0.001617	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Anaerostipes.s__Anaerostipes_hadrus
PathID_4532	0.001615	PWY-7663: gondoate biosynthesis (anaerobic)|g__Bacteroides.s__Bacteroides_faecis
PathID_6923	0.001612	PWY-5695: urate biosynthesis/inosine 5'-phosphate degradation|g__Bacteroides.s__Bacteroides_salyersiae
PathID_4969	0.001609	THISYNARA-PWY: superpathway of thiamin diphosphate biosynthesis III (eukaryotes)|g__Bacteroides.s__Bacteroides_xylanisolvens
PathID_7539	0.001602	PWY0-162: superpathway of pyrimidine ribonucleotides de novo biosynthesis
PathID_10912	0.0016	PWY-6151: S-adenosyl-L-methionine cycle I|g__Anaerostipes.s__Anaerostipes_hadrus
PathID_5655	0.001549	VALSYN-PWY: L-valine biosynthesis|g__Bacteroides.s__Bacteroides_finegoldii
PathID_9729	0.001535	VALSYN-PWY: L-valine biosynthesis|g__Clostridium.s__Clostridium_citroniae
PathID_3785	0.001529	PWY-7111: pyruvate fermentation to isobutanol (engineered)|g__Bacteroides.s__Bacteroides_finegoldii
PathID_10549	0.001525	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Clostridium.s__Clostridium_bolteae
PathID_10139	0.001518	PWY-7208: superpathway of pyrimidine nucleobases salvage|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_4823	0.001517	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Bilophila.s__Bilophila_wadsworthia
PathID_11322	0.001517	UNINTEGRATED|g__Alistipes.s__Alistipes_senegalensis
PathID_7692	0.001516	PWY-621: sucrose degradation III (sucrose invertase)|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_2525	0.001489	PANTO-PWY: phosphopantothenate biosynthesis I|g__Bilophila.s__Bilophila_wadsworthia
PathID_1710	0.001479	PYRIDNUCSYN-PWY: NAD biosynthesis I (from aspartate)|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_11953	0.001479	PWY-6163: chorismate biosynthesis from 3-dehydroquinate|g__Clostridium.s__Clostridium_clostridioforme
PathID_11738	0.001477	PWY-5695: urate biosynthesis/inosine 5'-phosphate degradation|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_1839	0.001476	PWY-7663: gondoate biosynthesis (anaerobic)|g__Bacteroides.s__Bacteroides_salyersiae
PathID_5089	0.00147	PWY-5304: superpathway of sulfur oxidation (Acidianus ambivalens)|unclassified
PathID_1265	0.001465	NONMEVIPP-PWY: methylerythritol phosphate pathway I|g__Bifidobacterium.s__Bifidobacterium_adolescentis
PathID_12615	0.001452	PWY-6703: preQ0 biosynthesis|g__Bacteroides.s__Bacteroides_salyersiae
PathID_6252	0.001449	RHAMCAT-PWY: L-rhamnose degradation I|g__Faecalibacterium.s__Faecalibacterium_prausnitzii
PathID_6890	0.001445	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Bacteroides.s__Bacteroides_salyersiae
PathID_4483	0.00144	PYRIDOXSYN-PWY: pyridoxal 5'-phosphate biosynthesis I|g__Bacteroides.s__Bacteroides_faecis
PathID_6558	0.001439	ORNDEG-PWY: superpathway of ornithine degradation
PathID_12043	0.001437	PWY-6125: superpathway of guanosine nucleotides de novo biosynthesis II|g__Parasutterella.s__Parasutterella_excrementihominis
PathID_6237	0.001429	PWY-2942: L-lysine biosynthesis III|g__Bacteroides.s__Bacteroides_finegoldii
PathID_760	0.001428	PWY-5686: UMP biosynthesis|g__Holdemania.s__Holdemania_filiformis
PathID_3500	0.001427	PWY-6700: queuosine biosynthesis|g__Parasutterella.s__Parasutterella_excrementihominis
PathID_6943	0.001424	PWY-5097: L-lysine biosynthesis VI|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_2792	0.001421	PWY-6121: 5-aminoimidazole ribonucleotide biosynthesis I|g__Clostridium.s__Clostridium_clostridioforme
PathID_10758	0.00141	PWY-6277: superpathway of 5-aminoimidazole ribonucleotide biosynthesis|g__Bilophila.s__Bilophila_wadsworthia
PathID_2973	0.001409	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Parabacteroides.s__Parabacteroides_johnsonii
PathID_5798	0.001409	PWY-6385: peptidoglycan biosynthesis III (mycobacteria)|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_8942	0.001405	PWY-6703: preQ0 biosynthesis|g__Bacteroides.s__Bacteroides_faecis
PathID_7169	0.0014	ASPASN-PWY: superpathway of L-aspartate and L-asparagine biosynthesis|g__Bacteroides.s__Bacteroides_xylanisolvens
PathID_7244	0.0014	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_9188	0.001399	PWY-6151: S-adenosyl-L-methionine cycle I|g__Bacteroides.s__Bacteroides_uniformis
PathID_5512	0.001399	PWY-6122: 5-aminoimidazole ribonucleotide biosynthesis II|g__Bilophila.s__Bilophila_wadsworthia
PathID_782	0.001398	PYRIDNUCSYN-PWY: NAD biosynthesis I (from aspartate)|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_6452	0.001391	PWY-7663: gondoate biosynthesis (anaerobic)|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_11808	0.001386	PWY-6897: thiamin salvage II
PathID_876	0.001378	PWY-5659: GDP-mannose biosynthesis|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_7114	0.001377	UNINTEGRATED|g__Bacteroides.s__Bacteroides_salyersiae
PathID_10882	0.00137	UNINTEGRATED|g__Bacteroides.s__Bacteroides_finegoldii
PathID_3847	0.001365	1CMET2-PWY: N10-formyl-tetrahydrofolate biosynthesis|g__Bacteroides.s__Bacteroides_finegoldii
PathID_1392	0.001363	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Bacteroides.s__Bacteroides_salyersiae
PathID_4737	0.001349	RHAMCAT-PWY: L-rhamnose degradation I|g__Bacteroides.s__Bacteroides_faecis
PathID_9092	0.001347	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Dialister.s__Dialister_invisus
PathID_6043	0.001346	PANTO-PWY: phosphopantothenate biosynthesis I|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_11326	0.001343	PWY-4981: L-proline biosynthesis II (from arginine)|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_11690	0.001341	PWY-7229: superpathway of adenosine nucleotides de novo biosynthesis I
PathID_11307	0.001333	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Bacteroides.s__Bacteroides_finegoldii
PathID_8919	0.00133	PWY-2942: L-lysine biosynthesis III|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_4187	0.00133	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Bilophila.s__Bilophila_wadsworthia
PathID_6612	0.001327	HEXITOLDEGSUPER-PWY: superpathway of hexitol degradation (bacteria)
PathID_3742	0.00132	PANTO-PWY: phosphopantothenate biosynthesis I|g__Bacteroides.s__Bacteroides_uniformis
PathID_1861	0.001316	PANTOSYN-PWY: pantothenate and coenzyme A biosynthesis I|g__Bacteroides.s__Bacteroides_xylanisolvens
PathID_6296	0.001314	1CMET2-PWY: N10-formyl-tetrahydrofolate biosynthesis|g__Bacteroides.s__Bacteroides_faecis
PathID_1840	0.001312	PYRIDOXSYN-PWY: pyridoxal 5'-phosphate biosynthesis I|g__Bacteroides.s__Bacteroides_uniformis
PathID_9026	0.001311	PWY-2942: L-lysine biosynthesis III|g__Bilophila.s__Bilophila_wadsworthia
PathID_6769	0.001311	PWY-6270: isoprene biosynthesis I
PathID_5311	0.001311	PWY-5097: L-lysine biosynthesis VI|g__Anaerostipes.s__Anaerostipes_hadrus
PathID_4032	0.00131	PWY-5659: GDP-mannose biosynthesis|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_10530	0.001307	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Bacteroides.s__Bacteroides_faecis
PathID_10975	0.001301	PWY-6700: queuosine biosynthesis|g__Bacteroides.s__Bacteroides_finegoldii
PathID_1471	0.001298	UDPNAGSYN-PWY: UDP-N-acetyl-D-glucosamine biosynthesis I|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_5805	0.001282	PWY-5695: urate biosynthesis/inosine 5'-phosphate degradation|g__Parabacteroides.s__Parabacteroides_johnsonii
PathID_1229	0.001279	PWY-5686: UMP biosynthesis|g__Bilophila.s__Bilophila_wadsworthia
PathID_529	0.001278	PWY-1042: glycolysis IV (plant cytosol)|g__Bacteroides.s__Bacteroides_xylanisolvens
PathID_2937	0.001278	PWY-6151: S-adenosyl-L-methionine cycle I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_4671	0.001278	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Bacteroides.s__Bacteroides_intestinalis
PathID_1679	0.001271	PWY-6737: starch degradation V|g__Dorea.s__Dorea_longicatena
PathID_11701	0.00127	METSYN-PWY: L-homoserine and L-methionine biosynthesis|g__Anaerostipes.s__Anaerostipes_hadrus
PathID_11751	0.00127	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Bacteroides.s__Bacteroides_uniformis
PathID_9015	0.001267	CALVIN-PWY: Calvin-Benson-Bassham cycle|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_4333	0.00126	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Parabacteroides.s__Parabacteroides_johnsonii
PathID_2079	0.001257	UNINTEGRATED|g__Clostridium.s__Clostridium_clostridioforme
PathID_5960	0.001254	PWY-7220: adenosine deoxyribonucleotides de novo biosynthesis II
PathID_10169	0.001249	PWY-6277: superpathway of 5-aminoimidazole ribonucleotide biosynthesis|g__Dorea.s__Dorea_longicatena
PathID_6499	0.001247	PWY-4981: L-proline biosynthesis II (from arginine)|g__Eubacterium.s__Eubacterium_ramulus
PathID_1477	0.001247	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Bacteroides.s__Bacteroides_uniformis
PathID_2449	0.001245	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Bacteroides.s__Bacteroides_finegoldii
PathID_5152	0.001245	THRESYN-PWY: superpathway of L-threonine biosynthesis|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_6854	0.001245	ARO-PWY: chorismate biosynthesis I|g__Clostridium.s__Clostridium_clostridioforme
PathID_7858	0.001245	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_5843	0.001241	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Parabacteroides.s__Parabacteroides_johnsonii
PathID_7399	0.001239	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Faecalibacterium.s__Faecalibacterium_prausnitzii
PathID_5811	0.001238	PWY-6700: queuosine biosynthesis|g__Anaerostipes.s__Anaerostipes_hadrus
PathID_7083	0.001237	PWY-6590: superpathway of Clostridium acetobutylicum acidogenic fermentation
PathID_2605	0.001232	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Roseburia.s__Roseburia_intestinalis
PathID_1072	0.001228	PWY-7222: guanosine deoxyribonucleotides de novo biosynthesis II
PathID_1821	0.001226	VALSYN-PWY: L-valine biosynthesis|g__Bacteroides.s__Bacteroides_salyersiae
PathID_2806	0.001216	GLUCUROCAT-PWY: superpathway of &beta;-D-glucuronide and D-glucuronate degradation
PathID_624	0.001213	ARGININE-SYN4-PWY: L-ornithine de novo  biosynthesis|g__Bacteroides.s__Bacteroides_finegoldii
PathID_4395	0.001212	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Bacteroides.s__Bacteroides_finegoldii
PathID_8192	0.001211	PWY-6609: adenine and adenosine salvage III|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_2682	0.001208	PWY-6151: S-adenosyl-L-methionine cycle I|g__Paraprevotella.s__Paraprevotella_xylaniphila
PathID_1953	0.001208	PWY66-399: gluconeogenesis III
PathID_9095	0.001205	ARGININE-SYN4-PWY: L-ornithine de novo  biosynthesis|g__Bacteroides.s__Bacteroides_xylanisolvens
PathID_6623	0.001204	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Bacteroides.s__Bacteroides_xylanisolvens
PathID_5909	0.001204	PWY-6124: inosine-5'-phosphate biosynthesis II|g__Bacteroides.s__Bacteroides_faecis
PathID_462	0.001202	RHAMCAT-PWY: L-rhamnose degradation I|g__Holdemania.s__Holdemania_filiformis
PathID_8827	0.001197	PWY-5695: urate biosynthesis/inosine 5'-phosphate degradation|g__Bacteroides.s__Bacteroides_faecis
PathID_10081	0.001197	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Bacteroides.s__Bacteroides_salyersiae
PathID_7712	0.001196	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Bifidobacterium.s__Bifidobacterium_adolescentis
PathID_8820	0.001191	PWY66-400: glycolysis VI (metazoan)|g__Bacteroides.s__Bacteroides_xylanisolvens
PathID_3499	0.001186	PWY0-1296: purine ribonucleosides degradation|g__Clostridium.s__Clostridium_citroniae
PathID_4724	0.001185	UNINTEGRATED|g__Bilophila.s__Bilophila_wadsworthia
PathID_5262	0.001185	PWY0-845: superpathway of pyridoxal 5'-phosphate biosynthesis and salvage|g__Bacteroides.s__Bacteroides_xylanisolvens
PathID_12099	0.001179	PWY-7208: superpathway of pyrimidine nucleobases salvage|g__Bilophila.s__Bilophila_wadsworthia
PathID_2184	0.001179	PWY-6163: chorismate biosynthesis from 3-dehydroquinate|g__Bilophila.s__Bilophila_wadsworthia
PathID_874	0.001177	PWY-6385: peptidoglycan biosynthesis III (mycobacteria)|g__Bacteroides.s__Bacteroides_xylanisolvens
PathID_7610	0.001175	PWY-6608: guanosine nucleotides degradation III|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_3998	0.001174	ARGININE-SYN4-PWY: L-ornithine de novo  biosynthesis|g__Bacteroides.s__Bacteroides_faecis
PathID_11691	0.001174	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Clostridium.s__Clostridium_clostridioforme
PathID_3779	0.001173	PWY-2942: L-lysine biosynthesis III|g__Bacteroides.s__Bacteroides_xylanisolvens
PathID_1522	0.001173	PWY-6609: adenine and adenosine salvage III|g__Holdemania.s__Holdemania_filiformis
PathID_4110	0.00117	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_10052	0.001169	PWY-5188: tetrapyrrole biosynthesis I (from glutamate)|g__Parasutterella.s__Parasutterella_excrementihominis
PathID_11354	0.001162	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Bacteroides.s__Bacteroides_salyersiae
PathID_12307	0.001161	GLYCOGENSYNTH-PWY: glycogen biosynthesis I (from ADP-D-Glucose)|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_1320	0.00116	PWY-7242: D-fructuronate degradation
PathID_7080	0.001159	NONOXIPENT-PWY: pentose phosphate pathway (non-oxidative branch)|g__Clostridium.s__Clostridium_bolteae
PathID_2842	0.001158	PWY-7282: 4-amino-2-methyl-5-phosphomethylpyrimidine biosynthesis (yeast)|g__Bacteroides.s__Bacteroides_xylanisolvens
PathID_9198	0.001151	PWY-6122: 5-aminoimidazole ribonucleotide biosynthesis II|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_8824	0.001148	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Clostridium.s__Clostridium_bolteae
PathID_3795	0.001147	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Bacteroides.s__Bacteroides_uniformis
PathID_7563	0.001146	PWY-6700: queuosine biosynthesis|g__Bacteroides.s__Bacteroides_faecis
PathID_7721	0.00114	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Dorea.s__Dorea_longicatena
PathID_11212	0.001138	PWY-6122: 5-aminoimidazole ribonucleotide biosynthesis II|g__Clostridium.s__Clostridium_clostridioforme
PathID_2928	0.001136	PWY-1042: glycolysis IV (plant cytosol)|g__Bacteroides.s__Bacteroides_faecis
PathID_4579	0.001135	PWY0-1296: purine ribonucleosides degradation|g__Clostridium.s__Clostridium_bolteae
PathID_216	0.001134	PWY-6700: queuosine biosynthesis|g__Bacteroides.s__Bacteroides_xylanisolvens
PathID_1755	0.00113	PWY-6124: inosine-5'-phosphate biosynthesis II|g__Bacteroides.s__Bacteroides_salyersiae
PathID_325	0.001126	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Bacteroides.s__Bacteroides_faecis
PathID_8697	0.001125	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Bacteroides.s__Bacteroides_faecis
PathID_9928	0.001124	PANTO-PWY: phosphopantothenate biosynthesis I|g__Bacteroides.s__Bacteroides_finegoldii
PathID_11895	0.001121	PWY-6737: starch degradation V|g__Clostridium.s__Clostridium_clostridioforme
PathID_6186	0.001119	PWY-3841: folate transformations II|g__Bacteroides.s__Bacteroides_uniformis
PathID_3771	0.001119	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Parabacteroides.s__Parabacteroides_johnsonii
PathID_9537	0.001116	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Bacteroides.s__Bacteroides_salyersiae
PathID_11589	0.001114	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Clostridium.s__Clostridium_clostridioforme
PathID_4704	0.001113	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Clostridium.s__Clostridium_clostridioforme
PathID_12288	0.001113	PWY-6737: starch degradation V|g__Eubacterium.s__Eubacterium_ventriosum
PathID_2939	0.001111	RIBOSYN2-PWY: flavin biosynthesis I (bacteria and plants)
PathID_3887	0.001108	ARGSYNBSUB-PWY: L-arginine biosynthesis II (acetyl cycle)|unclassified
PathID_2027	0.001102	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Alistipes.s__Alistipes_senegalensis
PathID_6124	0.001094	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Clostridium.s__Clostridium_bolteae
PathID_3726	0.001081	PWY-7197: pyrimidine deoxyribonucleotide phosphorylation|g__Parasutterella.s__Parasutterella_excrementihominis
PathID_4330	0.001081	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Bacteroides.s__Bacteroides_xylanisolvens
PathID_9090	0.00108	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Bacteroides.s__Bacteroides_finegoldii
PathID_8759	0.001072	RHAMCAT-PWY: L-rhamnose degradation I|g__Bacteroides.s__Bacteroides_finegoldii
PathID_10889	0.001066	PWY-2942: L-lysine biosynthesis III|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_8565	0.001065	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Bacteroides.s__Bacteroides_faecis
PathID_2998	0.001064	PWY-6151: S-adenosyl-L-methionine cycle I|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_6163	0.001063	ANAGLYCOLYSIS-PWY: glycolysis III (from glucose)|g__Bacteroides.s__Bacteroides_xylanisolvens
PathID_11586	0.001058	PWY-7197: pyrimidine deoxyribonucleotide phosphorylation
PathID_1351	0.001057	PWY-6277: superpathway of 5-aminoimidazole ribonucleotide biosynthesis|g__Clostridium.s__Clostridium_clostridioforme
PathID_12010	0.001057	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Bilophila.s__Bilophila_wadsworthia
PathID_5825	0.001056	PWY-7229: superpathway of adenosine nucleotides de novo biosynthesis I|g__Holdemania.s__Holdemania_filiformis
PathID_5819	0.001056	PWY-7111: pyruvate fermentation to isobutanol (engineered)|g__Bacteroides.s__Bacteroides_dorei
PathID_4335	0.001056	PWY-2942: L-lysine biosynthesis III|g__Bacteroides.s__Bacteroides_faecis
PathID_5186	0.001054	ARO-PWY: chorismate biosynthesis I|g__Parasutterella.s__Parasutterella_excrementihominis
PathID_12327	0.001053	PWY-7222: guanosine deoxyribonucleotides de novo biosynthesis II|g__Holdemania.s__Holdemania_filiformis
PathID_979	0.001047	VALSYN-PWY: L-valine biosynthesis|g__Roseburia.s__Roseburia_intestinalis
PathID_11074	0.001046	PWY-6609: adenine and adenosine salvage III|g__Bacteroides.s__Bacteroides_finegoldii
PathID_7679	0.001044	PWY-7111: pyruvate fermentation to isobutanol (engineered)|g__Clostridium.s__Clostridium_citroniae
PathID_4476	0.001043	PWY-6121: 5-aminoimidazole ribonucleotide biosynthesis I|g__Bilophila.s__Bilophila_wadsworthia
PathID_3410	0.001042	PWY-6122: 5-aminoimidazole ribonucleotide biosynthesis II|g__Holdemania.s__Holdemania_filiformis
PathID_8326	0.001042	PWY-7197: pyrimidine deoxyribonucleotide phosphorylation|g__Bilophila.s__Bilophila_wadsworthia
PathID_10150	0.001042	PWY-5097: L-lysine biosynthesis VI|g__Paraprevotella.s__Paraprevotella_xylaniphila
PathID_6621	0.001041	PWY-6126: superpathway of adenosine nucleotides de novo biosynthesis II
PathID_5984	0.001039	SO4ASSIM-PWY: sulfate reduction I (assimilatory)|g__Akkermansia.s__Akkermansia_muciniphila
PathID_6706	0.001038	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Bacteroides.s__Bacteroides_xylanisolvens
PathID_12132	0.001036	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_6294	0.001036	GALACT-GLUCUROCAT-PWY: superpathway of hexuronide and hexuronate degradation
PathID_1932	0.001034	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Bacteroides.s__Bacteroides_uniformis
PathID_10247	0.001033	PWY-6700: queuosine biosynthesis|g__Bacteroides.s__Bacteroides_uniformis
PathID_3906	0.001031	RHAMCAT-PWY: L-rhamnose degradation I
PathID_2616	0.001025	TRPSYN-PWY: L-tryptophan biosynthesis|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_2924	0.001023	PWY-5097: L-lysine biosynthesis VI|g__Bilophila.s__Bilophila_wadsworthia
PathID_7326	0.001022	PWY-3841: folate transformations II|g__Bacteroides.s__Bacteroides_faecis
PathID_7477	0.001019	PWY-6277: superpathway of 5-aminoimidazole ribonucleotide biosynthesis|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_4774	0.001018	PWY-6703: preQ0 biosynthesis|g__Ruminococcus.s__Ruminococcus_lactaris
PathID_208	0.001018	VALSYN-PWY: L-valine biosynthesis|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_10430	0.001012	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Bacteroides.s__Bacteroides_salyersiae
PathID_4678	0.001011	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Bacteroides.s__Bacteroides_intestinalis
PathID_173	0.001011	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Bacteroides.s__Bacteroides_faecis
PathID_1291	0.001008	PWY-2942: L-lysine biosynthesis III|g__Bacteroides.s__Bacteroides_uniformis
PathID_1678	0.001007	HISTSYN-PWY: L-histidine biosynthesis|g__Bacteroides.s__Bacteroides_faecis
PathID_9458	0.001006	PWY0-845: superpathway of pyridoxal 5'-phosphate biosynthesis and salvage|g__Bacteroides.s__Bacteroides_uniformis
PathID_5500	0.001005	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Bacteroides.s__Bacteroides_finegoldii
PathID_3913	0.001004	PWY-7242: D-fructuronate degradation|g__Eubacterium.s__Eubacterium_rectale
PathID_7475	0.001003	UNINTEGRATED|g__Bacteroides.s__Bacteroides_faecis
PathID_8974	0.001	PWY-7111: pyruvate fermentation to isobutanol (engineered)|g__Bacteroides.s__Bacteroides_salyersiae
PathID_897	0.000996	PANTO-PWY: phosphopantothenate biosynthesis I|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_7314	0.000994	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Parabacteroides.s__Parabacteroides_johnsonii
PathID_3252	0.000991	PWY-6385: peptidoglycan biosynthesis III (mycobacteria)|g__Bacteroides.s__Bacteroides_finegoldii
PathID_2884	0.000991	PWY0-166: superpathway of pyrimidine deoxyribonucleotides de novo biosynthesis (E. coli)|g__Parasutterella.s__Parasutterella_excrementihominis
PathID_1564	0.000989	PWY-6609: adenine and adenosine salvage III|g__Bacteroides.s__Bacteroides_faecis
PathID_3648	0.000987	PWY-5097: L-lysine biosynthesis VI|g__Bacteroides.s__Bacteroides_xylanisolvens
PathID_4529	0.000986	UNINTEGRATED|g__Bacteroides.s__Bacteroides_uniformis
PathID_3873	0.000979	DTDPRHAMSYN-PWY: dTDP-L-rhamnose biosynthesis I|g__Bacteroides.s__Bacteroides_finegoldii
PathID_1	0.000976	PWY-6151: S-adenosyl-L-methionine cycle I|g__Dorea.s__Dorea_longicatena
PathID_4666	0.000976	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Bacteroides.s__Bacteroides_finegoldii
PathID_7439	0.000975	PWY-5989: stearate biosynthesis II (bacteria and plants)
PathID_3318	0.000973	PWY-7560: methylerythritol phosphate pathway II
PathID_3612	0.000972	PWY-1042: glycolysis IV (plant cytosol)|g__Alistipes.s__Alistipes_senegalensis
PathID_6434	0.000967	PWY-6125: superpathway of guanosine nucleotides de novo biosynthesis II
PathID_5308	0.000966	PWY-5304: superpathway of sulfur oxidation (Acidianus ambivalens)|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_9317	0.000965	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Bacteroides.s__Bacteroides_finegoldii
PathID_9457	0.000963	PWY-6305: putrescine biosynthesis IV|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_10931	0.000962	PWY-5097: L-lysine biosynthesis VI|g__Parabacteroides.s__Parabacteroides_johnsonii
PathID_1033	0.000958	NONOXIPENT-PWY: pentose phosphate pathway (non-oxidative branch)|g__Bacteroides.s__Bacteroides_xylanisolvens
PathID_10579	0.000953	SER-GLYSYN-PWY: superpathway of L-serine and glycine biosynthesis I|g__Roseburia.s__Roseburia_intestinalis
PathID_8564	0.000953	PWY-5104: L-isoleucine biosynthesis IV|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_1216	0.00095	PWY-6277: superpathway of 5-aminoimidazole ribonucleotide biosynthesis|g__Holdemania.s__Holdemania_filiformis
PathID_4443	0.000947	PWY-6595: superpathway of guanosine nucleotides degradation (plants)|unclassified
PathID_7429	0.000946	PWY-6163: chorismate biosynthesis from 3-dehydroquinate|g__Parasutterella.s__Parasutterella_excrementihominis
PathID_2991	0.000946	COMPLETE-ARO-PWY: superpathway of aromatic amino acid biosynthesis|g__Clostridium.s__Clostridium_clostridioforme
PathID_1331	0.000946	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Clostridium.s__Clostridium_clostridioforme
PathID_2070	0.000946	PWY-6151: S-adenosyl-L-methionine cycle I|g__Clostridium.s__Clostridium_clostridioforme
PathID_6536	0.000946	PWY-6527: stachyose degradation|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_3532	0.000943	UNINTEGRATED|g__Clostridium.s__Clostridium_bolteae
PathID_9947	0.000942	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Bacteroides.s__Bacteroides_uniformis
PathID_7472	0.000941	PYRIDOXSYN-PWY: pyridoxal 5'-phosphate biosynthesis I|g__Bacteroides.s__Bacteroides_xylanisolvens
PathID_8305	0.000941	PWY0-845: superpathway of pyridoxal 5'-phosphate biosynthesis and salvage|g__Bacteroides.s__Bacteroides_faecis
PathID_4180	0.000941	ILEUSYN-PWY: L-isoleucine biosynthesis I (from threonine)|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_5297	0.000937	PWY-6123: inosine-5'-phosphate biosynthesis I|g__Bacteroides.s__Bacteroides_faecis
PathID_7343	0.000935	1CMET2-PWY: N10-formyl-tetrahydrofolate biosynthesis|g__Bacteroides.s__Bacteroides_uniformis
PathID_12502	0.000934	PWY-5097: L-lysine biosynthesis VI|g__Bacteroides.s__Bacteroides_uniformis
PathID_2332	0.000932	PWY-6588: pyruvate fermentation to acetone|g__Anaerostipes.s__Anaerostipes_hadrus
PathID_8455	0.000932	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Bacteroides.s__Bacteroides_salyersiae
PathID_878	0.00093	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Bacteroides.s__Bacteroides_finegoldii
PathID_2911	0.000929	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_8233	0.000929	PWY-1269: CMP-3-deoxy-D-manno-octulosonate biosynthesis I|g__Bacteroides.s__Bacteroides_faecis
PathID_6474	0.000928	ASPASN-PWY: superpathway of L-aspartate and L-asparagine biosynthesis|g__Bacteroides.s__Bacteroides_finegoldii
PathID_10830	0.000927	PWY-6126: superpathway of adenosine nucleotides de novo biosynthesis II|g__Holdemania.s__Holdemania_filiformis
PathID_285	0.000924	PWY-7111: pyruvate fermentation to isobutanol (engineered)|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_2095	0.000922	PWY-5103: L-isoleucine biosynthesis III|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_9173	0.000921	PWY-5097: L-lysine biosynthesis VI|g__Bacteroides.s__Bacteroides_faecis
PathID_4610	0.00092	PWY-5097: L-lysine biosynthesis VI|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_1808	0.00092	ILEUSYN-PWY: L-isoleucine biosynthesis I (from threonine)|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_9721	0.00092	PWY-5686: UMP biosynthesis|g__Bacteroides.s__Bacteroides_faecis
PathID_368	0.00092	PWY-6168: flavin biosynthesis III (fungi)|g__Bacteroides.s__Bacteroides_faecis
PathID_7070	0.000919	NONMEVIPP-PWY: methylerythritol phosphate pathway I|g__Bacteroides.s__Bacteroides_xylanisolvens
PathID_2216	0.000917	PWY-6703: preQ0 biosynthesis|g__Bacteroides.s__Bacteroides_uniformis
PathID_1489	0.000917	PWY-6527: stachyose degradation|g__Roseburia.s__Roseburia_intestinalis
PathID_12275	0.000913	PWY-6123: inosine-5'-phosphate biosynthesis I|g__Coprococcus.s__Coprococcus_sp_ART55_1
PathID_10868	0.000911	PWY-7220: adenosine deoxyribonucleotides de novo biosynthesis II|g__Holdemania.s__Holdemania_filiformis
PathID_8840	0.00091	NONOXIPENT-PWY: pentose phosphate pathway (non-oxidative branch)|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_1029	0.00091	PWY-7228: superpathway of guanosine nucleotides de novo biosynthesis I|g__Parasutterella.s__Parasutterella_excrementihominis
PathID_123	0.000909	PWY-7228: superpathway of guanosine nucleotides de novo biosynthesis I|g__Bilophila.s__Bilophila_wadsworthia
PathID_5405	0.000905	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Clostridium.s__Clostridium_clostridioforme
PathID_3247	0.000905	PWY-6700: queuosine biosynthesis|g__Bacteroides.s__Bacteroides_salyersiae
PathID_63	9e-04	PWY0-166: superpathway of pyrimidine deoxyribonucleotides de novo biosynthesis (E. coli)
PathID_3478	0.000899	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Dorea.s__Dorea_longicatena

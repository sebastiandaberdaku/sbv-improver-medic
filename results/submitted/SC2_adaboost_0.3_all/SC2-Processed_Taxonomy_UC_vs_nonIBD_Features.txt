TaxonomyID	Importance_Optional	Description
TaxID_441209	0.195714	Rhodobaca barguzinensis
TaxID_584	0.204714	Proteus mirabilis
TaxID_33025	0.244429	Phascolarctobacterium faecium
TaxID_689	0.304429	Vibrio mediterranei
TaxID_76860	0.232714	Streptococcus constellatus
TaxID_631220	0.256571	Desulfovibrio sp. G11
TaxID_1145276	0.286	Lysinibacillus varians
TaxID_122420	0.215429	Thermococcus sp. P6
TaxID_490	0.187429	Neisseria sicca
TaxID_452	0.202714	Legionella spiritensis
TaxID_2502843	0.167714	Erythrobacter sp. HKB08
TaxID_1160	0.321143	Planktothrix agardhii
TaxID_1747	0.401286	Cutibacterium acnes
TaxID_1262537	0.163857	Lactococcus phage P680
TaxID_1597	0.166857	Lactobacillus paracasei
TaxID_1725232	0.216429	Candidatus Desulfovibrio trichonymphae
TaxID_881	0.321857	Desulfovibrio vulgaris
TaxID_41707	0.213714	Lawsonia
TaxID_583	0.222571	Proteus
TaxID_54304	0.283857	Planktothrix
TaxID_10270	0.244714	Leporipoxvirus
TaxID_33024	0.245429	Phascolarctobacterium
TaxID_1912216	0.347571	Cutibacterium
TaxID_909930	0.217857	Acidaminococcaceae
TaxID_216572	0.190571	Oscillospiraceae
TaxID_1843488	0.219857	Acidaminococcales
TaxID_909932	0.230714	Negativicutes

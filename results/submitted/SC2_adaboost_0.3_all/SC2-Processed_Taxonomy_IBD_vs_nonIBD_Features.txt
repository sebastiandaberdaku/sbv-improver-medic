TaxonomyID	Importance_Optional	Description
TaxID_271098	0.195571	Shewanella halifaxensis
TaxID_292800	0.221714	Flavonifractor plautii
TaxID_161675	0.254143	Tamana bat virus
TaxID_1273687	0.251286	Mycobacterium sp. VKM Ac-1817D
TaxID_2218628	0.223143	Photorhabdus laumondii
TaxID_204039	0.245714	Dickeya dianthicola
TaxID_2081703	0.234286	Peptostreptococcaceae bacterium oral taxon 929
TaxID_1816219	0.349714	Colwellia sp. PAMC 21821
TaxID_1778262	0.221571	Candidatus Doolittlea endobia
TaxID_208479	0.284714	[Clostridium] bolteae
TaxID_76517	0.231429	Campylobacter hominis
TaxID_1853276	0.203	Neisseria sp. 10022
TaxID_885867	0.212143	Prochlorococcus phage P-SSP10
TaxID_39491	0.312143	[Eubacterium] rectale
TaxID_1630141	0.206429	Candidatus Nitrosoglobus terrae
TaxID_2486578	0.229571	Rickettsiales endosymbiont of Stachyamoeba lipophora
TaxID_1834196	0.327857	Lachnoclostridium sp. YL32
TaxID_1509	0.235	Clostridium sporogenes
TaxID_143393	0.226	[Eubacterium] sulci
TaxID_43659	0.254714	Pseudoalteromonas tetraodonis
TaxID_158841	0.258	Leminorella richardii
TaxID_44822	0.197	Microcystis viridis
TaxID_1747	0.176143	Cutibacterium acnes
TaxID_649756	0.338571	Anaerostipes hadrus
TaxID_39485	0.213571	[Eubacterium] eligens
TaxID_367190	0.218429	Yersinia similis
TaxID_1850252	0.199857	Tenacibaculum todarodis
TaxID_1160721	0.230857	Ruminococcus bicirculans
TaxID_216851	0.243429	Faecalibacterium
TaxID_1934947	0.220429	Immundisolibacter
TaxID_1906657	0.227143	Candidatus Doolittlea
TaxID_1506553	0.304286	Lachnoclostridium
TaxID_1730	0.221571	Eubacterium
TaxID_11051	0.244857	Flavivirus
TaxID_456826	0.310286	Candidatus Cloacimonas
TaxID_946234	0.183571	Flavonifractor
TaxID_207244	0.285571	Anaerostipes
TaxID_253238	0.295143	Ethanoligenens
TaxID_1263	0.252857	Ruminococcus
TaxID_1934946	0.208857	Immundisolibacteraceae
TaxID_186806	0.219143	Eubacteriaceae
TaxID_338190	0.189429	Nitrosopumilaceae
TaxID_11050	0.249143	Flaviviridae
TaxID_91347	0.186286	Enterobacterales
TaxID_31932	0.191571	Nitrosopumilales
TaxID_1236	0.208	Gammaproteobacteria
TaxID_203490	0.172714	Fusobacteriia
TaxID_84992	0.236429	Acidimicrobiia
TaxID_1224	0.185143	Proteobacteria
TaxID_32066	0.175429	Fusobacteria

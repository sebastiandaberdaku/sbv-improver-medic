TaxonomyID	Importance_Optional	Description
TaxID_1678129	0.110286	Limnohabitans sp. 103DPR2
TaxID_1767	0.137	Mycobacterium intracellulare
TaxID_1862959	0.157286	Lactococcus phage D4412
TaxID_53408	0.204	Pseudomonas citronellolis
TaxID_271098	0.214286	Shewanella halifaxensis
TaxID_292800	0.232143	Flavonifractor plautii
TaxID_1580596	0.181857	Phaeobacter piscinae
TaxID_1891926	0.153286	Fuerstia marisgermanicae
TaxID_1262536	0.159286	Lactococcus phage jm3
TaxID_1809410	0.151	Herminiimonas arsenitoxidans
TaxID_161675	0.242143	Tamana bat virus
TaxID_1852374	0.094	Ezakiella massiliensis
TaxID_334542	0.101286	Rhodococcus qingshengii
TaxID_861	0.139143	Fusobacterium ulcerans
TaxID_1028989	0.133143	Pseudomonas sp. StFLB209
TaxID_45972	0.136286	Staphylococcus pasteuri
TaxID_28901	0.127571	Salmonella enterica
TaxID_417	0.145429	Methylomonas clara
TaxID_1166130	0.102	Enterobacter sp. R4-368
TaxID_588898	0.143857	Haloterrigena daqingensis
TaxID_1055487	0.245286	Methylotenera versatilis
TaxID_83816	0.155571	Methanobrevibacter ruminantium
TaxID_60893	0.111714	Desulfobacca acetoxidans
TaxID_414970	0.131429	Enterobacteria phage cdtI
TaxID_665884	0.151286	Lactococcus virus CB14
TaxID_1755811	0.121429	Paraphotobacterium marinum
TaxID_1500757	0.151286	Pseudomonas phage phiPSA1
TaxID_1273687	0.207143	Mycobacterium sp. VKM Ac-1817D
TaxID_665883	0.149429	Lactococcus virus CB13
TaxID_2218628	0.218143	Photorhabdus laumondii
TaxID_204039	0.282	Dickeya dianthicola
TaxID_720	0.121143	Actinobacillus lignieresii
TaxID_1852373	0.128571	Murdochiella vaginalis
TaxID_2081703	0.234286	Peptostreptococcaceae bacterium oral taxon 929
TaxID_1932881	0.133429	Pacmanvirus A23
TaxID_1873959	0.128286	Pectobacterium phage PP90
TaxID_1816219	0.308286	Colwellia sp. PAMC 21821
TaxID_101564	0.199286	Pseudomonas alcaliphila
TaxID_853	0.371	Faecalibacterium prausnitzii
TaxID_1862961	0.159143	Lactococcus phage M6162
TaxID_32051	0.175143	Synechococcus sp. WH 7803
TaxID_1264	0.154571	Ruminococcus albus
TaxID_1145276	0.104	Lysinibacillus varians
TaxID_36343	0.159	Lactococcus virus bIL67
TaxID_1778262	0.195	Candidatus Doolittlea endobia
TaxID_63118	0.137714	Lactococcus virus bIL170
TaxID_1816218	0.164714	Colwellia sp. PAMC 20917
TaxID_398	0.152286	Rhizobium tropici
TaxID_208479	0.332714	[Clostridium] bolteae
TaxID_76517	0.213714	Campylobacter hominis
TaxID_1862962	0.164571	Lactococcus phage M6165
TaxID_1898684	0.125286	Pseudomonas sp. LPH1
TaxID_1233873	0.165143	Geobacillus sp. GHH01
TaxID_1853276	0.178143	Neisseria sp. 10022
TaxID_885867	0.184143	Prochlorococcus phage P-SSP10
TaxID_1394	0.130714	[Bacillus] caldolyticus
TaxID_39491	0.352286	[Eubacterium] rectale
TaxID_1262535	0.151714	Lactococcus phage jm2
TaxID_357276	0.149143	Bacteroides dorei
TaxID_220622	0.126857	Sulfurospirillum sp. JPD-1
TaxID_698828	0.217143	Kushneria konosiri
TaxID_1761016	0.116143	Paraburkholderia caffeinilytica
TaxID_1262533	0.154	Lactococcus phage 340
TaxID_164516	0.145	Blattabacterium clevelandi
TaxID_213775	0.135286	Lactococcus virus jj50
TaxID_2161821	0.117429	Dialister sp. Marseille-P5638
TaxID_1981160	0.102	Escherichia virus 24B
TaxID_1630141	0.156429	Candidatus Nitrosoglobus terrae
TaxID_238854	0.109286	Synechococcus phage S-PM2
TaxID_2486578	0.183	Rickettsiales endosymbiont of Stachyamoeba lipophora
TaxID_494269	0.153143	Lactococcus virus Bibb29
TaxID_1834196	0.341857	Lachnoclostridium sp. YL32
TaxID_1509	0.178143	Clostridium sporogenes
TaxID_827	0.106143	Campylobacter ureolyticus
TaxID_143393	0.199	[Eubacterium] sulci
TaxID_43659	0.219143	Pseudoalteromonas tetraodonis
TaxID_47884	0.135143	Pseudomonas taetrolens
TaxID_158841	0.261571	Leminorella richardii
TaxID_562	0.149714	Escherichia coli
TaxID_31532	0.160143	Lactococcus virus sk1
TaxID_1985691	0.137857	Rochambeau curiovirus
TaxID_287412	0.153714	Lactococcus virus Sl4
TaxID_44822	0.195143	Microcystis viridis
TaxID_1747	0.111143	Cutibacterium acnes
TaxID_797277	0.138143	Pseudomonas litoralis
TaxID_1262537	0.154143	Lactococcus phage P680
TaxID_1490057	0.109571	Anoxybacillus sp. B7M1
TaxID_649756	0.392286	Anaerostipes hadrus
TaxID_585423	0.211857	Synechococcus sp. KORDI-49
TaxID_39485	0.245429	[Eubacterium] eligens
TaxID_31537	0.134286	Lactococcus virus c2
TaxID_1680	0.218571	Bifidobacterium adolescentis
TaxID_1157947	0.137429	Thermotoga sp. Cell2
TaxID_1878942	0.107286	Candidatus Fukatsuia symbiotica
TaxID_658666	0.132857	Bacillus bombysepticus
TaxID_213769	0.134571	Lactococcus virus 712
TaxID_83129	0.130571	Lactococcus virus P008
TaxID_28251	0.107571	Ornithobacterium rhinotracheale
TaxID_33033	0.133429	Parvimonas micra
TaxID_1528099	0.161429	Lawsonella clevelandensis
TaxID_1165134	0.151571	Lactococcus virus ASCC191
TaxID_367190	0.142857	Yersinia similis
TaxID_1262538	0.156	Lactococcus phage phi7
TaxID_1850252	0.255143	Tenacibaculum todarodis
TaxID_1160721	0.293714	Ruminococcus bicirculans
TaxID_2336	0.105143	Thermotoga maritima
TaxID_216851	0.372286	Faecalibacterium
TaxID_2268	0.229143	Thermofilum
TaxID_561	0.121714	Escherichia
TaxID_1623305	0.118286	Sk1virus
TaxID_1678	0.108857	Bifidobacterium
TaxID_1985687	0.132571	Curiovirus
TaxID_70774	0.163286	Hydrogenophilus
TaxID_1934947	0.172	Immundisolibacter
TaxID_1677050	0.104571	Novibacillus
TaxID_590	0.122143	Salmonella
TaxID_1161127	0.136143	Murdochiella
TaxID_316625	0.134143	Saccharophagus
TaxID_1582879	0.158	Ezakiella
TaxID_186532	0.144857	C2virus
TaxID_1906657	0.199429	Candidatus Doolittlea
TaxID_1506553	0.327714	Lachnoclostridium
TaxID_180541	0.14	Salinisphaera
TaxID_1730	0.246143	Eubacterium
TaxID_698776	0.091286	Cellulosilyticum
TaxID_28250	0.105286	Ornithobacterium
TaxID_11051	0.246	Flavivirus
TaxID_848	0.196714	Fusobacterium
TaxID_1847725	0.188714	Lawsonella
TaxID_456826	0.283857	Candidatus Cloacimonas
TaxID_1981157	0.104	Nona33virus
TaxID_1747777	0.124143	Cuniculiplasma
TaxID_946234	0.195143	Flavonifractor
TaxID_2042066	0.147571	Paraphotobacterium
TaxID_325455	0.126857	Gammapapillomavirus
TaxID_207244	0.348286	Anaerostipes
TaxID_253238	0.284571	Ethanoligenens
TaxID_1805626	0.109	Pithovirus
TaxID_1980513	0.131571	Candidatus Nitrosoglobus
TaxID_1263	0.332	Ruminococcus
TaxID_543311	0.147714	Parvimonas
TaxID_206349	0.163286	Hydrogenophilaceae
TaxID_1334117	0.161286	Melioribacteraceae
TaxID_1934946	0.160571	Immundisolibacteraceae
TaxID_1240483	0.157429	Orbaceae
TaxID_541000	0.169857	Ruminococcaceae
TaxID_1570339	0.164571	Peptoniphilaceae
TaxID_186806	0.246143	Eubacteriaceae
TaxID_203492	0.201571	Fusobacteriaceae
TaxID_338190	0.150714	Nitrosopumilaceae
TaxID_114378	0.182714	Thermofilaceae
TaxID_742031	0.140286	Salinisphaeraceae
TaxID_543	0.223714	Enterobacteriaceae
TaxID_1747776	0.137714	Cuniculiplasmataceae
TaxID_11050	0.230286	Flaviviridae
TaxID_91347	0.229714	Enterobacterales
TaxID_203491	0.200286	Fusobacteriales
TaxID_31932	0.141429	Nitrosopumilales
TaxID_1737405	0.121	Tissierellales
TaxID_85004	0.111143	Bifidobacteriales
TaxID_1236	0.256714	Gammaproteobacteria
TaxID_203490	0.210571	Fusobacteriia
TaxID_84992	0.159857	Acidimicrobiia
TaxID_1224	0.264714	Proteobacteria
TaxID_32066	0.222286	Fusobacteria

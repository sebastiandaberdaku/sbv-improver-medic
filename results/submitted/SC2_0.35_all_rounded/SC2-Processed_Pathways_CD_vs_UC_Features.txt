PathwayID	Importance_Optional	Description
PathID_1364	0.265714	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Bacteroides.s__Bacteroides_uniformis
PathID_1390	0.276857	PWY-5686: UMP biosynthesis|g__Bacteroides.s__Bacteroides_uniformis
PathID_1815	0.248	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Bacteroides.s__Bacteroides_uniformis
PathID_3577	0.232714	PWY-6151: S-adenosyl-L-methionine cycle I|g__Bacteroides.s__Bacteroides_uniformis
PathID_4700	0.245857	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Bacteroides.s__Bacteroides_uniformis
PathID_6864	0.260286	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Bacteroides.s__Bacteroides_uniformis
PathID_7290	0.251857	PWY-3841: folate transformations II|g__Bacteroides.s__Bacteroides_uniformis
PathID_8918	0.291714	PWY-6700: queuosine biosynthesis|g__Bacteroides.s__Bacteroides_uniformis
PathID_9383	0.239714	PWY-7242: D-fructuronate degradation|g__Eubacterium.s__Eubacterium_rectale
PathID_11280	0.260714	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Bacteroides.s__Bacteroides_uniformis
PathID_11461	0.284286	1CMET2-PWY: N10-formyl-tetrahydrofolate biosynthesis|g__Bacteroides.s__Bacteroides_uniformis
PathID_12177	0.291143	PWY-6703: preQ0 biosynthesis|g__Bacteroides.s__Bacteroides_uniformis

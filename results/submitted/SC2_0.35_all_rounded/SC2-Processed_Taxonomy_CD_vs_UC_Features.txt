TaxonomyID	Importance_Optional	Description
TaxID_1505596	0.273714	Blochmannia endosymbiont of Polyrhachis (Hedomyrma) turneri
TaxID_1160	0.273857	Planktothrix agardhii
TaxID_1747	0.290429	Cutibacterium acnes
TaxID_200991	0.261143	Planococcus rifietoensis
TaxID_1879023	0.220714	Mycobacterium sp. djl-10
TaxID_54304	0.262857	Planktothrix
TaxID_1912216	0.250143	Cutibacterium
TaxID_151340	0.258714	Papillomaviridae

TaxonomyID	Importance_Optional	Description
TaxID_1862959	0.157286	Lactococcus phage D4412
TaxID_53408	0.204	Pseudomonas citronellolis
TaxID_271098	0.214286	Shewanella halifaxensis
TaxID_292800	0.232143	Flavonifractor plautii
TaxID_1580596	0.181857	Phaeobacter piscinae
TaxID_161675	0.242143	Tamana bat virus
TaxID_861	0.139143	Fusobacterium ulcerans
TaxID_28901	0.127571	Salmonella enterica
TaxID_1055487	0.245286	Methylotenera versatilis
TaxID_1273687	0.207143	Mycobacterium sp. VKM Ac-1817D
TaxID_2218628	0.218143	Photorhabdus laumondii
TaxID_204039	0.282	Dickeya dianthicola
TaxID_2081703	0.234286	Peptostreptococcaceae bacterium oral taxon 929
TaxID_1816219	0.308286	Colwellia sp. PAMC 21821
TaxID_101564	0.199286	Pseudomonas alcaliphila
TaxID_853	0.371	Faecalibacterium prausnitzii
TaxID_1264	0.154571	Ruminococcus albus
TaxID_1778262	0.195	Candidatus Doolittlea endobia
TaxID_208479	0.332714	[Clostridium] bolteae
TaxID_76517	0.213714	Campylobacter hominis
TaxID_1898684	0.125286	Pseudomonas sp. LPH1
TaxID_1233873	0.165143	Geobacillus sp. GHH01
TaxID_1853276	0.178143	Neisseria sp. 10022
TaxID_885867	0.184143	Prochlorococcus phage P-SSP10
TaxID_1394	0.130714	[Bacillus] caldolyticus
TaxID_39491	0.352286	[Eubacterium] rectale
TaxID_1262535	0.151714	Lactococcus phage jm2
TaxID_357276	0.149143	Bacteroides dorei
TaxID_698828	0.217143	Kushneria konosiri
TaxID_1262533	0.154	Lactococcus phage 340
TaxID_2486578	0.183	Rickettsiales endosymbiont of Stachyamoeba lipophora
TaxID_494269	0.153143	Lactococcus virus Bibb29
TaxID_1834196	0.341857	Lachnoclostridium sp. YL32
TaxID_1509	0.178143	Clostridium sporogenes
TaxID_143393	0.199	[Eubacterium] sulci
TaxID_43659	0.219143	Pseudoalteromonas tetraodonis
TaxID_158841	0.261571	Leminorella richardii
TaxID_562	0.149714	Escherichia coli
TaxID_31532	0.160143	Lactococcus virus sk1
TaxID_44822	0.195143	Microcystis viridis
TaxID_797277	0.138143	Pseudomonas litoralis
TaxID_649756	0.392286	Anaerostipes hadrus
TaxID_585423	0.211857	Synechococcus sp. KORDI-49
TaxID_39485	0.245429	[Eubacterium] eligens
TaxID_1680	0.218571	Bifidobacterium adolescentis
TaxID_33033	0.133429	Parvimonas micra
TaxID_1528099	0.161429	Lawsonella clevelandensis
TaxID_1165134	0.151571	Lactococcus virus ASCC191
TaxID_367190	0.142857	Yersinia similis
TaxID_1850252	0.255143	Tenacibaculum todarodis
TaxID_1160721	0.293714	Ruminococcus bicirculans
TaxID_216851	0.372286	Faecalibacterium
TaxID_2268	0.229143	Thermofilum
TaxID_561	0.121714	Escherichia
TaxID_70774	0.163286	Hydrogenophilus
TaxID_1934947	0.172	Immundisolibacter
TaxID_590	0.122143	Salmonella
TaxID_1582879	0.158	Ezakiella
TaxID_1906657	0.199429	Candidatus Doolittlea
TaxID_1506553	0.327714	Lachnoclostridium
TaxID_1730	0.246143	Eubacterium
TaxID_11051	0.246	Flavivirus
TaxID_848	0.196714	Fusobacterium
TaxID_1847725	0.188714	Lawsonella
TaxID_456826	0.283857	Candidatus Cloacimonas
TaxID_946234	0.195143	Flavonifractor
TaxID_207244	0.348286	Anaerostipes
TaxID_253238	0.284571	Ethanoligenens
TaxID_1263	0.332	Ruminococcus
TaxID_1334117	0.161286	Melioribacteraceae
TaxID_1934946	0.160571	Immundisolibacteraceae
TaxID_1570339	0.164571	Peptoniphilaceae
TaxID_186806	0.246143	Eubacteriaceae
TaxID_203492	0.201571	Fusobacteriaceae
TaxID_338190	0.150714	Nitrosopumilaceae
TaxID_114378	0.182714	Thermofilaceae
TaxID_543	0.223714	Enterobacteriaceae
TaxID_11050	0.230286	Flaviviridae
TaxID_91347	0.229714	Enterobacterales
TaxID_203491	0.200286	Fusobacteriales
TaxID_31932	0.141429	Nitrosopumilales
TaxID_1236	0.256714	Gammaproteobacteria
TaxID_203490	0.210571	Fusobacteriia
TaxID_1224	0.264714	Proteobacteria
TaxID_32066	0.222286	Fusobacteria

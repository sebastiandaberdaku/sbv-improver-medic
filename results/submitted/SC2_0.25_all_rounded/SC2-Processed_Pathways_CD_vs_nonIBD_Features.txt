PathwayID	Importance_Optional	Description
PathID_20	0.230571	PWY-7196: superpathway of pyrimidine ribonucleosides salvage|unclassified
PathID_171	0.338571	PWY0-1297: superpathway of purine deoxyribonucleosides degradation
PathID_193	0.161143	PWY-6700: queuosine biosynthesis|g__Bacteroides.s__Bacteroides_sp_4_3_47FAA
PathID_216	0.178286	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Clostridium.s__Clostridium_bolteae
PathID_417	0.124571	PWY-7228: superpathway of guanosine nucleotides de novo biosynthesis I|g__Bacteroides.s__Bacteroides_sp_4_3_47FAA
PathID_484	0.178714	PWY-6737: starch degradation V|unclassified
PathID_525	0.155429	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_544	0.202143	PWY-724: superpathway of L-lysine, L-threonine and L-methionine biosynthesis II
PathID_563	0.192	URDEGR-PWY: superpathway of allantoin degradation in plants
PathID_799	0.281571	UNINTEGRATED|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_870	0.153429	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Eubacterium.s__Eubacterium_ventriosum
PathID_993	0.191	PWY-6277: superpathway of 5-aminoimidazole ribonucleotide biosynthesis|g__Eubacterium.s__Eubacterium_ventriosum
PathID_1073	0.225714	PANTO-PWY: phosphopantothenate biosynthesis I|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_1077	0.137143	PWY-7111: pyruvate fermentation to isobutanol (engineered)|g__Parabacteroides.s__Parabacteroides_merdae
PathID_1096	0.154	PWY-7220: adenosine deoxyribonucleotides de novo biosynthesis II|unclassified
PathID_1131	0.159286	PWY-6385: peptidoglycan biosynthesis III (mycobacteria)|g__Eubacterium.s__Eubacterium_ventriosum
PathID_1185	0.153714	METSYN-PWY: L-homoserine and L-methionine biosynthesis|unclassified
PathID_1221	0.243714	PWY-7111: pyruvate fermentation to isobutanol (engineered)|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_1229	0.160571	CENTFERM-PWY: pyruvate fermentation to butanoate
PathID_1369	0.165	HISTSYN-PWY: L-histidine biosynthesis|unclassified
PathID_1390	0.181429	PWY-5686: UMP biosynthesis|g__Bacteroides.s__Bacteroides_uniformis
PathID_1392	0.256	PWY66-409: superpathway of purine nucleotide salvage
PathID_1522	0.197571	PWY0-1296: purine ribonucleosides degradation|g__Clostridium.s__Clostridium_clostridioforme
PathID_1535	0.142714	NONMEVIPP-PWY: methylerythritol phosphate pathway I|g__Bacteroides.s__Bacteroides_sp_4_3_47FAA
PathID_1571	0.164	PWY-6122: 5-aminoimidazole ribonucleotide biosynthesis II|unclassified
PathID_1626	0.251143	HSERMETANA-PWY: L-methionine biosynthesis III
PathID_1897	0.161571	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Parabacteroides.s__Parabacteroides_merdae
PathID_1935	0.234286	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Parabacteroides.s__Parabacteroides_merdae
PathID_2095	0.155714	PWY-5695: urate biosynthesis/inosine 5'-phosphate degradation|g__Bacteroides.s__Bacteroides_salyersiae
PathID_2136	0.319857	PWY-5304: superpathway of sulfur oxidation (Acidianus ambivalens)
PathID_2155	0.150857	PWY-6151: S-adenosyl-L-methionine cycle I|g__Butyrivibrio.s__Butyrivibrio_crossotus
PathID_2268	0.181857	FASYN-ELONG-PWY: fatty acid elongation -- saturated|unclassified
PathID_2274	0.145429	PWY-6277: superpathway of 5-aminoimidazole ribonucleotide biosynthesis|g__Butyrivibrio.s__Butyrivibrio_crossotus
PathID_2359	0.231286	PWY-6737: starch degradation V|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_2445	0.166714	PWY-6317: galactose degradation I (Leloir pathway)|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_2503	0.144143	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Ruminococcus.s__Ruminococcus_callidus
PathID_2620	0.257714	PWY-6737: starch degradation V
PathID_2639	0.156571	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Oxalobacter.s__Oxalobacter_formigenes
PathID_2785	0.142286	PWY-6317: galactose degradation I (Leloir pathway)|g__Ruminococcus.s__Ruminococcus_bromii
PathID_2806	0.200857	PWY-6163: chorismate biosynthesis from 3-dehydroquinate|g__Clostridium.s__Clostridium_clostridioforme
PathID_2823	0.195429	PWY-6122: 5-aminoimidazole ribonucleotide biosynthesis II|g__Eubacterium.s__Eubacterium_ventriosum
PathID_2846	0.230857	PWY-5659: GDP-mannose biosynthesis|unclassified
PathID_2884	0.141571	PWY-7663: gondoate biosynthesis (anaerobic)|g__Bacteroides.s__Bacteroides_salyersiae
PathID_3099	0.173571	PWY0-1296: purine ribonucleosides degradation|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_3140	0.213714	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_3168	0.145429	PWY-6737: starch degradation V|g__Pseudoflavonifractor.s__Pseudoflavonifractor_capillosus
PathID_3197	0.29	UNINTEGRATED|g__Eubacterium.s__Eubacterium_ventriosum
PathID_3286	0.204286	PWY-6121: 5-aminoimidazole ribonucleotide biosynthesis I|g__Clostridium.s__Clostridium_clostridioforme
PathID_3351	0.215286	PWY-5989: stearate biosynthesis II (bacteria and plants)|unclassified
PathID_3373	0.222143	PWY-5097: L-lysine biosynthesis VI|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_3567	0.259143	PANTO-PWY: phosphopantothenate biosynthesis I
PathID_3575	0.170714	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Ruminococcus.s__Ruminococcus_callidus
PathID_3686	0.242286	PWY-5121: superpathway of geranylgeranyl diphosphate biosynthesis II (via MEP)|unclassified
PathID_3719	0.155714	GLUCUROCAT-PWY: superpathway of &beta;-D-glucuronide and D-glucuronate degradation|unclassified
PathID_3731	0.215857	UNINTEGRATED|g__Blautia.s__Ruminococcus_obeum
PathID_3758	0.179143	PANTO-PWY: phosphopantothenate biosynthesis I|g__Eubacterium.s__Eubacterium_ramulus
PathID_3768	0.167714	PWY-6277: superpathway of 5-aminoimidazole ribonucleotide biosynthesis|unclassified
PathID_3902	0.266857	PWY-6936: seleno-amino acid biosynthesis|unclassified
PathID_3912	0.248286	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Parabacteroides.s__Parabacteroides_merdae
PathID_3983	0.177429	PWY0-862: (5Z)-dodec-5-enoate biosynthesis|unclassified
PathID_4123	0.212143	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Eubacterium.s__Eubacterium_ramulus
PathID_4139	0.137429	PWY-6163: chorismate biosynthesis from 3-dehydroquinate|g__Eubacterium.s__Eubacterium_ventriosum
PathID_4210	0.161571	PYRIDOXSYN-PWY: pyridoxal 5'-phosphate biosynthesis I|g__Bacteroides.s__Bacteroides_uniformis
PathID_4305	0.168143	PWY-6277: superpathway of 5-aminoimidazole ribonucleotide biosynthesis|g__Clostridium.s__Clostridium_bolteae
PathID_4427	0.182714	PWY-621: sucrose degradation III (sucrose invertase)|unclassified
PathID_4435	0.201143	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_4498	0.159429	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_4582	0.199	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_4590	0.155714	PWY-7237: myo-, chiro- and scillo-inositol degradation|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_4608	0.184	PWY-7664: oleate biosynthesis IV (anaerobic)|unclassified
PathID_4610	0.167714	PWY-6151: S-adenosyl-L-methionine cycle I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_4653	0.142714	PWY-6700: queuosine biosynthesis|g__Butyrivibrio.s__Butyrivibrio_crossotus
PathID_4737	0.222857	UNINTEGRATED|g__Clostridium.s__Clostridium_clostridioforme
PathID_4901	0.140857	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_4985	0.204714	PWY-7228: superpathway of guanosine nucleotides de novo biosynthesis I|g__Bacteroides.s__Bacteroides_intestinalis
PathID_5007	0.135857	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Butyrivibrio.s__Butyrivibrio_crossotus
PathID_5090	0.141143	PWY-6737: starch degradation V|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_5104	0.137286	UNINTEGRATED|g__Bacteroides.s__Bacteroides_sp_4_3_47FAA
PathID_5152	0.198429	ARO-PWY: chorismate biosynthesis I|g__Clostridium.s__Clostridium_clostridioforme
PathID_5165	0.123286	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Bacteroides.s__Bacteroides_sp_4_3_47FAA
PathID_5222	0.150571	PWY-5188: tetrapyrrole biosynthesis I (from glutamate)|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_5235	0.177286	PWY-6282: palmitoleate biosynthesis I (from (5Z)-dodec-5-enoate)|unclassified
PathID_5311	0.169429	PWY-6590: superpathway of Clostridium acetobutylicum acidogenic fermentation
PathID_5314	0.123429	PANTOSYN-PWY: pantothenate and coenzyme A biosynthesis I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_5347	0.104857	PWY-5097: L-lysine biosynthesis VI|g__Bacteroides.s__Bacteroides_sp_4_3_47FAA
PathID_5354	0.142714	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Butyrivibrio.s__Butyrivibrio_crossotus
PathID_5455	0.160714	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Bacteroides.s__Bacteroides_intestinalis
PathID_5494	0.214714	UNINTEGRATED|g__Dorea.s__Dorea_formicigenerans
PathID_5603	0.172714	PWY-3841: folate transformations II|g__Eubacterium.s__Eubacterium_rectale
PathID_5645	0.211286	ARO-PWY: chorismate biosynthesis I|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_5753	0.180857	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Eubacterium.s__Eubacterium_ventriosum
PathID_5759	0.167	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Eubacterium.s__Eubacterium_ventriosum
PathID_5763	0.168571	HSERMETANA-PWY: L-methionine biosynthesis III|unclassified
PathID_5849	0.171714	P461-PWY: hexitol fermentation to lactate, formate, ethanol and acetate|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_5951	0.151429	PWY-5695: urate biosynthesis/inosine 5'-phosphate degradation|g__Bacteroides.s__Bacteroides_sp_4_3_47FAA
PathID_5971	0.141429	COMPLETE-ARO-PWY: superpathway of aromatic amino acid biosynthesis|g__Butyrivibrio.s__Butyrivibrio_crossotus
PathID_6008	0.172286	1CMET2-PWY: N10-formyl-tetrahydrofolate biosynthesis|g__Eubacterium.s__Eubacterium_rectale
PathID_6055	0.203	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Parabacteroides.s__Parabacteroides_merdae
PathID_6087	0.207857	PANTO-PWY: phosphopantothenate biosynthesis I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_6197	0.187143	NONMEVIPP-PWY: methylerythritol phosphate pathway I|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_6198	0.195857	POLYAMINSYN3-PWY: superpathway of polyamine biosynthesis II|unclassified
PathID_6338	0.216571	PWY-2942: L-lysine biosynthesis III|g__Faecalibacterium.s__Faecalibacterium_prausnitzii
PathID_6409	0.150857	PWY-5686: UMP biosynthesis
PathID_6434	0.121571	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Clostridium.s__Clostridium_clostridioforme
PathID_6440	0.189714	PWY-6168: flavin biosynthesis III (fungi)|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_6551	0.264429	CRNFORCAT-PWY: creatinine degradation I
PathID_6558	0.146571	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Bacteroides.s__Bacteroides_salyersiae
PathID_6655	0.171	PWY-5097: L-lysine biosynthesis VI|g__Parabacteroides.s__Parabacteroides_merdae
PathID_6737	0.147	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Eubacterium.s__Eubacterium_ramulus
PathID_6820	0.162857	PWY-5695: urate biosynthesis/inosine 5'-phosphate degradation|g__Bacteroides.s__Bacteroides_intestinalis
PathID_6854	0.15	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Clostridium.s__Clostridium_bolteae
PathID_6892	0.197429	PWY-2942: L-lysine biosynthesis III|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_6923	0.224143	PWY-6122: 5-aminoimidazole ribonucleotide biosynthesis II|g__Clostridium.s__Clostridium_clostridioforme
PathID_6955	0.225714	COMPLETE-ARO-PWY: superpathway of aromatic amino acid biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_7027	0.156857	COA-PWY: coenzyme A biosynthesis I|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_7099	0.166286	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Bacteroides.s__Bacteroides_intestinalis
PathID_7102	0.159571	PWY-6121: 5-aminoimidazole ribonucleotide biosynthesis I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_7223	0.151143	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Barnesiella.s__Barnesiella_intestinihominis
PathID_7239	0.163857	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Eubacterium.s__Eubacterium_ventriosum
PathID_7244	0.276429	PWY-6737: starch degradation V|g__Clostridium.s__Clostridium_clostridioforme
PathID_7326	0.161857	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Bacteroides.s__Bacteroides_salyersiae
PathID_7343	0.224286	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Clostridium.s__Clostridium_clostridioforme
PathID_7354	0.146	PWY-5100: pyruvate fermentation to acetate and lactate II|g__Eubacterium.s__Eubacterium_ventriosum
PathID_7399	0.243	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Clostridium.s__Clostridium_clostridioforme
PathID_7429	0.120286	PWY-6737: starch degradation V|g__Eubacterium.s__Eubacterium_ventriosum
PathID_7439	0.142571	RIBOSYN2-PWY: flavin biosynthesis I (bacteria and plants)
PathID_7592	0.216286	PWY-7234: inosine-5'-phosphate biosynthesis III|unclassified
PathID_7593	0.191714	PWY-6386: UDP-N-acetylmuramoyl-pentapeptide biosynthesis II (lysine-containing)|g__Parabacteroides.s__Parabacteroides_merdae
PathID_7644	0.162571	PWY-5667: CDP-diacylglycerol biosynthesis I|g__Parabacteroides.s__Parabacteroides_merdae
PathID_7704	0.155429	RHAMCAT-PWY: L-rhamnose degradation I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_7766	0.217	THISYNARA-PWY: superpathway of thiamin diphosphate biosynthesis III (eukaryotes)|unclassified
PathID_7812	0.227	PWY-6277: superpathway of 5-aminoimidazole ribonucleotide biosynthesis|g__Clostridium.s__Clostridium_clostridioforme
PathID_7829	0.137571	PWY-6703: preQ0 biosynthesis|g__Bacteroides.s__Bacteroides_sp_4_3_47FAA
PathID_7843	0.273143	PWY-5484: glycolysis II (from fructose 6-phosphate)
PathID_8063	0.191714	PWYG-321: mycolate biosynthesis|unclassified
PathID_8117	0.124	PWY-7111: pyruvate fermentation to isobutanol (engineered)|g__Bacteroides.s__Bacteroides_dorei
PathID_8205	0.125143	PWY-6125: superpathway of guanosine nucleotides de novo biosynthesis II|g__Bacteroides.s__Bacteroides_sp_4_3_47FAA
PathID_8258	0.189286	PWY-5692: allantoin degradation to glyoxylate II
PathID_8389	0.229571	PWY-6590: superpathway of Clostridium acetobutylicum acidogenic fermentation|unclassified
PathID_8670	0.215571	PWY-6163: chorismate biosynthesis from 3-dehydroquinate|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_8685	0.147286	PWY-7222: guanosine deoxyribonucleotides de novo biosynthesis II|unclassified
PathID_8738	0.188143	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Bacteroides.s__Bacteroides_intestinalis
PathID_8914	0.260429	PWY-7211: superpathway of pyrimidine deoxyribonucleotides de novo biosynthesis|unclassified
PathID_9093	0.163429	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Eubacterium.s__Eubacterium_ventriosum
PathID_9095	0.132	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Bacteroides.s__Bacteroides_salyersiae
PathID_9196	0.177429	PWY-2942: L-lysine biosynthesis III|g__Bacteroides.s__Bacteroides_uniformis
PathID_9215	0.165429	PWY-6588: pyruvate fermentation to acetone|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_9230	0.141714	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Butyrivibrio.s__Butyrivibrio_crossotus
PathID_9306	0.138857	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Butyrivibrio.s__Butyrivibrio_crossotus
PathID_9312	0.239857	PWY-6305: putrescine biosynthesis IV|unclassified
PathID_9357	0.289143	GLYCOLYSIS: glycolysis I (from glucose 6-phosphate)
PathID_9468	0.219143	GALACT-GLUCUROCAT-PWY: superpathway of hexuronide and hexuronate degradation|unclassified
PathID_9507	0.238286	CENTFERM-PWY: pyruvate fermentation to butanoate|unclassified
PathID_9536	0.128429	PWY-6936: seleno-amino acid biosynthesis|g__Eubacterium.s__Eubacterium_ventriosum
PathID_9541	0.218	PWY0-781: aspartate superpathway|unclassified
PathID_9780	0.149857	PWY-6737: starch degradation V|g__Ruminococcus.s__Ruminococcus_bromii
PathID_9903	0.227	PWY-6151: S-adenosyl-L-methionine cycle I
PathID_9911	0.179286	PEPTIDOGLYCANSYN-PWY: peptidoglycan biosynthesis I (meso-diaminopimelate containing)|g__Parabacteroides.s__Parabacteroides_merdae
PathID_9918	0.170857	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Eubacterium.s__Eubacterium_ventriosum
PathID_9948	0.278571	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_9954	0.188714	UNINTEGRATED|g__Lachnospiraceae_noname.s__Lachnospiraceae_bacterium_5_1_63FAA
PathID_10029	0.123571	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Pseudoflavonifractor.s__Pseudoflavonifractor_capillosus
PathID_10330	0.153714	PWY-6121: 5-aminoimidazole ribonucleotide biosynthesis I|g__Clostridium.s__Clostridium_bolteae
PathID_10367	0.188857	PWY-7219: adenosine ribonucleotides de novo biosynthesis|g__Eubacterium.s__Eubacterium_ventriosum
PathID_10439	0.181286	COA-PWY-1: coenzyme A biosynthesis II (mammalian)|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_10497	0.142286	ARO-PWY: chorismate biosynthesis I|g__Eubacterium.s__Eubacterium_ventriosum
PathID_10764	0.174429	PWY-6609: adenine and adenosine salvage III|g__Eubacterium.s__Eubacterium_ventriosum
PathID_10875	0.181143	PWY-5686: UMP biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_10960	0.150714	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Eubacterium.s__Eubacterium_ramulus
PathID_10975	0.209857	COMPLETE-ARO-PWY: superpathway of aromatic amino acid biosynthesis|g__Clostridium.s__Clostridium_clostridioforme
PathID_11032	0.121286	PWY-6387: UDP-N-acetylmuramoyl-pentapeptide biosynthesis I (meso-diaminopimelate containing)|g__Bacteroides.s__Bacteroides_sp_4_3_47FAA
PathID_11074	0.178857	PWY-7221: guanosine ribonucleotides de novo biosynthesis|g__Clostridium.s__Clostridium_clostridioforme
PathID_11096	0.168286	PWY-6151: S-adenosyl-L-methionine cycle I|g__Clostridium.s__Clostridium_clostridioforme
PathID_11110	0.251714	CRNFORCAT-PWY: creatinine degradation I|unclassified
PathID_11201	0.131714	PWY-6151: S-adenosyl-L-methionine cycle I|g__Bacteroides.s__Bacteroides_sp_4_3_47FAA
PathID_11309	0.132	PWY-5177: glutaryl-CoA degradation
PathID_11323	0.236857	VALSYN-PWY: L-valine biosynthesis|g__Ruminococcus.s__Ruminococcus_sp_5_1_39BFAA
PathID_11474	0.137286	PWY0-1296: purine ribonucleosides degradation|g__Butyrivibrio.s__Butyrivibrio_crossotus
PathID_11512	0.198286	PWY-5097: L-lysine biosynthesis VI|g__Bacteroides.s__Bacteroides_uniformis
PathID_11797	0.159571	PWY-6121: 5-aminoimidazole ribonucleotide biosynthesis I|unclassified
PathID_12034	0.162571	PWY-6122: 5-aminoimidazole ribonucleotide biosynthesis II|g__Clostridium.s__Clostridium_bolteae
PathID_12257	0.110857	PWY0-1296: purine ribonucleosides degradation|g__Eubacterium.s__Eubacterium_ventriosum
PathID_12264	0.343857	PWY-7357: thiamin formation from pyrithiamine and oxythiamine (yeast)|unclassified
PathID_12364	0.199143	UNINTEGRATED|g__Parabacteroides.s__Parabacteroides_merdae
PathID_12502	0.225857	PWY0-1319: CDP-diacylglycerol biosynthesis II|g__Clostridium.s__Clostridium_clostridioforme
PathID_12520	0.172857	PWY-6305: putrescine biosynthesis IV|g__Faecalibacterium.s__Faecalibacterium_prausnitzii

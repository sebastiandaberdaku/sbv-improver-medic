PathwayID	Importance_Optional	Description
PathID_695	0.370143	PWY-5676: acetyl-CoA fermentation to butanoate II|unclassified
PathID_1946	0.234286	ANAGLYCOLYSIS-PWY: glycolysis III (from glucose)|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_3906	0.267143	PWY-4981: L-proline biosynthesis II (from arginine)|g__Odoribacter.s__Odoribacter_splanchnicus
PathID_4561	0.295143	PANTOSYN-PWY: pantothenate and coenzyme A biosynthesis I|unclassified
PathID_8117	0.290286	PWY-7111: pyruvate fermentation to isobutanol (engineered)|g__Bacteroides.s__Bacteroides_dorei

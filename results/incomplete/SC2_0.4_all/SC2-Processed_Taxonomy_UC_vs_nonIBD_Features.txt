TaxonomyID	Importance_Optional	Description
TaxID_689	0.304429	Vibrio mediterranei
TaxID_631220	0.256571	Desulfovibrio sp. G11
TaxID_1145276	0.286	Lysinibacillus varians
TaxID_1160	0.321143	Planktothrix agardhii
TaxID_1747	0.401286	Cutibacterium acnes
TaxID_881	0.321857	Desulfovibrio vulgaris
TaxID_54304	0.283857	Planktothrix
TaxID_1912216	0.347571	Cutibacterium
